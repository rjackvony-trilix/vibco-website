﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;

namespace SitefinityWebApp.Widgets.Admin.ProductEditorFields
{
    public partial class ProductFieldsEditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (ProductEditor.ProductId != Guid.Empty)
                {
                    if (ProductEditor.Product != null)
                    {
                        TextBoxName.Text = ProductEditor.Product.Name;
                        TextBoxKey.Text = ProductEditor.Product.Key;
                        HtmlEditorDescription.Value = ProductEditor.Product.Description;
                        DimensionAttributesEditor.Value = ProductEditor.Product.Dimensions.Items;
                        TechnicalDataAttributesEditor.Value = ProductEditor.Product.TechnicalData.Items;
                        ReviewsEditor.Value = ProductEditor.Product.Reviews.Items;
                        AccessoriesEditor.Value = ProductEditor.Product.Accessories.Items;
                        CategoriesEditor.Value = ProductEditor.Product.Categories;

                        if (ProductEditor.Product.Categories.Where(p => p.Id == ProductSetup.ProductAccessoriesCategoryId).Count() > 0)
                        {
                            PanelCategoriesEditor.Visible = false;
                            PanelListAttributes.Visible = false;
                        }
                    }
                   

                }
                else
                {
                    if (IsNewAcessory)
                    {
                        PanelCategoriesEditor.Visible = false;
                        PanelListAttributes.Visible = false;
                    }
                }

                DimensionAttributesEditor.AttributeParameters = ProductEditor.ProductManager.GetDimensionParameters();
                TechnicalDataAttributesEditor.AttributeParameters = ProductEditor.ProductManager.GetTechnicalDataParameters();
                //ReviewsEditor.AttributeParameters = ProductEditor.ProductManager.GetTechnicalDataParameters();
            }

            HyperLinkCancel.NavigateUrl = ProductEditor.ReturnBackUrl;
        }

        protected void CvTextBoxTitle_ServerValidate(object source, ServerValidateEventArgs e)
        {
             
                ProductEditor.Product.Title = String.Format("{0} - {1}", TextBoxKey.Text, TextBoxName.Text);
                e.IsValid = ProductEditor.ProductManager.CheckProductTitle(ProductEditor.Product.Title, ProductEditor.ProductId);
            
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            
            ProductEditor.Product.Name = TextBoxName.Text;
            ProductEditor.Product.Key = TextBoxKey.Text;
            ProductEditor.Product.Title = String.Format("{0} - {1}",TextBoxKey.Text, TextBoxName.Text);

            ProductEditor.Product.Description = HtmlEditorDescription.Value.ToString();
            ProductEditor.Product.Dimensions.Items = DimensionAttributesEditor.Value;
            ProductEditor.Product.TechnicalData.Items = TechnicalDataAttributesEditor.Value;
            ProductEditor.Product.Reviews.Items = ReviewsEditor.Value;
            ProductEditor.Product.Accessories.Items = AccessoriesEditor.Value;

            if (IsNewAcessory || ProductEditor.Product.Categories.Where(p => p.Id == ProductSetup.ProductAccessoriesCategoryId).Count() > 0)
                ProductEditor.Product.Categories = new List<ProductCategory>() { new ProductCategory() { Id = ProductSetup.ProductAccessoriesCategoryId, Name = "Accessories"} };
            else
                ProductEditor.Product.Categories = CategoriesEditor.Value;
            
            if (ProductEditor.ProductId == Guid.Empty)
                ProductEditor.ProductManager.CreateProduct(ProductEditor.Product);
            else
            {
                ProductEditor.Product.Id = ProductEditor.ProductId;
                ProductEditor.ProductManager.SaveProductChanges(ProductEditor.Product);
            }

            if (((Control)sender).ID == "ButtonSaveReturn")
                Response.Redirect(ProductEditor.ReturnBackUrl);

            string stepurl = Request.Url.ToString();

            string stepurlquery = Request.Url.Query.ToString();

            if (string.IsNullOrEmpty(stepurlquery))
            {
                stepurl = string.Concat(stepurl, "?");
            }

            if (!stepurlquery.Contains("productid"))
            {
                if (stepurlquery.Length == 0)
                    stepurl = string.Concat(stepurl, "productid=", ProductEditor.Product.Id);
                else
                    stepurl = string.Concat(stepurl, "&productid=", ProductEditor.Product.Id);
            }

            if (!stepurlquery.Contains("step"))
            {
                stepurl = string.Concat(stepurl, "&step=", 2);
            }
            else
            {
                stepurl = stepurl.Replace("step=1", "step=2");
            }

            Response.Redirect(stepurl);
        }

        protected bool IsNewAcessory
        {
            get
            {
                if (Request.Params["editor-type"] == "accessory")
                    return true;
                return false;
            }
        }

        protected ProductEditor ProductEditor
        {
            get
            {
                ProductEditor pEditor = this.Parent.Parent as ProductEditor;
                return pEditor;
            }
        }

    }
}