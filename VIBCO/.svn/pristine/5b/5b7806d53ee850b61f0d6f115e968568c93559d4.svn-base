﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Web.UI;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages.Configuration;
using System.Xml.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Text.RegularExpressions;

namespace Products
{
    public class ProductManager
    {
        private static ProductSetup productSetup;
        public ProductSetup ProductSetup
        {
            get
            {
                if(productSetup == null)
                    productSetup = new ProductSetup();

                return productSetup;
            }
        }

        #region Public Methods

        #region Product Parameters

        public IList<string> GetDimensionParameters()
        {
            var manager = ContentManager.GetManager();
            var item = manager.GetContent(ProductSetup.DimensionParametersContentId);
          
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            XDocument xdoc = XDocument.Parse("<root>" + itemContent + "</root>");

            var parameters = xdoc.Root.Elements("p").Select(element => element.Value).ToList<string>();

            return parameters;
        }
        public void deleteParameter(string value, string specificParameter)
        {
            var manager = ContentManager.GetManager();
            ContentItem item;
            Guid contentId = new Guid();
            string outputString = "";

            if(specificParameter == "Dimensions")
            {
               contentId = ProductSetup.DimensionParametersContentId;
            }
            else if (specificParameter == "Technical Data")
            {
               contentId = ProductSetup.TechnicalDataParametersContentId;
            }

            item = manager.GetContent(contentId);
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            string[] Contents = Regex.Split(itemContent, "</p>");
            
            for (var i = 0; i < Contents.Length-1; i++)
            {
                int index = Contents[i].IndexOf("<p>");
                string originalContent = Contents[i].Substring(index+3).Trim();
                if (originalContent != value)
                {
                    outputString += "<p>" + originalContent + "</p> ";
                }
            }
            outputString = outputString.Trim();
            App.WorkWith().ContentItem(contentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = outputString;
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();
        }
        public void setParameter(string value, string specificParameter)
        {
            var manager = ContentManager.GetManager();
            ContentItem item;
            Guid contentId = new Guid();
            if (specificParameter == "Dimensions")
            {
                contentId = ProductSetup.DimensionParametersContentId;
            }
            else if (specificParameter == "Technical Data")
            {
                contentId = ProductSetup.TechnicalDataParametersContentId;
            }
            item = manager.GetContent(contentId);
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));
            itemContent += " <p>" + value + "</p>";

            App.WorkWith().ContentItem(contentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = itemContent;
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();
        }
        public IList<string> GetTechnicalDataParameters()
        {
            IList<string> result = new List<string>();

            var manager = ContentManager.GetManager();
            var item = manager.GetContent(ProductSetup.TechnicalDataParametersContentId);

            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            XDocument xdoc = XDocument.Parse("<root>" + itemContent + "</root>");
            
            var parameters = xdoc.Root.Elements("p").Select(element => element.Value).ToList<string>();

            return parameters;
        }

        public int GetProductLibraryFilesCount(string libraryName, string title)
        {
            IList<ProductFile> result = new List<ProductFile>();

             DocumentLibrary dlib = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault();

             foreach (Document doc in dlib.Documents.Where(d => d.Status == ContentLifecycleStatus.Live ))
             {
                 result.Add(new ProductFile(){ FileId = doc.Id, FileType = ProductFileType.File, Key = doc.Title});
             }

            return result.Where(r => r.Key.Contains(title)).Count();
        }

        public IList<ProductFile> GetProductLibraryFiles(string libraryName, int maximumRowsCount, int startRowIndex, string title)
        {
            IList<ProductFile> result = new List<ProductFile>();

             DocumentLibrary dlib = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault();

             foreach (Document doc in dlib.Documents)
             {
                 result.Add(new ProductFile(){ FileId = doc.Id, FileType = ProductFileType.File, Key = doc.Title});
             }

            return result.Where(r => r.Key.Contains(title)).Skip(startRowIndex).Take(maximumRowsCount).ToList<ProductFile>();
        }

        #endregion

        #region Managing Products

        public void DeleteProducts(IList<Guid> productIdsToDelete)
        {
            using (var sf = App.WorkWith())
            {
                foreach (Guid id in productIdsToDelete)
                {
                    //try
                    //{
                    //    sf.DocumentLibrary(product.RelatedProductFiles.LibraryId).Delete();
                    //}
                    //catch (Exception ex) { }

                    //try
                    //{
                    //    sf.Image(product.RelatedProductFiles.GetId("Image")).Delete();
                    //}
                    //catch (Exception ex) { }

                    //try
                    //{
                    //    sf.Image(product.RelatedProductFiles.GetId("Spec Image")).Delete();
                    //}
                    //catch (Exception ex) { }
                    try
                    {
                        sf.ContentItem(id).Delete();
                    }
                    catch (Exception ex) { }
                }
            }
            
            HttpContext.Current.Cache.Remove("BaseProductList");
            
        }

        public void CreateProduct(Product product)
        {
            using (var sf = App.WorkWith())
            {
                try
                {
                    var taxManager = TaxonomyManager.GetManager();
                    var taxon = taxManager.GetTaxon<FlatTaxon>(ProductSetup.ProductsTaxonId);
                    var contentManager = ContentManager.GetManager();
                    
                    sf.ContentItem()
                        .CreateNew(product.Id)
                        .Do(c =>
                        { //setting the Content item (c) properties
                            product.FillContentItem(c);
                        })
                        .Publish()
                        .SaveChanges();

                    //Guid libraryId = product.RelatedProductFiles.LibraryId;

                    //sf.DocumentLibrary().CreateNew(libraryId).Do(d =>
                    //{
                    //    d.Title = string.Concat("ProductLibrary ", product.Title);
                    //}).SaveChanges();

                }
                catch (Exception ex)
                {

                }                
            }

            HttpContext.Current.Cache.Remove("BaseProductList");
        }

        public Guid DuplicateProduct(Guid productId)
        {
            Product product = GetProduct(productId);
            product.Id = Guid.NewGuid();
            CreateProduct(product);
            return product.Id;
        }

        public void SaveProductChanges(Product product)
        {
            using (var sf = App.WorkWith())
            {
                try
                {
                    sf.ContentItem(product.Id)
                        //.CheckOut()
                        .Do(c =>
                        { //setting the Content item (c) properties
                            product.FillContentItem(c);
                        })
                       // .CheckInAndPublish()
                        
                        .SaveChanges();


                }
                catch (Exception ex)
                {

                }
            }

            HttpContext.Current.Cache.Remove("BaseProductList");
        }

        #endregion

        #region Retrieve Products
        
        public IList<Product> GetProducts()
        {
            IList<Product> products = new List<Product>();

            using (var sf = App.WorkWith())
            {
                try
                {
                                        
                    sf.ContentItems()
                        .Where(c =>  c.ExpirationDate > DateTime.Today)
                        .OrderByDescending(c => c.DateCreated)
                        .ForEach(c =>
                        {
                            if (c.Organizer.TaxonExists("Tags", ProductSetup.ProductsTaxonId))
                            {
                                Product product = new Product(c);                                
                                products.Add(product);
                            }
                        });
                }
                catch(Exception ex)
                {

                }

                return products;
                
            }
        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex, string title)
        {
            IList<BaseProduct> products = new List<BaseProduct>();

            try
            {
                if(title == string.Empty)
                    products = GetAllBaseProducts().Skip(startRowIndex).Take(maximumRows).ToList();
                else
                    products = GetAllBaseProducts().Where(c => c.Title.ToLower().Contains(title.ToLower())).Skip(startRowIndex).Take(maximumRows).ToList();
            }
            catch (Exception ex)
            {
            }

            return products;
        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex)
        {
            return GetBaseProducts(maximumRows, startRowIndex, null);
        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex, object checkedNodes, string title)
        {
            
            IList<Guid> checkedNodesList = (IList<Guid>) checkedNodes;
            IList<BaseProduct> products = new List<BaseProduct>();

            try
            {
                products = GetAllBaseProducts().Where(c =>
                    {
                        if (checkedNodes == null || checkedNodesList.Count == 0)
                            return true;
                        foreach (ProductCategory category in c.Categories)
                        {
                            if (checkedNodesList.Contains(category.Id))
                                return true;
                        }
                        return false;
                    })
                    .Where(c =>
                        {
                            if (string.IsNullOrWhiteSpace(title))
                                return true;
                            return c.Title.ToLower().Contains(title.ToLower());
                        })
                    .Skip(startRowIndex)
                    .Take(maximumRows).ToList();
            }
            catch (Exception ex)
            {
            } 

            return products;
            
        }

        public IDictionary<Guid, string> GetProductsIdName()
        {
            IDictionary<Guid, string> products = new Dictionary<Guid, string>();

            using (var sf = App.WorkWith())
            {
                try
                {

                    sf.ContentItems()
                       // .Where(c => c.ExpirationDate > DateTime.Today)
                        .OrderBy(c => c.Name)
                        .ForEach(c =>
                        {
                            if (c.Organizer.TaxonExists("Tags", ProductSetup.ProductsTaxonId))
                            {
                                products.Add(new KeyValuePair<Guid,string>(c.Id, c.Title));                                
                            }
                        });
                }
                catch (Exception ex)
                {

                }

                return products;
            }
        }

        public Product GetProduct(string title)
        {
            Product product = null;

            var manager = ContentManager.GetManager();
            var item = manager.GetContent().Where(t => t.Title == title).FirstOrDefault();
            if (item != null)
            {
                product = new Product(item);
            }
            return product;
        }

        public Product GetProductByKey(string key)
        {
          /*BaseProduct result = GetAllBaseProducts().Where(c =>
              {
                  return c.Key == key;
              }).FirstOrDefault();

          return (result == null) ? null : (Product)result;*/

          //var manager = ContentManager.GetManager();
          //var item = manager.GetContent().Where(t => (string)t.GetValue("[PRODUCT KEY]") == key).FirstOrDefault();
          /*            var items = App.WorkWith()
                          .ContentItems()
                          .Where(t => t.Title.Contains(key))
                          .Get();*/

          //foreach(var item in items)
          //{
          //    if (item.GetValue<string>("PRODUCT KEY") == key)
          //}


          try {
            var manager = ContentManager.GetManager();
            var item = manager.GetContent().Where(t => t.Title == key).FirstOrDefault();
            return new Product(item);
            
          }
          catch(Exception ex)
          {
              string s = ex.Message;
          }          

          return null;
        }

        public Product GetProduct(Guid id)
        {
            Product product = null;

            var manager = ContentManager.GetManager();
            var item = manager.GetContent(id);
            if (item != null)
            {
                product = new Product(item);
            }
            return product;
        }

        #region Products Count

        public int GetProductsCount(string title)
        {
            if (title == string.Empty)
                return GetAllBaseProducts().Count();
            return GetAllBaseProducts().Where(c => c.Title.ToLower().Contains(title.ToLower())).Count();
        }

        public int GetProductsCount(int maximumRows, int startRowIndex, object checkedNodes, string title)
        {
            IList<Guid> checkedNodesList = (IList<Guid>)checkedNodes;

            return GetAllBaseProducts().Where(c =>
            {
                if (checkedNodes == null || checkedNodesList.Count == 0)
                    return true;
                foreach (ProductCategory category in c.Categories)
                {
                    if (checkedNodesList.Contains(category.Id))
                        return true;
                }
                return false;
            }).Where(c =>
                        {
                            if (string.IsNullOrWhiteSpace(title))
                                return true;
                            return c.Title.ToLower().Contains(title.ToLower());
                        }).Count();
        }

        #endregion

        #endregion

        #region Product Categories

        public IList<ProductCategory> GetCategories()
        {
            if (HttpContext.Current.Cache["ProductCategories"] == null)
            {
                TaxonomyManager manager = TaxonomyManager.GetManager();
                var parentTaxon = manager.GetTaxon<HierarchicalTaxon>(ProductSetup.ProductsCategoryId);

                IList<ProductCategory> categories = new List<ProductCategory>();

                GetSubCategories(parentTaxon, categories);

                var parentAcessoryTaxon = manager.GetTaxon<HierarchicalTaxon>(ProductSetup.ProductAccessoriesCategoryId);

                IList<ProductCategory> accessorycategories = new List<ProductCategory>();

                GetSubCategories(parentAcessoryTaxon, accessorycategories);

                foreach (ProductCategory apc in accessorycategories)
                    categories.Add(apc);

                HttpContext.Current.Cache.Insert("ProductCategories", categories, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0));
            }

            return ((IList<ProductCategory>)HttpContext.Current.Cache["ProductCategories"]);
        }

        public Guid GetCategoryId(string name)
        {
            Guid id = Guid.Empty;
            try
            {
                id = GetCategories().Where(c => c.Name == name).Single().Id;
            }
            catch (Exception) {}
            return id;
        }

        public ProductCategory GetCategory(Guid id)
        {
            try
            {
                return GetCategories().Where(c => c.Id == id).Single();
            }
            catch (Exception) { }
            return null;
        }

        public string GetCategoryName(Guid id)
        {
          try
          {
              return GetCategories().Where(c => c.Id == id).Single().Name;
          }
          catch (Exception) { }
          return null;
        }

        public IList<ProductCategory> GetFLSubCategories(Guid categoryId)
        {
            TaxonomyManager manager = TaxonomyManager.GetManager();
            var parentTaxon = manager.GetTaxon<HierarchicalTaxon>(categoryId);

            IList<ProductCategory> categories = new List<ProductCategory>();

            ProductCategory productCategory = new ProductCategory()
            {
              Id = parentTaxon.Id,
              Name = parentTaxon.Name,
            };

            if (parentTaxon.Parent != null)
              productCategory.ParentId = parentTaxon.Parent.Id;

            categories.Add(productCategory);

            foreach (HierarchicalTaxon childtaxon in parentTaxon.Subtaxa)
            {
              categories.Add(
                new ProductCategory() {
                  Id = childtaxon.Id,
                  Name = childtaxon.Name,
              });
            }

            return categories;
        }

        private bool IsInCategories(IList<Guid> itemCategories, IList<Guid> checkedNodes)
        {
            foreach (Guid id in checkedNodes)
            {
                if (itemCategories.Contains(id))
                    return true;
            }

            return false;
        }

        #endregion
        
        #region Files

        public void UploadFiles(Product product)
        {
            foreach (string imageKey in product.RelatedProductFiles.GetImageKeys())
            {

                Guid imageId = product.RelatedProductFiles.GetId(imageKey);
                ProductUploadedFile imageFile = product.RelatedProductFiles.GetImage(imageKey);
 
                if (imageId == Guid.Empty)
                {
                    App.WorkWith()
                        .Album(ProductSetup.ThumbnailsAlbumId)
                        .CreateImage()
                        .Do(i =>
                        {

                            i.Title = string.Concat(product.Title, " ", imageKey);
                            imageId = i.Id;
                        })
                        .CheckOut()
                        .UploadContent(imageFile.InputStream, imageFile.GetExtension())
                        .CheckIn().Publish()
                        .SaveChanges();
                    
                    product.RelatedProductFiles.Add(imageKey, imageId, ProductFileType.Image);
                }
                else
                {

                    App.WorkWith()
                       .Image(imageId)
                       .CheckOut()
                           .UploadContent(imageFile.InputStream, imageFile.GetExtension())
                           .Do(i =>
                           {
                               i.Title = string.Concat(product.Title, " ", imageKey);
                               i.LastModified = DateTime.UtcNow;
                           })
                        .CheckIn().Publish()
                        .SaveChanges();



                }

            }


            foreach (string fileKey in product.RelatedProductFiles.GetFileKeys())
            {

                Guid fileId = product.RelatedProductFiles.GetId(fileKey);
                ProductUploadedFile file = product.RelatedProductFiles.GetFile(fileKey);

                //Document document;
                if (fileId == Guid.Empty)
                {
                    //App.WorkWith()
                    //    .DocumentLibrary(product.RelatedProductFiles.LibraryId)
                    //    .CreateDocument()
                    //    .Do(d =>
                    //    {
                    //        d.Title = string.Concat(fileKey);
                    //        fileId = d.Id;
                    //    })
                    //    .CheckOut()
                    //    .UploadContent(file.InputStream, file.GetExtension())
                    //    .CheckInAndPublish()
                    //    .SaveChanges();

                    App.WorkWith()
                        .DocumentLibrary(GetFileLibId(fileKey))
                        .CreateDocument()
                        .Do(d =>
                        {
                            d.Title = product.Title;
                            fileId = d.Id;
                        })
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckInAndPublish(true)
                        .SaveChanges();

                    product.RelatedProductFiles.Add(fileKey, fileId, ProductFileType.File);
                }
                else
                {
                    App.WorkWith()
                        .Document(fileId)
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .Do(d =>
                        {
                            d.LastModified = DateTime.UtcNow;
                        })
                        .CheckInAndPublish(true)
                        .SaveChanges();
                }

            }

            SaveProductChanges(product);
        }

        public void UploadFileToProductLibrary(Guid libraryId, ProductUploadedFile file)
        {
            App.WorkWith()
                        .DocumentLibrary(libraryId)
                        .CreateDocument()
                        .Do(d =>
                        {
                            d.Title = file.FileName;
                        })
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckInAndPublish()
                        .SaveChanges();
        }

        public string GetImageURL(Product product, string imgName)
        {
            Guid id = product.RelatedProductFiles.GetId(imgName);
            if (id == Guid.Empty)
                return null;
            else
            {
                Image result = App.WorkWith().Image(id).Get();

                if (result.Width > 300)
                  return result.MediaUrl.Contains("?") ? 
                   result.MediaUrl + "&size=400" :
                   result.MediaUrl + "?size=400";
                else
                  return result.MediaUrl;
            }
        }

        public string GetFileURL(Product product, string fileType)
        {
            Guid id = product.RelatedProductFiles.GetId(fileType);

            if (id == Guid.Empty)
                return null;
            else
            {
                try {
                  return App.WorkWith().Document(id).Get().MediaUrl;
                }
                catch {
                  return null;
                }
            }
        }

        public string GetFileName(Product product, string fileType)
        {
            Guid id = product.RelatedProductFiles.GetId(fileType);

            if (id == Guid.Empty)
                return string.Empty;
            else
            {
                try
                {
                    return App.WorkWith().Document(id).Get().Title;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public void DeleteFile(Product product, string fileName) 
        {
            Guid id = product.RelatedProductFiles.GetId(fileName);

            if (id == Guid.Empty)
              return;
            else
            {
                product.RelatedProductFiles.DeleteFile(fileName);
                SaveProductChanges(product);

                App.WorkWith().Document(id).Delete().SaveChanges();
            }
        }
        #endregion

        public bool CheckProductTitle(string title, Guid id)
        {
            try
            {
                int indicator = 0;
                App.WorkWith().ContentItems().Publihed()
                                    // .Where(c => c.ExpirationDate > DateTime.Today)
                                     .Where( (c => c.Title == title && !c.Id.Equals(id)) )
                                     .Count(out indicator);
             
                if (indicator == 0)
                    return true;

            }
            catch (Exception ex)
            {

            }
            return false;
        }
        
        #endregion

        #region Private Methods

        private Guid GetFileLibId(string fileKey)
        {
            string libraryName = string.Empty;

            switch (fileKey)
            {
                case "Application Bulletin":
                    libraryName = "Product Application Bulletins";
                    break;
                case "Complete Service Manual":
                    libraryName = "Complete Service Manuals";
                    break;
                case "Flyer":
                    libraryName = "Flyers";
                    break;
                case "Catalog":
                    libraryName = "Catalogs";
                    break;
                case "Quick Reference Guide":
                    libraryName = "Quick Reference Guides";
                    break;
            }

            Guid result = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault().Id;
            return result;
        }

        private IList<BaseProduct> GetAllBaseProducts()
        {
            if (HttpContext.Current.Cache["BaseProductList"] == null)
            {
                IList<BaseProduct> products = new List<BaseProduct>();
                try
                {
                    App.WorkWith().ContentItems().Publihed()
                        //.Where(c => c.ExpirationDate > DateTime.Today)
                        .Where(c => c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId))
                        .OrderByDescending(c => c.DateCreated)
                        .ForEach(c =>
                        {
                            BaseProduct product = new BaseProduct(c);
                            products.Add(product);
                        });
                }
                catch (Exception ex)
                {

                }

                HttpContext.Current.Cache.Insert("BaseProductList", products, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 1, 0));
            }
            return (IList<BaseProduct>)HttpContext.Current.Cache["BaseProductList"];
        }

        private void GetSubCategories(HierarchicalTaxon taxon, IList<ProductCategory> categories)
        {
            ProductCategory productCategory = new ProductCategory()
            {
                Id = taxon.Id,
                Name = taxon.Name,
            };

            if (taxon.Parent != null)
                productCategory.ParentId = taxon.Parent.Id;

            categories.Add(productCategory);

            foreach (HierarchicalTaxon childtaxon in taxon.Subtaxa)
            {
                GetSubCategories(childtaxon, categories);
            }
        }

        #endregion
    }
}
