﻿Type.registerNamespace("SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor");

SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor = function (element) {
    SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor.initializeBase(this, [element]);

    this._imageId = null;
}

SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor.prototype = {
    initialize: function () {
        SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor.callBaseMethod(this, 'initialize');
        this._itemSelectDelegate = Function.createDelegate(this, this._itemSelect);
        this._selectorView.add_onItemSelectCommand(this._itemSelectDelegate);

    },
    dispose: function () {
        SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor.callBaseMethod(this, 'dispose');
        if (this._selectorView) {
            this._selectorView.add_onItemSelectCommand(this._itemSelectDelegate);
        }
    },
    get_selectorView: function () {
        return this._selectorView;
    },
    set_selectorView: function (value) {
        this._selectorView = value;
    },

    _itemSelect: function (sender, args) {
        //Get the date from the image.  I wanted the ID of the image.
        this._imageId = args.get_dataItem().Id;
        //Save this in a hidden field on the page.  This allows it to get passed back to the server.
        $("#imageId").val(this._imageId);
    },

    add_onItemSelectCommand: function (delegate) {
        this.get_events().addHandler('onItemSelectCommand', delegate);
    }
}

SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor.registerClass('SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor', Sys.Component);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
