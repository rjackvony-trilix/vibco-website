﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telerik.Sitefinity.Services.Search.Data;


namespace SitefinityWebApp.Widgets
{
    public class VIBCOSearchProvider : LuceneSearchProvider
    {
        public override IResultSet Find(string catalogueName, string query, int skip, int take, params string[] orderBy)
        {
            return new VIBCOResultSet(this, catalogueName, query, skip, take, orderBy);
        }

        public override IResultSet Find(string catalogueName, string query, params string[] orderBy)
        {
            return new VIBCOResultSet(this, catalogueName, query, orderBy);
        }
    }
}
