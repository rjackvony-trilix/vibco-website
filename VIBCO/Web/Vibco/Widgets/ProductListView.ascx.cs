﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using System.Collections;
using Telerik.Web.UI;
using System.Data;
namespace SitefinityWebApp.Widgets
{
    public partial class ProductListView : System.Web.UI.UserControl
    {
        ProductManager pm = new ProductManager();
        public string CategoryId
        {
          get; set;
        }

        public string CategoryName
        {
          get;
          set;
        }

        public string ProductsNavigateUrl
        {
          get;
          set;
        }

        private void Page_Init(object sender, EventArgs args)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void ProductsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if(!string.IsNullOrEmpty(CategoryName))
            {
                Guid category = pm.GetCategoryId(CategoryName);

                if(category != Guid.Empty)
                    CategoryId = category.ToString();
            }

         /*   if(string.IsNullOrEmpty(CategoryId))
                CategoryId = ProductSetup.ProductsCategoryId.ToString(); */  //can't find it don't show anything

            if (!string.IsNullOrEmpty(CategoryId))
            {
                List<Guid> categories = new List<Guid>();

                
                Guid catId = new Guid(CategoryId);

                CategoryName = pm.GetCategoryName(catId);


                GetRecursiveCategories(ref categories, catId);

                e.InputParameters["checkedNodes"] = categories;
            }
            else
                e.Cancel = true;//if category is bad show nothing not everything.
        }

        private void GetRecursiveCategories(ref List<Guid> categories, Guid cID)
        {
            categories.Add(cID);  //alwasy returns same as first one
            for (int i = 1; i < pm.GetFLSubCategories(cID).Count; i++)
            {
                GetRecursiveCategories(ref categories, pm.GetFLSubCategories(cID)[i].Id);
               
            }
            
        }
        protected void RadGridProducts_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (e.Column is GridGroupSplitterColumn)
            {
                e.Column.HeaderStyle.Width = Unit.Pixel(1);
                e.Column.HeaderStyle.Font.Size = FontUnit.Point(1);
                e.Column.ItemStyle.Width = Unit.Pixel(1);
                e.Column.ItemStyle.Font.Size = FontUnit.Point(1);
                e.Column.Resizable = false;
            }
        }
        protected void RadGridProducts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                (e.Item as GridGroupHeaderItem).Cells[0].Controls.Clear();
            }
        } 
        //NavigateUrl="<%# ProductsNavigateUrl %>?title=<%# Eval("Title") %>"
        //<asp:HyperLink runat="server" ID="Hyperlink1" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetProductUrl(Convert.ToString(Eval("Key"))) %>' />
        public string GetProductUrl(string key, string category)
        {
            string result = string.Empty;
            
            if(string.IsNullOrEmpty(ProductsNavigateUrl))
                result = "~/products/product";
            else {
                if (category != CategoryName)
                  result = ProductsNavigateUrl +  category.Replace("/","-") + "/product";
                else
                  result = ProductsNavigateUrl + "product";
            }

            return result + "?productkey=" + key;
        }
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["MainCategory"].ToString();
            }
        }

    }
}