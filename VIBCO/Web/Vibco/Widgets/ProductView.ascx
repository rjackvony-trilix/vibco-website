﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductView.ascx.cs" Inherits="SitefinityWebApp.Widgets.ProductView" %>
<h1><asp:Label runat="server" ID="LabelTitle"></asp:Label></h1>

<script language="javascript" type="text/javascript">
  $(function () {
    var tabContainers = $('div.tabs > div');
    tabContainers.hide().filter(':first').show();

    $('div.tabs ul.tabNavigation a').click(function () {
      tabContainers.hide();
      tabContainers.filter(this.hash).show();
      $('div.tabs ul.tabNavigation li').removeClass('selected');
      $(this).parent().addClass('selected');
      return false;
    }).filter(':first').click();
  });
</script>

<div>
<asp:Image ID="imgLabelProduct" runat="server" />
</div>

<br /><br />

<div class="tabs"> 

  <ul class="tabNavigation">
	  <li class="selected"><a href="#accessoires">Accessories</a></li>
	  <li><a href="#documents">Documents</a></li>
	  <li><a href="#dimensions">Dimensions</a></li>
	  <li><a href="#techdata">Tech Data</a></li>
  </ul>

  <div id="accessoires" class="tab">
    <h4>RECOMMENDED <span>ACCESSORIES</span></h4>
    <table width="410" class="tblProduct">
      <asp:ListView ID="lvAccessoires" runat="server">
      <ItemTemplate>
        <tr class="tblOdd">
          <td><%# Eval("Name")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
      <tr class="tblEven">
          <td><%# Eval("Name")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table>
  </div>

  <div id="documents" class="tab">
  <asp:HyperLink ID="hlAppBulletin" runat="server">Application Bulletin</asp:HyperLink><br />
  <asp:HyperLink ID="hlQuickReferenceGuide" runat="server">Quick Reference Guides</asp:HyperLink><br />
  <asp:HyperLink ID="hlCompleteServiceManual" runat="server">Complete Service Manual</asp:HyperLink><br />
  <asp:HyperLink ID="hlFlyer" runat="server">Flyer</asp:HyperLink><br />
  <asp:HyperLink ID="hlCatalog" runat="server">Catalog</asp:HyperLink><br />
  </div>

  <div id="dimensions" class="tab">
    <h4><span>Dimensions</span></h4>
      <asp:Image ID="imgSpec" runat="server" Width="410px"/>
    <table width="410" class="tblProduct">
      <asp:ListView ID="lvDimensions" runat="server">
      <ItemTemplate>
        <tr class="tblOdd">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
        <tr class="tblEven">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table>
  </div>

  <div id="techdata" class="tab">
    <h4><span>Tech Data</span></h4>
    <table width="410" class="tblProduct">
      <asp:ListView ID="lvTechData" runat="server">
      <ItemTemplate>
       <tr  class="tblOdd">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
        <tr class="tblEven">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table>
  </div>

</div>