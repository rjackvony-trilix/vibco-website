﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResults.ascx.cs" Inherits="SitefinityWebApp.Widgets.Templates.SearchResults" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Services.Search.Web.UI.Public" TagPrefix="sfSearch" %>

<script type="text/javascript">
    var sideheader = document.getElementById("SideBarNavTitle");

    if (sideheader != null) {

        sideheader.innerHTML = "Search Results"
        //window.location(window.location);
    }

</script>

<h1 style="margin-bottom:0px">Search Results</h1>

<asp:Label ID="lblTest" runat="server" Text="Label"/>

<telerik:RadListView runat="server" ID="radList" AllowPaging="True" 
    EnableEmbeddedSkins="false">
    <ItemTemplate>
        <tr class="rlvA">
            <td>
                <div class="searchResults" id="sresult" runat="server">
                    <dt class="sfsearchResultTitle"><a id="A1" runat="server" href='<%# CorrectedLink(Eval("Link").ToString(),Eval("Title").ToString())%>'>
                        <%# Eval("Title") %></a></dt>
                    <dd class="sfsearchResultSnippet">
                        <%# GetSummary(Eval("Title").ToString())%></dd>
                </div>
            </td>
        </tr>
    </ItemTemplate>
    <LayoutTemplate>
        <div class="RadListView RadListView_Vista">
            <table cellspacing="0" style="width: 100%;" class="rlvTable">
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <asp:DataPager ID="DataPagerResults" runat="server" PagedControlID="radList"
                                PageSize="10">
                                <Fields>
                                    <asp:TemplatePagerField>
                                        <PagerTemplate>
                                            <span style="display: block; padding: 3px 7px; background: #f6f6f6">
                                        </PagerTemplate>
                                    </asp:TemplatePagerField>
                                    <asp:NextPreviousPagerField ShowFirstPageButton="True" ShowNextPageButton="false" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ShowLastPageButton="True" ShowPreviousPageButton="false" />
                                    <asp:TemplatePagerField>
                                        <PagerTemplate>
                                            </span>
                                        </PagerTemplate>
                                    </asp:TemplatePagerField>
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </tbody>
            </table>
        </div>
    </LayoutTemplate>
</telerik:RadListView>