﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Products;
using System.Text.RegularExpressions;

using Telerik.Sitefinity.Services.Search.Data;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Utilities.Lucene.Net.Analysis.Standard;
using Telerik.Sitefinity.Utilities.Lucene.Net.QueryParsers;
using Telerik.Sitefinity.Utilities.Lucene.Net.Search;
using Telerik.Sitefinity.Utilities.Lucene.Net.Store;
using Telerik.Sitefinity.Utilities.Lucene.Net.Util;
using Telerik.Sitefinity.Utilities.Lucene.Net.Index;

using MyLuceneStore = Telerik.Sitefinity.Utilities.Lucene.Net.Store;
using MyLuceneSearch = Telerik.Sitefinity.Utilities.Lucene.Net.Search;
using MyLuceneIndex = Telerik.Sitefinity.Utilities.Lucene.Net.Index;
using MyLuceneDocuments = Telerik.Sitefinity.Utilities.Lucene.Net.Documents;
using MyLuceneAnalysis = Telerik.Sitefinity.Utilities.Lucene.Net.Analysis;
using MyLuceneUtil = Telerik.Sitefinity.Utilities.Lucene.Net.Util;
using MyLuceneQuery = Telerik.Sitefinity.Utilities.Lucene.Net.QueryParsers;
using System.IO;
using System.Data;

namespace SitefinityWebApp.Widgets.Templates
{
    public partial class SearchResults : System.Web.UI.UserControl
    {
        //private const int ITEMS_PER_PAGE = 10;
        //private int TotalHits = 0;

        MyLuceneSearch.Searcher _searcher;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Page.Items["IsInIndexMode"] == null)
                BindData();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            /*if (!Page.IsPostBack)
                BindData();*/
        }

        private void BindData()
        {
            //fields to search
            string[] fields = {"Content", "Title"};
            MyLuceneAnalysis.Analyzer analyzer = new MyLuceneAnalysis.KeywordAnalyzer();

            //use this to search multiple fields
            MyLuceneQuery.MultiFieldQueryParser parser = new MultiFieldQueryParser(MyLuceneUtil.Version.LUCENE_29, fields , analyzer);
            parser.SetAllowLeadingWildcard(true);
            string query = Request.QueryString["searchQuery"].ToString();
            MyLuceneSearch.Query theQuery = parser.Parse("*" + query);

            string path = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            path = Path.Combine(path, "Sitefinity");
            path = Path.Combine(path, "Search");
            path = Path.Combine(path, Request.QueryString["indexCatalogue"].ToString());

            MyLuceneStore.FSDirectory dir = FSDirectory.Open(new DirectoryInfo(path));

            MyLuceneSearch.Searcher searcher = new IndexSearcher(MyLuceneIndex.IndexReader.Open(dir, true));

            //so the compareTo methods can get the docs
            _searcher = searcher;

            TopScoreDocCollector collector = TopScoreDocCollector.create(100, true);
            
            searcher.Search(theQuery, collector);

            ScoreDoc[] hits = collector.TopDocs().scoreDocs;
            List<ScoreDoc> sortedHits = new List<ScoreDoc>(hits);
            int ProductHits = 0;
            
            //count product hits
            foreach (ScoreDoc s in sortedHits)
            {
                MyLuceneDocuments.Document d = searcher.Doc(s.doc);
                if (d.GetField("ContentType").StringValue().Contains("Model.ContentItem"))
                    ProductHits++;
            }

            //sort entire list with ContentItems first (Products)
            sortedHits.Sort(CompareSearchResultsType);
            
            //sort products by title
            List<ScoreDoc> tempProds = new List<ScoreDoc>(sortedHits.GetRange(0, ProductHits));
            tempProds.Sort(CompareSearchResultsTitle);

            //sort other content by title
            List<ScoreDoc> tempOther = new List<ScoreDoc>(sortedHits.GetRange(ProductHits, sortedHits.Count-ProductHits));
            tempOther.Sort(CompareSearchResultsTitle);

            //combine seperated lists for the search results control
            List<ScoreDoc> sortedAllHitsCombined = new List<ScoreDoc>();
            sortedAllHitsCombined.AddRange(tempProds);
            sortedAllHitsCombined.AddRange(tempOther);
            
            //datatable to bind to control
            DataTable dt = new DataTable();
            dt.Columns.Add("Title", typeof(string));
            dt.Columns.Add("Link", typeof(string));
            dt.Columns.Add("Content", typeof(string));
            dt.Columns.Add("HighLighterResult", typeof(string));

            int totalResults = 0;

            //fill rows with data from document list
            //add rows to table
            for (int i = 0; i < sortedAllHitsCombined.Count; i++)
            {
                int docId = sortedAllHitsCombined[i].doc;

                MyLuceneDocuments.Document doc = searcher.Doc(docId);
                MyLuceneDocuments.Field title = doc.GetField("Title");
                MyLuceneDocuments.Field link = doc.GetField("Link");
                MyLuceneDocuments.Field content = doc.GetField("Content");

                string linkStr = link.StringValue();

                if (linkStr.Contains("Sitefinity/"))
                    continue;

                DataRow dr = dt.NewRow();
                dr["Title"] = title.StringValue();
                dr["Link"] = linkStr;
                dr["Content"] = content.StringValue();
                dr["HighLighterResult"] = query.Replace("*", "");

                dt.Rows.Add(dr);
                totalResults++;
            }

            //populate the total hit result label
            string plural = "";
            if (hits.Length > 1)
                plural = "s";
            lblTest.Text = "<p class=\"sfsearchResultStatistics\">" + totalResults + " search result" + plural + " for <strong>" + query.Replace("*", "") + "</strong></p>";

            //bind
            radList.DataSource = dt;
            radList.DataBind();
        }

        private int CompareSearchResultsType(ScoreDoc x, ScoreDoc y)
        {
            MyLuceneDocuments.Document doc1 = _searcher.Doc(x.doc);
            MyLuceneDocuments.Document doc2 = _searcher.Doc(y.doc);
            string first = doc1.GetField("ContentType").StringValue();
            string second = doc2.GetField("ContentType").StringValue();

            if (first.Equals(second))
                return 0;
            else
            {
                string[] split1 = first.Split('.');
                string type1 = split1[split1.Length - 1];
                /*string[] split2 = second.Split('.');
                string type2 = split2[split2.Length];
                
                string title1 = doc1.GetField("Title").StringValue();
                string title2 = doc2.GetField("Title").StringValue();
                */
                if (type1.Equals("ContentItem"))
                    return -1;
                else
                    return 1;
            }
        }

        private int CompareSearchResultsTitle(ScoreDoc x, ScoreDoc y)
        {
            MyLuceneDocuments.Document doc1 = _searcher.Doc(x.doc);
            MyLuceneDocuments.Document doc2 = _searcher.Doc(y.doc);
            string first = doc1.GetField("Title").StringValue();
            string second = doc2.GetField("Title").StringValue();

            return first.CompareTo(second);
        }

        //public int PgNum
        //{
        //    get
        //    {
        //        if (ViewState["PgNum"] != null)
        //            return Convert.ToInt32(ViewState["PgNum"]);
        //        else
        //            return 0;
        //    }
        //    set
        //    {
        //        ViewState["PgNum"] = value;
        //    }
        //}

        //protected void lnkPrev_Click(object sender, EventArgs e)
        //{
        //    PgNum -= 1;
        //    BindData();
        //}

        //protected void lnkNext_Click(object sender, EventArgs e)
        //{
        //    PgNum += 1;
        //    BindData();
        //}

        //private void CreateLinkButtons()
        //{
        //    linkButtonPanel.Controls.Clear();
        //    int TotalPages = (TotalHits / ITEMS_PER_PAGE) + 1;
        //    for (int i = 0; i < TotalPages; i++)
        //    {
        //        LinkButton lnkButton = new LinkButton();
        //        lnkButton.Text = (i + 1).ToString();
        //        lnkButton.Attributes.Add("runat", "server");
        //        lnkButton.Click += new EventHandler(SelectPage);

        //        if (i == PgNum)
        //        {
        //            lnkButton.Attributes.Remove("href");
        //            lnkButton.Attributes.CssStyle[HtmlTextWriterStyle.Color] = "gray";
        //            lnkButton.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
        //            lnkButton.Enabled = false;
        //        }

        //        linkButtonPanel.Controls.Add(lnkButton);
        //    }
        //}

        //protected void SelectPage(object sender, EventArgs e)
        //{
        //    int pageNum = 0;
        //    pageNum = Int32.Parse(((LinkButton)sender).Text);
        //}

        public string CorrectedLink(String strOldLink, String strTitle)
        {
             

            if (strOldLink.Contains("/products/"))
                return FormatURL(strTitle, strOldLink);
            else
                return strOldLink;
            
        }

        public string GetSummary(String strTitle)
        {
            ProductManager productManager = new ProductManager();
            Product product = productManager.GetProductByKey(strTitle);
            String strSum = "";

            if (product != null)
            {
                if (product.Categories.Count != 0 && product.Categories[0].Name != "Accessories")
                {
                    if (product.Description.Length > 250)
                        strSum = product.Description.Substring(0, 250);
                    else
                        strSum = product.Description;

                    if (!strSum.IsNullOrEmpty())
                    {
                        strSum = Regex.Replace(strSum, "<h2>", "<p>");
                        strSum = Regex.Replace(strSum, "</h2>", "</p>");
                        strSum = Regex.Replace(strSum, "<h3>", "<p>");
                        strSum = Regex.Replace(strSum, "</h3>", "</p>");
                        strSum = Regex.Replace(strSum, "<h1>", "<p>");
                        strSum = Regex.Replace(strSum, "</h1>", "</p>");
                        if (strSum.LastIndexOf(" ") != -1)
                        {
                            strSum = strSum.Substring(0, strSum.LastIndexOf(" "));
                            strSum += "...</p>";
                        }
                    }
                }
            }



            return strSum;
        }

		private string FormatURL(string strTitle, string strOldLink)
        {
            string urlReturn = "";
            ProductManager productManager = new ProductManager();
            Product product = productManager.GetProductByKey(strTitle);
          

            urlReturn = "";

            try
            {
                if (product != null)
                {
                    ProductCategory pc = product.Categories[0];

                    GetRecursiveCategories(ref urlReturn, pc);

                    urlReturn = "/" + urlReturn + product.ProductUrl;

                    if (urlReturn.Contains("Accessories"))
                        urlReturn = "javascript:alert('Accessories are not viewable');";
                }
                else
                {
                    if (strOldLink.Contains("/Product"))
                    {
                        urlReturn = strOldLink.Substring(0, strOldLink.LastIndexOf("/Product"));
                    }
                    else
                    {
                        urlReturn = strOldLink;
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return urlReturn;

        }

        private void GetRecursiveCategories(ref string strReturn, ProductCategory pc)
        {
            if (pc == null)
                return;
           ProductManager pm = new ProductManager();
           strReturn = pc.Name.Replace("/", "-").Replace(" & ","-").Replace(" ","-") + "/" + strReturn;  //alwasy returns same as first one
           GetRecursiveCategories(ref strReturn, pm.GetCategory(pc.ParentId));


        }
    }
}