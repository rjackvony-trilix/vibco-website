﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI.PublicControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Fluent.Pages;

namespace SitefinityWebApp.Widgets
{
    public partial class ProductView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SiteMapNode currentNode = SiteMapBase.GetActualCurrentNode();
            //if (currentNode != null)
            //{
            //    PageSiteNode node = (PageSiteNode)currentNode;
            //    var pageNode = App.WorkWith().Pages().LocatedIn(PageLocation.Frontend).Where(p => p.UrlName == node.UrlName).Get().FirstOrDefault();

            //    ContentPlaceHolder cph = (ContentPlaceHolder)Page.Master.FindControl("Breadcrumbs");
            //    SiteMapPath smp = (SiteMapPath)cph.FindControl("SiteMapPath1");

             
                

            //}

            
            if (!IsPostBack)
            {

                ProductManager productManager = new ProductManager();
                  Product product = null;
                  if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                      product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
                  else if (!string.IsNullOrEmpty(Request.QueryString["producttitle"]))
                      product = productManager.GetProduct(Server.UrlDecode(Request.QueryString["producttitle"]));
                  else
                      return;

                // Title
                LabelTitle.Text = product.Title;

                // Main Image
                string url = productManager.GetImageURL(product, "Image");

                if(!string.IsNullOrEmpty(url)) imgLabelProduct.ImageUrl = url;
                else imgLabelProduct.Visible = false;

                // Accessoires
                lvAccessoires.DataSource = product.Accessories.Items;
                lvAccessoires.DataBind();

                // Documents
                url = productManager.GetFileURL(product, "Application Bulletin");

                if (!string.IsNullOrEmpty(url)) hlAppBulletin.NavigateUrl = url;
                else hlAppBulletin.Visible = false;

                url = productManager.GetFileURL(product, "Complete Service Manual");

                if (!string.IsNullOrEmpty(url)) hlCompleteServiceManual.NavigateUrl = url;
                else hlCompleteServiceManual.Visible = false;

                url = productManager.GetFileURL(product, "Quick Reference Guide");

                if (!string.IsNullOrEmpty(url)) hlQuickReferenceGuide.NavigateUrl = url;
                else hlQuickReferenceGuide.Visible = false;

                lvDimensions.DataSource = product.Dimensions.Items;
                lvDimensions.DataBind();

                lvTechData.DataSource = product.TechnicalData.Items;
                lvTechData.DataBind();

                Page.Title = product.Title;

            }
        }
    }
}