﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 
using System.Web.UI.HtmlControls;
using Products;

namespace SitefinityWebApp.Widgets.Templates
{
    public partial class SearchResulstMobile : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            resultsStats.Text = resultsStats.Text.Replace("*", "");

        }
        public string CorrectedLink(String strOldLink, String strTitle)
        {


            if (strOldLink.Contains("/products/"))
                return FormatURL(strTitle);
            else
                return strOldLink;

        }



        private string FormatURL(string strTitle)
        {
            string urlReturn = "";
            ProductManager productManager = new ProductManager();
            Product product = productManager.GetProductByKey(strTitle);


            urlReturn = "";

            if (product != null && product.Categories[0].Name != "Accessories")
            {
                urlReturn = "/mobile/" + product.ProductUrl.Replace("product/", "products/");

                if (urlReturn.Contains("Accessories"))
                    urlReturn = "javascript:alert('Accessories are not viewable');";
            }
            else
                urlReturn = "javascript:alert('Not viewable in mobile version.');";

            return urlReturn;

        }

        protected void resultsList_itemdatabound(Object Sender, RepeaterItemEventArgs e)
        {
            /*    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
                { 
               }*/

        }
    }
}