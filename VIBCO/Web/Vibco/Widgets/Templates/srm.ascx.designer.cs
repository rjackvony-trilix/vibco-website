﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace SitefinityWebApp.Widgets.Templates {
    
    
    public partial class SearchResulstMobile {
        
        /// <summary>
        /// topSearchBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Sitefinity.Services.Search.Web.UI.Public.SearchBox topSearchBox;
        
        /// <summary>
        /// resultsStats control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Sitefinity.Web.UI.SitefinityLabel resultsStats;
        
        /// <summary>
        /// resultsList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater resultsList;
        
        /// <summary>
        /// pager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Sitefinity.Web.UI.Pager pager;
        
        /// <summary>
        /// bottomSearchBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Sitefinity.Services.Search.Web.UI.Public.SearchBox bottomSearchBox;
    }
}
