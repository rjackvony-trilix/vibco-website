﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.PublicControls.BrowseAndEdit" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>


<sf:BrowseAndEditToolbar ID="browseAndEditToolbar" runat="server" Mode="Edit"></sf:BrowseAndEditToolbar>
<div id="itemsContainer" runat="server">
    <asp:Repeater ID="documentsRepeater" runat="server">
        <HeaderTemplate>
		    <ul class="sfdownloadList sfListMode">
	    </HeaderTemplate>
	    <ItemTemplate>
		    <li id="docItem" runat="server" class="sfdownloadFile">
			    <sitefinity:SitefinityHyperLink ID="documentLink" runat="server" CssClass="sfdownloadTitle" target="_blank" /><br />
                <asp:Label ID="descLabel" runat="server" Text='<%# Eval("Description")%>'  CssClass="sfInfo"></asp:Label>
                <sitefinity:SitefinityLabel id="infoLabel" runat="server" WrapperTagName="div" HideIfNoText="false" CssClass="sfInfo" />
                
                
            </li>
	    </ItemTemplate>
	    <FooterTemplate>
		    </ul>
	    </FooterTemplate>
    </asp:Repeater>
</div>
<sitefinity:Pager id="pager" runat="server"></sitefinity:Pager>
