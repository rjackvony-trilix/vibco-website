﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;

namespace SitefinityWebApp.Widgets
{
    public partial class ProductViewDescription : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["productid"]))
            {
                ProductManager productManager = new ProductManager();
                Product product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));

                // Description
                string strHTML = product.Description;
                strHTML = strHTML.Replace("</h2>", "</h2><br />");
                LabelDescription.Text = strHTML;

            }
        }
    }
}