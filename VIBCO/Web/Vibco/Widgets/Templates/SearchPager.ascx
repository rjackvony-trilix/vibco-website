﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sf" %>
<sf:SitefinityHyperLink ID="cmdFirst" runat="server" Text="First" />
<sf:SitefinityHyperLink ID="cmdPrev" runat="server" Text="Previous" />
<div runat="server" id="numeric" class="sf_pagerNumeric"></div>
<sf:SitefinityHyperLink ID="cmdNext" runat="server" Text="44" />
<sf:SitefinityHyperLink ID="cmdLast" runat="server" Text="55" />