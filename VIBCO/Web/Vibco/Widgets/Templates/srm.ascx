﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="srm.ascx.cs" Inherits="SitefinityWebApp.Widgets.Templates.SearchResulstMobile" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Services.Search.Web.UI.Public" TagPrefix="sfSearch" %>

 
<sfSearch:SearchBox ID="topSearchBox" runat="server" />

<h1>Mobile Search Results</h1>

<sitefinity:SitefinityLabel id="resultsStats" runat="server" WrapperTagName="p" CssClass="sfsearchResultStatistics" Text="<%$Resources:SearchResources, SearchResultsStatusMessage %>" />

<asp:Repeater ID="resultsList" runat="server"  OnItemDataBound="resultsList_itemdatabound">
    <HeaderTemplate>
        <dl class="sfsearchResultsWrp sfsearchReultTitleSnippetUrl">
    </HeaderTemplate>
    
    <ItemTemplate>
      <div class="searchResults" id="sresult" runat="server">
        <dt class="sfsearchResultTitle"><a id="A1" runat="server" href='<%# CorrectedLink(Eval("Link").ToString(),Eval("Title").ToString())%>'><%# Eval("Title") %></a></dt>  
		 <br/>
        </div>
    </ItemTemplate>
    
    <FooterTemplate>
        </dl>
    </FooterTemplate>
</asp:Repeater>
<sitefinity:Pager ID="pager" runat="server" LayoutTemplatePath="~/Widgets/Templates/SearchPager.ascx" />
<sfSearch:SearchBox ID="bottomSearchBox" runat="server" />