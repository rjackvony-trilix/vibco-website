﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Telerik.Sitefinity.Services.Search.Data;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Utilities.Lucene.Net.Analysis.Standard;
using Telerik.Sitefinity.Utilities.Lucene.Net.QueryParsers;
using Telerik.Sitefinity.Utilities.Lucene.Net.Search;
using Telerik.Sitefinity.Utilities.Lucene.Net.Store;
using Telerik.Sitefinity.Utilities.Lucene.Net.Util;
using Telerik.Sitefinity.Utilities.Lucene.Net.Index;

using MyLuceneStore = Telerik.Sitefinity.Utilities.Lucene.Net.Store;
using MyLuceneSearch = Telerik.Sitefinity.Utilities.Lucene.Net.Search;
using MyLuceneIndex = Telerik.Sitefinity.Utilities.Lucene.Net.Index;
using MyLuceneDocuments = Telerik.Sitefinity.Utilities.Lucene.Net.Documents;
using MyLuceneAnalysis = Telerik.Sitefinity.Utilities.Lucene.Net.Analysis;
using MyLuceneUtil = Telerik.Sitefinity.Utilities.Lucene.Net.Util;
using MyLuceneQuery = Telerik.Sitefinity.Utilities.Lucene.Net.QueryParsers;

namespace SitefinityWebApp.Widgets
{
    public class VIBCOResultSet : LuceneResultSet
    {
        public VIBCOResultSet(VIBCOSearchProvider provider, string catalogueName, string query, params string[] orderBy)
            : this(provider, catalogueName, query, 0, 0, orderBy)
        {
         
        }
 
         string catalogueName = String.Empty;
         public VIBCOResultSet(VIBCOSearchProvider provider, string catalogueName, string query, int skip, int take, params string[] orderBy)
             : base(provider, catalogueName, query, 0, 0, orderBy)
         {
             this.catalogueName = catalogueName;
             this.query = query;
         }
 
        protected override void ExecuteQuery(bool tryToFix = true)
        {
            /*try
            {
                var path = ((VIBCOSearchProvider)SearchManager.GetManager("").Provider).GetCataloguePath(catalogueName);
                if (System.IO.Directory.Exists(path))
                {*/
            MyLuceneAnalysis.Analyzer analyzer = new MyLuceneAnalysis.KeywordAnalyzer();
            MyLuceneQuery.QueryParser parser = new QueryParser(MyLuceneUtil.Version.LUCENE_29, "Content", analyzer);
            parser.SetAllowLeadingWildcard(true);
            MyLuceneSearch.Query theQuery = parser.Parse("*" + this.query);

            MyLuceneStore.FSDirectory dir = FSDirectory.Open(new DirectoryInfo(@"C:\Users\gmoore\Documents\VIBCO\Web\Vibco\App_Data\Sitefinity\Search\all-vibco\"));
            MyLuceneSearch.Searcher searcher = new IndexSearcher(MyLuceneIndex.IndexReader.Open(dir, true));

            TopScoreDocCollector collector = TopScoreDocCollector.create(100, true);
            searcher.Search(theQuery, collector);
            //can't use this call in 4.1 cause SF sucks.
            //this.hits = searcher.Search(theQuery);
            ScoreDoc[] hits = collector.TopDocs().scoreDocs;

            MyLuceneDocuments.Document[] myDocs = new MyLuceneDocuments.Document[hits.Length];
            for (int i = 0; i < hits.Length; i++)
            {
                int docId = hits[i].doc;
                float score = hits[i].score;

                myDocs[i] = searcher.Doc(docId);
            }

            Results = new List<MyLuceneDocuments.Document>(myDocs);
/*
            this.hits = searcher.Search(queryObj);
            
            for (var iter = 0; iter < this.HitCount; iter++)
            {
                var doc = this.hits.Doc(iter);
            }
                }
            }*/
            /*catch (Exception ex)
            {
                if (tryToFix && ex is ParseException)
                {
                    this.query = this.TryFixQuery(this.query);
                    this.ExecuteQuery(false);
                }
                else
                {
                    this.error = ex;
                    this.hits = null;
                }
            }*/
        }

        public List<MyLuceneDocuments.Document> Results;

        private Exception error;
        private string query;
        private IndexSearcher searcher;
        public void Dispose()
        {
            if (this.searcher != null)
            {
                this.searcher.Close();
                this.searcher = null;
            }
        }
    }
}
