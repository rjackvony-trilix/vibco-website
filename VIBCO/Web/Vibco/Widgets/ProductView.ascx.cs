﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;

namespace SitefinityWebApp.Widgets
{
    public partial class ProductView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProductManager productManager = new ProductManager();
                Product product = null;

                if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                  product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
                else if (!string.IsNullOrEmpty(Request.QueryString["productkey"]))
                  product = productManager.GetProductByKey(Request.QueryString["productkey"]);

                if(product == null)
                  return;
               
                    // Title
                    LabelTitle.Text = product.Title;
                    Page.Title = product.Title;

                    // Main Image
                    string url = productManager.GetImageURL(product, "Image");

                    if (!string.IsNullOrEmpty(url)) imgLabelProduct.ImageUrl = url;
                    else imgLabelProduct.Visible = false;

                    //Spec Image
                    url = productManager.GetImageURL(product, "Spec Image");
                    if (!string.IsNullOrEmpty(url)) imgSpec.ImageUrl = url;
                    else imgSpec.Visible = false;

                    // Accessoires
                    lvAccessoires.DataSource = product.Accessories.Items;
                    lvAccessoires.DataBind();
                    
                    // Documents
                    url = productManager.GetFileURL(product, "Application Bulletin");
                    if (!string.IsNullOrEmpty(url)) hlAppBulletin.NavigateUrl = url;
                    else hlAppBulletin.Visible = false;

                    url = productManager.GetFileURL(product, "Complete Service Manual");
                    if (!string.IsNullOrEmpty(url)) hlCompleteServiceManual.NavigateUrl = url;
                    else hlCompleteServiceManual.Visible = false;

                    url = productManager.GetFileURL(product, "Quick Reference Guides"); //If fails change back to "Quick Reference Guides"
                    if (!string.IsNullOrEmpty(url)) hlQuickReferenceGuide.NavigateUrl = url;
                    else hlQuickReferenceGuide.Visible = false;

                    url = productManager.GetFileURL(product, "Flyer");
                    if (!string.IsNullOrEmpty(url)) hlFlyer.NavigateUrl = url;
                    else hlFlyer.Visible = false;

                    url = productManager.GetFileURL(product, "Catalog");
                    if (!string.IsNullOrEmpty(url)) hlCatalog.NavigateUrl = url;
                    else hlCatalog.Visible = false;

                    
                    lvDimensions.DataSource = product.Dimensions.Items;
                    lvDimensions.DataBind();

                    lvTechData.DataSource = product.TechnicalData.Items;
                    lvTechData.DataBind();
               
                
            }
        }
    }
}