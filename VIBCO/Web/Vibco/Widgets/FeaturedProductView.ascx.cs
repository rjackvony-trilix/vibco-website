﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;

namespace SitefinityWebApp.Widgets
{
    public partial class FeaturedProductView : System.Web.UI.UserControl
    {
        private string strPKey = "";
        private string strPLink = "";

        public string ProductLink
        {
            get { return strPLink; }
            set { strPLink = value; }
        }

        public string ProductKey
        {
            get { return strPKey; }
            set { strPKey = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProductManager productManager = new ProductManager();
                Product product = null;

                //if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                //  product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
                //else if (!string.IsNullOrEmpty(Request.QueryString["productkey"]))
                product = productManager.GetProductByKey(ProductKey);

                if(product == null)
                  return;

                // Title
                LabelTitle.Text = product.Title;

                // Main Image
                string url = productManager.GetImageURL(product, "Image");

                if(!string.IsNullOrEmpty(url)) imgLabelProduct.ImageUrl = url;
                else imgLabelProduct.Visible = false;

                //Product Link
                imgLabelProduct.NavigateUrl = ProductLink + "product?productkey=" + ProductKey;
                HyperLink1.NavigateUrl = ProductLink + "product?productkey=" + ProductKey;
                HyperLink1.Text = "Learn More about the " + ProductKey;
            }
        }
    }
}