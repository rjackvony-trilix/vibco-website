﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchBoxV2.ascx.cs" Inherits="SitefinityWebApp.Widgets.SearchBoxV2" %>
<div class="tbbg">
 <asp:TextBox ID="searchTextBox" CssClass="SearchBox" TextMode="SingleLine" Text="Search..." runat="server"></asp:TextBox>
</div>
<asp:Button ID="searchButton" runat="server" CssClass="SearchBtn" Width="20px" 
    Height="22px" BackColor="Transparent" BorderStyle="None" 
    onclick="searchButton_Click">
</asp:Button>               