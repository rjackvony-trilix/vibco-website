﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductViewReviews.ascx.cs" Inherits="SitefinityWebApp.Widgets.ProductViewReviews" %>

<div id="reviews">
  <h3>Customer <span>Testimonials</span></h3>
    <asp:ListView ID="lvReviews" runat="server">
    <ItemTemplate>
        <p><%# Eval("Value")%></p>
        <p style="text-align: right;">- <%# Eval("ParamName")%></p>
    </ItemTemplate>
    </asp:ListView>
</div>