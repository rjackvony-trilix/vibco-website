﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;

namespace SitefinityWebApp.Widgets
{
  public partial class ProductViewReviews : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProductManager productManager = new ProductManager();
                Product product = null;

                if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                  product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
                else if (!string.IsNullOrEmpty(Request.QueryString["productkey"]))
                  product = productManager.GetProductByKey(Request.QueryString["productkey"]);

                if (product == null)
                  return;

                // Reviews
                lvReviews.DataSource = product.Reviews.Items;
                lvReviews.DataBind();

            }
        }
    }
}