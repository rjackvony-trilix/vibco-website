﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using SitefinityWebApp.Widgets.Admin.ProductEditorFields;
using Telerik.Sitefinity.Modules.GenericContent;

namespace SitefinityWebApp.Widgets.Admin
{
    public partial class ProductEditor : System.Web.UI.UserControl
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Control stepEditor = new Control();
            switch (StepNumber)
            {
                case 1:
                    stepEditor = LoadControl("ProductEditorFields/ProductFieldsEditor.ascx");
                    break;
                case 2:
                    stepEditor = LoadControl("ProductEditorFields/FilesEditor.ascx");
                    break;
            }
            //stepEditor.ID = "Editor";
            EditorPlaceholder.Controls.Add(stepEditor);
            
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.Params.AllKeys.Contains("Content$C000$HiddenFieldProductId"))
            //    HiddenFieldProductId.Value = Request.Params["Content$C000$HiddenFieldProductId"].ToString();

            

            HyperLinkReturn.NavigateUrl = ReturnBackUrl;
           
        }
       

        public Guid ProductId
        {
            get
            {
                /*ProductManager productManager = new ProductManager();
                Product product = null;

                if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                    product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
                else if (!string.IsNullOrEmpty(Request.QueryString["productkey"]))
                    product = productManager.GetProductByKey(Request.QueryString["productkey"]);
                */

                if (string.IsNullOrEmpty(Request.QueryString["productid"]))
                    return Guid.Empty;

                return Guid.Parse(Request.QueryString["productid"]);
               // return product.Id;
            }
        }

        public Guid ProductIdToDuplicate
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["duplicateprodid"]))
                    return Guid.Empty;

                return Guid.Parse(Request.QueryString["duplicateprodid"]);
            }
        }

        public Product DuplicateProduct
        {
            get
            {
                return new Product(ProductManager, ContentManager.GetManager().GetContent(ProductIdToDuplicate));
            }
        }

        public int StepNumber
        {
            get
            {
                int n = 1;
                if(string.IsNullOrEmpty(Request.QueryString["step"]))
                    return n;
                int.TryParse(Request.QueryString["step"], out n);

                return n;
            }
        }

        public bool IsNewProduct
        {
            get
            {
                return _isNewProduct;
            }
            set
            {
                _isNewProduct = value;
            }
        }

        public Product Product
        {
            get
            {
                if(product == null)
                {
                    if (ProductId != Guid.Empty)
                        product = ProductManager.GetProduct(ProductId);
                    else
                        product = new Product();
                }
                return product;
            }
            set
            {
                product = value;
            }
        }

        private Product product;
        private bool _isNewProduct;
        private ProductManager productManager;

        public ProductManager ProductManager
        {
            get
            {
                if (productManager == null)
                    productManager = new ProductManager();
                return productManager;
            }

        }

        //private Guid ProductId
        //{
        //    get
        //    {

        //        if (string.IsNullOrEmpty(HiddenFieldProductId.Value))
        //            return Guid.Empty;
        //        Guid productId;
        //        if (Guid.TryParse(HiddenFieldProductId.Value, out productId))
        //            return productId;

        //        return Guid.Empty;
        //    }
        //    set
        //    {
        //        HiddenFieldProductId.Value = value.ToString();
        //    }
        //}

        public string ReturnBackUrl
        {
            get
            {
                //if(Request.UrlReferrer != null && string.IsNullOrEmpty(HiddenFieldRequestUrlRefferer.Value))
                //    HiddenFieldRequestUrlRefferer.Value = Request.UrlReferrer.ToString();

                return "~/sitefinity/Content/products";
            }
           
        }
    }
}