﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductListEditor" %>
<telerik:RadAjaxManager runat="server" ID="RadAjaxManager" OnAjaxRequest="RadAjaxManager_AjaxRequest" ClientIDMode="Static">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="CategoriesTreeView">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridProducts"></telerik:AjaxUpdatedControl>
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ButtonSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridProducts"></telerik:AjaxUpdatedControl>
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadInputManager ID="RadInputManager" runat="server">
    <telerik:TextBoxSetting EmptyMessage="Search" ClientEvents-OnKeyPress="textBoxSubmit" >
        <TargetControls>
                <telerik:TargetInput ControlID="TextBoxSearch" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>


<script type="text/javascript" language="javascript">
    function UpdateAllChildren(nodes, checked) {
        var i;
        for (i = 0; i < nodes.get_count(); i++) {
            if (checked) {
                nodes.getNode(i).check();
            }
            else {
                nodes.getNode(i).set_checked(false);
            }

            if (nodes.getNode(i).get_nodes().get_count() > 0) {
                UpdateAllChildren(nodes.getNode(i).get_nodes(), checked);
            }
        }
    }
    function clientNodeChecked(sender, eventArgs) {
        var childNodes = eventArgs.get_node().get_nodes();
        var isChecked = eventArgs.get_node().get_checked();
        UpdateAllChildren(childNodes, isChecked);
    }

    function textBoxSubmit(sender, eventArgs) {
        var c = eventArgs.get_keyCode();
        if (c == 13) {
            var ajaxManager = $find("RadAjaxManager");
            ajaxManager.ajaxRequest('search')
            
        }   
    }

    function submit(id, e) {
       // aler(e.keyCode);
        //var isEnter = window.event == null ? 
        //                e.keyCode == 13 : 
        //                window.event.keyCode == 13;
        //if(isEnter)
        //    document.getElementById(id).click();
    }
</script>

<div style="height:2000px">
        <div class="sfContent">
            <div class="sfAllToolsWrapper">
                <div class="sfAllTools">
                    <div class="sfActions">
                        <ul>
                            <li class="sfMainAction">
                                <asp:HyperLink class="sfLinkBtn sfNew" ID="LinkCreateNewProduct" runat="server" NavigateUrl="~/sitefinity/Content/Products/Product-Editor" >
                                    <span class="sfLinkBtnIn">Create a Product</span>
                                </asp:HyperLink>
                            </li>
                            <li class="sfMainAction">
                                <asp:HyperLink class="sfLinkBtn sfNew" ID="LinkCreateNewAccessory" runat="server" NavigateUrl="~/sitefinity/Content/Products/Product-Editor?editor-type=accessory" >
                                    <span class="sfLinkBtnIn">Create an Accessory</span>
                                </asp:HyperLink>
                            </li>
                            
                            <li>
                                <asp:LinkButton ID="ButtonDelete" CssClass="sfLinkBtn" runat="server" onclick="ButtonDelete_Click">
                                    <asp:Label id="LabelSave"  CssClass="sfLinkBtnIn" runat="server">Delete</asp:Label>
                                </asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="ButtonDuplicate" CssClass="sfLinkBtn" runat="server" onclick="ButtonDuplicate_Click">
                                    <asp:Label id="LabelDuplicate"  CssClass="sfLinkBtnIn" runat="server">Duplicate</asp:Label>
                                </asp:LinkButton>
                            </li>
                            <li>
                                <telerik:RadMenu ID="GroupEditorMenu" runat="server" 
                                     
                                     OnItemClick="GroupEditorMenu_ItemClick" EnableEmbeddedScripts="false"
                                    CssClass="RadMenu RadMenu_Sitefinity sfActionsDDL">
                                    <Items>
                                        <telerik:RadMenuItem Text="Group Operation" >
                                            <Items>
                                                <telerik:RadMenuItem Text="Edit Name" />
                                                <telerik:RadMenuItem Text="Edit Description" />
                                                <telerik:RadMenuItem Text="Edit Categories" />
                                                <telerik:RadMenuItem Text="Edit Dimensions" />
                                                <telerik:RadMenuItem Text="Edit Technical Data" />
                                                <telerik:RadMenuItem Text="Edit Accessoires" />
                                                <telerik:RadMenuItem Text="Group Documents" />
                                            </Items>
                                        </telerik:RadMenuItem>
                                    </Items>
                                </telerik:RadMenu>
                            </li>
                            <li>
                                <asp:LinkButton ID="ButtonFindUncategorized" CssClass="sfLinkBtn" runat="server" onclick="ButtonUncat_Click">
                                    <asp:Label id="LabelUncat"  CssClass="sfLinkBtnIn" runat="server">Find Uncategorized Products</asp:Label>
                                </asp:LinkButton>
                            </li>
                            <li>
                                <asp:TextBox runat="server" ID="TextBoxSearch" ></asp:TextBox>
                                <asp:LinkButton ID="ButtonSearch" CssClass="sfLinkBtn" runat="server" ClientIDMode="Static" onclick="ButtonSearch_Click">
                                    <asp:Label id="LabelButtonSearch"  CssClass="sfLinkBtnIn" runat="server">Search</asp:Label>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="sfWorkArea">
                <telerik:RadGrid ID="RadGridProducts" Width="100%" 
                    AllowPaging="True" PageSize="15" runat="server" AllowSorting="true" AllowMultiRowSelection="true"
                     DataSourceID="ProductsDataSource" GridLines="None">
                    <ClientSettings>
                    <Selecting  AllowRowSelect="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Id" RetrieveAllDataFields="false" AutoGenerateColumns="false" Width="100%" >
                        <Columns>
                            <telerik:GridClientSelectColumn 
                                UniqueName="ProductSelectColumn" ItemStyle-Width="15" />
                            <telerik:GridHyperLinkColumn uniqueName="productLink" DataTextField="Title" DataNavigateUrlFields="EditProductUrl" />
                        </Columns>
                    </MasterTableView>
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <FilterMenu EnableTheming="True">
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                    </FilterMenu>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource
                    ID="ProductsDataSource"
                    runat="server"
                    EnablePaging="True"
                    OnSelecting="ProductsDataSource_Selecting"
                    SelectCountMethod="GetProductsCount"
                    SelectMethod="GetBaseProducts" 
                    TypeName="Products.ProductManager">
                        <SelectParameters>
                            <asp:Parameter Name="maximumRows" DbType="Int32" />
                            <asp:Parameter Name="startRowIndex" DbType="Int32" />
                            <asp:Parameter Name="checkedNodes" DbType="Object" />
                            <asp:Parameter Name="title" DbType="String" />
                        </SelectParameters>
                 </asp:ObjectDataSource>
            </div>
        </div>
        <div class="sfSidebar">
            <h2>Categories</h2>
            <telerik:RadTreeView ID="CategoriesTreeView" runat="server" DataSourceID="CategoriesDataSource" 
                DataTextField="Name" DataValueField="Id" DataFieldID="Id" CheckBoxes="true" DataFieldParentID="ParentId"  
                Width="100%" Height="600px" 
                OnNodeCheck="CategoriesTreeView_NodeCheck" OnClientNodeChecked="clientNodeChecked"
                OnNodeDataBound="CategoriesTreeView_NodeDataBound">
                <DataBindings>
                    <telerik:RadTreeNodeBinding Expanded="True" />
                </DataBindings>
            </telerik:RadTreeView>

            <asp:ObjectDataSource ID="CategoriesDataSource" runat="server" 
                TypeName="Products.ProductManager" 
                SelectMethod="GetCategories" />
        </div>
    
</div>