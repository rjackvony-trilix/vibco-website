﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductGroupEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductGroupEditor" %>
<%@ Register src="~/Widgets/Admin/ProductEditorFields/ProductListAttributesEditor.ascx" tagname="ProductListAttributesEditor" tagprefix="vibco" %>
<%@ Register src="~/Widgets/Admin/ProductEditorFields/AccessoriesEditor.ascx" tagname="AccessoriesEditor" tagprefix="vibco" %>
<%@ Register Src="~/Widgets/Admin/ProductEditorFields/CategoriesEditor.ascx" tagname="CategoriesEditor" tagprefix="vibco" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.GenericContent.Web.UI" TagPrefix="sf" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sf" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sf" %>

<fieldset class="sfNewContentForm">
    <div>
        <asp:HyperLink ID="HyperLinkReturn" runat="server" CssClass="sfLinkBtn" >
            <asp:Label ID="Label4" runat="server" CssClass="sfLinkBtnIn" >Return</asp:Label>
        </asp:HyperLink>
        <br />
        <asp:Label ID="LabelWarning" runat="server" Visible="false" ForeColor="Red">Warning! Edited fields in selected products are not the same.</asp:Label>
        <br />
        <div class="sfForm sfFirstForm sfExpandedForm">
        <div class="sfFormIn">
            <div class="sfExpandedTarget">
                <ul>
                    <li class="sfTitleField">
                    <div>
                        <div class="sfFieldWrp">
                            <asp:TextBox ID="TextBoxName" runat="server" Visible="false" ValidationGroup="ProductTitle" CssClass="sfTxt"/>
                        </div>
                        <asp:RequiredFieldValidator runat="server" ID="RfvTextBoxName" Display="Dynamic"
                        ControlToValidate="TextBoxName" CssClass="sfError" 
                        Text="Name field cannot be empty." ValidationGroup="ProductTitle" ></asp:RequiredFieldValidator>
                    </div>
                    </li>
                    <li class="sfTitleField">
                        <div class="sfFieldWrp sfClearfix">
                            
                            <sf:ResourceLinks id="resourcesLinks" runat="server">
                                <sf:ResourceFile Name="Styles/Window.css" />
                            </sf:ResourceLinks>

                            <sf:FormManager ID="formManager" runat="server" />
                            <div style="width: 95%; overflow: hidden;">
                                <sf:HtmlField Visible="false" 
                                    ID="HtmlEditorDescription" 
                                    runat="server" 
                                    Width="99%"
                                    Height="370px" CssClass="Sitefinity"
                                    EditorContentFilters="DefaultFilters"
                                    EditorStripFormattingOptions="MSWord,Css,Font,Span,ConvertWordLists"
                                    DisplayMode="Write">
                                </sf:HtmlField>
                            </div>
                            <script type="text/javascript">
                                $("body").addClass("sfContentBlockDesigner");
                            </script>
                            

                        </div>
                    </li>
                    <li class="sfEditorWrp sfClearfix">
                       <vibco:CategoriesEditor ID="CategoriesEditor" runat="server" Visible="false" />
                    </li>
                    <li class="sfEditorWrp sfClearfix">
                        <div class="sfFieldWrp">
                            <vibco:ProductListAttributesEditor Visible="false" ID="DimensionAttributesEditor" runat="server" AttributeName="Dimensions" />
                        </div>
                    </li>
                    <li class="sfEditorWrp sfClearfix">
                        <div class="sfFieldWrp">
                            <vibco:ProductListAttributesEditor Visible="false" ID="TechnicalDataAttributesEditor" runat="server" AttributeName="Technical Data" />
                        </div>
                    </li>
                    <li class="sfEditorWrp sfClearfix">
                        <div class="sfFieldWrp">
                            <vibco:AccessoriesEditor ID="AccessoriesEditor" Visible="false" runat="server" />
                        </div>
                    </li>
                    <li class="sfEditorWrp sfClearfix">
                        <div >
                             <telerik:RadProgressManager id="RadProgressManager1" runat="server" SuppressMissingHttpModuleError="true" />
                                <telerik:RadProgressArea runat="server" ID="progressArea" EnableEmbeddedSkins="false" Skin="Sitefinity"></telerik:RadProgressArea>
                                   <%-- <div class="clear"><!-- --></div>--%>
                                    <asp:Label ID="Label1" runat="server" Visible="false">Please upload a CSV file</asp:Label>
                                    <telerik:RadUpload ID="RadUploadCSV" runat="server" AllowedFileExtensions=".csv" 
                                                    MaxFileInputsCount="1" 
                                                    Width="100%" 
                                                    InputSize="45" 
                                                    ControlObjectsVisibility="None" style=" margin:30px" TargetFolder="~/Widgets/Admin/Temp" Visible="false" />
                                    <asp:Label ID="Label2" runat="server" Visible="false">Please upload a DOC file</asp:Label>
                                <telerik:RadUpload ID="RadUploadDocs" runat="server" AllowedFileExtensions=".pdf,.csv" 
                                                    MaxFileInputsCount="1" 
                                                    Width="100%" 
                                                    InputSize="45" 
                                                    ControlObjectsVisibility="None" style=" margin:30px" TargetFolder="~/Widgets/Admin/Temp" Visible="false" />
                                <telerik:RadComboBox id="selectID" runat="server" Visible="false" >
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="Application Bulletin" />
                                        <telerik:RadComboBoxItem runat="server" Text="Quick Reference Guides" />
                                        <telerik:RadComboBoxItem runat="server" Text="Complete Service Manual" />
                                        <telerik:RadComboBoxItem runat="server" Text="Flyer" />
                                        <telerik:RadComboBoxItem runat="server" Text="Catalog" />
                                        <telerik:RadComboBoxItem runat="server" Text="Testimonials" />
                                    </Items>
                                </telerik:RadComboBox>
                    
                        </div>   
                    </li>
                </ul>
            </div>
         </div>
    </div>
    
    </div>

    <asp:LinkButton ID="ButtonSaveReturn" ValidationGroup="ProductTitle" CssClass="sfLinkBtn sfSave" runat="server" onclick="ButtonSave_Click">
        <asp:Label id="LabelSaveReturn"  CssClass="sfLinkBtnIn" runat="server">Save & Return</asp:Label>
    </asp:LinkButton>

    <asp:HyperLink ID="HyperLinkCancel" runat="server" CssClass="sfLinkBtn" >
        <asp:Label ID="LabelLinkCancel" runat="server" CssClass="sfLinkBtnIn" >Cancel</asp:Label>
    </asp:HyperLink>

</fieldset>
<br />
<br />
<br />