﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;

namespace SitefinityWebApp.Widgets.Admin
{
    public partial class BackDateNews : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonNews_Click(object sender, EventArgs e)
        {
            using (var newsAll = App.WorkWith())
            {
                newsAll.NewsItems()
                 .Where(nI => nI.Status == ContentLifecycleStatus.Live)
                 .ForEach(nI =>
                 {
                     var val = nI.GetValue("RealPublicationDate");
                     if (val != null)
                     {
                         String date = val.ToString();
                         DateTime ss;
                         DateTime.TryParse(date, out ss);
                         nI.PublicationDate = ss;
                     }
                 })
                 .SaveChanges();
            }
        }
    }
}