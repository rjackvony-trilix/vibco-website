﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Products;
using Products.Migration;

namespace SitefinityWebApp.Widgets.Admin
{
    public partial class Setup : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSetup_Click(object sender, EventArgs e)
        {
            ProductManager productsManager = new ProductManager();
            productsManager.ProductSetup.Setup();
        }

        protected void ButtonUpdate1_Click(object sender, EventArgs e)
        {
            ProductManager productsManager = new ProductManager();
            productsManager.ProductSetup.Update1();
        }

        protected void ButtonUpdate2_Click(object sender, EventArgs e)
        {
            ProductManager productsManager = new ProductManager();
            productsManager.ProductSetup.Update2();
        }

        protected void ButtonUpdate3_Click(object sender, EventArgs e)
        {
            ProductManager productsManager = new ProductManager();
            productsManager.ProductSetup.Update3();
        }

        protected void ButtonMigration_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration1();
        }

        protected void ButtonMigration21_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(1);
        }

        protected void ButtonMigration22_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(2);
        }

        protected void ButtonMigration23_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(3);
        }
        
        protected void ButtonMigration24_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(4);
        }

        protected void ButtonMigration25_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(5);
        }

        protected void ButtonMigration26_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration2(6);
        }

        protected void ButtonMigration3_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigration3();
        }

        protected void ButtonMigrationUpdate1_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigrationUpdate1();
        }

        protected void ButtonMigrationUpdate2_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigrationUpdate2();
        }

        protected void ButtonMigrationUpdate3_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigrationUpdate3();
        }

        protected void ButtonMigrationUpdate4_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigrationUpdate4();
        }

        protected void ButtonMigrationUpdate5_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoMigrationUpdate5();
        }

        protected void ButtonUpdate4_Click(object sender, EventArgs e)
        {
            ProductManager productsManager = new ProductManager();
            productsManager.ProductSetup.Update4();
        }

        protected void ButtonUpdate5_Click(object sender, EventArgs e)
        {
          ProductManager productsManager = new ProductManager();
          productsManager.ProductSetup.Update5();
        }

        protected void ButtonMigrateAccessoires_Click(object sender, EventArgs e)
        {
            MigrationManager migrationManager = new MigrationManager();
            migrationManager.DoAccessoiresMigration();
        }
    }
}