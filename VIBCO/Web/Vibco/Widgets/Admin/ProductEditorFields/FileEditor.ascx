﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.FileEditor" %>

 <div>
    <asp:Label ID="LabelTitle" runat="server" CssClass="sfTxtLbl" />
    <div class="sfFieldWrp">
     <telerik:RadTabStrip ID="TabStrip" runat="server"  MultiPageID="RadMultiPage1"
                SelectedIndex="0" Align="Left">
        <Tabs>
            <telerik:RadTab Text="Select from existing">
            </telerik:RadTab>
            <telerik:RadTab Text="Upload new">
            </telerik:RadTab>
        </Tabs>
     </telerik:RadTabStrip>
     <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px">
        <telerik:RadPageView ID="RadPageViewSelect" runat="server">
         <telerik:RadComboBox ID="FileComboBox" AppendDataBoundItems="true" Height="200px" 
                Width="200px" style="margin:30px" runat="server" 
                            EmptyMessage="Select a Document" 
                            EnableLoadOnDemand="true" 
                            ShowMoreResultsBox="false"
                            EnableVirtualScrolling="false" 
                onselectedindexchanged="FileComboBox_SelectedIndexChanged">  
                                <WebServiceSettings Path="~/Services/ProductService.asmx" />
                        </telerik:RadComboBox>
        </telerik:RadPageView>
                
        <telerik:RadPageView ID="RadPageViewUpload" runat="server">
            <telerik:RadUpload ID="RadUploadDocs" runat="server" AllowedFileExtensions=".pdf" 
                                MaxFileInputsCount="1" 
                                Width="100%" 
                                InputSize="45" 
                                ControlObjectsVisibility="None" style=" margin:30px"/>
        </telerik:RadPageView>
        
     </telerik:RadMultiPage>
                           
    </div>

    <asp:HyperLink ID="hl" runat="server">Click to open</asp:HyperLink>
    <asp:Label ID="hlDelimiter" runat="server" Text="Label"> | </asp:Label>
    <asp:LinkButton ID="hlRemove" runat="server" onclick="hlRemove_Click">Remove file</asp:LinkButton>
</div>