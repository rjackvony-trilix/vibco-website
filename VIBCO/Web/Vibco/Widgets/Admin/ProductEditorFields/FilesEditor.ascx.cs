﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Products;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.GenericContent;
using System.IO;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Libraries.Model;

namespace SitefinityWebApp.Widgets.Admin.ProductEditorFields
{
    public partial class FilesEditor : System.Web.UI.UserControl/*, IScriptControl*/
    {
        private ScriptManager sm;
        private bool DuplicateProduct = false;
        /*
        protected override void OnPreRender(EventArgs e)
        {
            if (!this.DesignMode)
            {
                sm = RadScriptManager.GetCurrent(this.Page);

                if (sm == null)
                    throw new HttpException("ScriptManager not found");

                sm.RegisterScriptControl(this);
            }
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.DesignMode)
                sm.RegisterScriptDescriptors(this);

            base.Render(writer);
        }
        */
        /*protected virtual IEnumerable<ScriptReference> GetScriptReferences()
        {
            //This is where you load the javascript file just like you would in a javascript control.
            ScriptReference imageReference = new ScriptReference();
            imageReference.Path = "~/App_Themes/VIBCOOrange/Global//FilesEditor.js";

            return new ScriptReference[] { imageReference };
        }

        //Must be implemented to use the IScriptControl interface.
        protected virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            //Must declare your control as the client.  Normaally you would put the custom control in here but instead you would put the client ID of your control
            ScriptControlDescriptor imageDescriptor = new ScriptControlDescriptor("SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor", this.ClientID);
            //Send the MediaContentSelectorView clientside object to the page.
            imageDescriptor.AddComponentProperty("selectorView", selectorView.ClientID);

            return new ScriptDescriptor[] { imageDescriptor };
        }

        IEnumerable<ScriptReference> IScriptControl.GetScriptReferences()
        {
            return GetScriptReferences();
        }

        IEnumerable<ScriptDescriptor> IScriptControl.GetScriptDescriptors()
        {
            return GetScriptDescriptors();
        }
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            HyperLinkCancel.NavigateUrl = ProductEditor.ReturnBackUrl;

            if (ProductEditor != null)
            {
                Product product = null;
                if (ProductEditor.Product.Categories.Where(p => p.Id == ProductSetup.ProductAccessoriesCategoryId).Count() > 0)
                {
                    PanelFiles.Visible = false;
                    PanelSpecImage.Visible = false;
                }

                if (ProductEditor.ProductIdToDuplicate != Guid.Empty)
                {
                    product = ProductEditor.DuplicateProduct;
                    DuplicateProduct = true;
                }
                else
                    product = ProductEditor.Product;

                // Images
                string url = ProductEditor.ProductManager.GetImageURL(product, "Image");

                if (!string.IsNullOrEmpty(url)) imgLabelProduct.ImageUrl = url;
                else imgLabelProduct.Visible = false;

                url = ProductEditor.ProductManager.GetImageURL(product, "Spec Image");

                if (!string.IsNullOrEmpty(url)) imgLabelProductSpec.ImageUrl = url;
                else imgLabelProductSpec.Visible = false;
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (ProductEditor != null)
            {
                Product product = ProductEditor.Product;
                product.Id = ProductEditor.ProductId;

                if (RadUploadImage.UploadedFiles.Count > 0)
                {
                    ProductUploadedFile file = new ProductUploadedFile()
                    {
                        FileName = RadUploadImage.UploadedFiles[0].FileName,
                        Extension = RadUploadImage.UploadedFiles[0].GetExtension(),
                        InputStream = RadUploadImage.UploadedFiles[0].InputStream
                    };
                    product.RelatedProductFiles.AddImageToUpload("Image", file);
                }
                else
                {
                    if (DuplicateProduct && imgLabelProduct.ImageUrl != null)
                    {
                        FillImageFromDupProd("Image");
                    }
                }
                
                if (RadUploadSpecImage.UploadedFiles.Count > 0)
                {
                    ProductUploadedFile file = new ProductUploadedFile()
                    {
                        FileName = RadUploadSpecImage.UploadedFiles[0].FileName,
                        Extension = RadUploadSpecImage.UploadedFiles[0].GetExtension(),
                        InputStream = RadUploadSpecImage.UploadedFiles[0].InputStream
                    };
                    product.RelatedProductFiles.AddImageToUpload("Spec Image", file);
                }
                else
                {
                    if (DuplicateProduct && imgLabelProductSpec.ImageUrl != null)
                    {
                        FillImageFromDupProd("Spec Image");
                    }
                }

                if (ApplicationBulletinEditor.SelectedTab == "Select from existing")
                {
                    if (ApplicationBulletinEditor.SelectedId != Guid.Empty)
                    {
                        product.RelatedProductFiles.DeleteFile(ApplicationBulletinEditor.Type);
                        product.RelatedProductFiles.Add(ApplicationBulletinEditor.Type, ApplicationBulletinEditor.SelectedId, ProductFileType.File);
                    }
                    else
                    {
                        Guid selectedId;
                        if (DuplicateProduct && (selectedId = GetDocumentFromDB(ApplicationBulletinEditor.Type)) != Guid.Empty)
                        {
                            product.RelatedProductFiles.DeleteFile(ApplicationBulletinEditor.Type);
                            product.RelatedProductFiles.Add(ApplicationBulletinEditor.Type, selectedId, ProductFileType.File);
                        }
                    }
                }
                else
                {
                    if (ApplicationBulletinEditor.UploadedFiles.Count > 0)
                    {
                        ProductUploadedFile file = new ProductUploadedFile()
                        {
                            FileName = ApplicationBulletinEditor.UploadedFiles[0].FileName,
                            Extension = ApplicationBulletinEditor.UploadedFiles[0].GetExtension(),
                            InputStream = ApplicationBulletinEditor.UploadedFiles[0].InputStream
                        };

                        product.RelatedProductFiles.AddFileToUpload(ApplicationBulletinEditor.Type,
                           file);
                    }
                }

                if (CompleteServiceManualEditor.SelectedTab == "Select from existing")
                {
                    if (CompleteServiceManualEditor.SelectedId != Guid.Empty)
                    {
                        product.RelatedProductFiles.DeleteFile(CompleteServiceManualEditor.Type);
                        product.RelatedProductFiles.Add(CompleteServiceManualEditor.Type, CompleteServiceManualEditor.SelectedId, ProductFileType.File);
                    }
                    else
                    {
                        Guid selectedId;
                        if (DuplicateProduct && (selectedId = GetDocumentFromDB(CompleteServiceManualEditor.Type)) != Guid.Empty)
                        {
                            product.RelatedProductFiles.DeleteFile(CompleteServiceManualEditor.Type);
                            product.RelatedProductFiles.Add(CompleteServiceManualEditor.Type, selectedId, ProductFileType.File);
                        }
                    }
                }
                else
                {
                    if (CompleteServiceManualEditor.UploadedFiles.Count > 0)
                    {
                        ProductUploadedFile file = new ProductUploadedFile()
                        {
                            FileName = CompleteServiceManualEditor.UploadedFiles[0].FileName,
                            Extension = CompleteServiceManualEditor.UploadedFiles[0].GetExtension(),
                            InputStream = CompleteServiceManualEditor.UploadedFiles[0].InputStream
                        };

                        product.RelatedProductFiles.AddFileToUpload(CompleteServiceManualEditor.Type,
                           file);
                    }
                }

                if (QuickReferenceGuideEditor.SelectedTab == "Select from existing")
                {
                    if (QuickReferenceGuideEditor.SelectedId != Guid.Empty)
                    {
                        product.RelatedProductFiles.DeleteFile(QuickReferenceGuideEditor.Type);
                        product.RelatedProductFiles.Add(QuickReferenceGuideEditor.Type, QuickReferenceGuideEditor.SelectedId, ProductFileType.File);
                    }
                    else
                    {
                        Guid selectedId;
                        if (DuplicateProduct && (selectedId = GetDocumentFromDB(QuickReferenceGuideEditor.Type)) != Guid.Empty)
                        {
                            product.RelatedProductFiles.DeleteFile(QuickReferenceGuideEditor.Type);
                            product.RelatedProductFiles.Add(QuickReferenceGuideEditor.Type, selectedId, ProductFileType.File);
                        }
                    }
                }
                else
                {
                    if (QuickReferenceGuideEditor.UploadedFiles.Count > 0)
                    {
                        ProductUploadedFile file = new ProductUploadedFile()
                        {
                            FileName = QuickReferenceGuideEditor.UploadedFiles[0].FileName,
                            Extension = QuickReferenceGuideEditor.UploadedFiles[0].GetExtension(),
                            InputStream = QuickReferenceGuideEditor.UploadedFiles[0].InputStream
                        };

                        product.RelatedProductFiles.AddFileToUpload(QuickReferenceGuideEditor.Type,
                           file);
                    }
                }

                if (FlyerEditor.SelectedTab == "Select from existing")
                {
                    if (FlyerEditor.SelectedId != Guid.Empty)
                    {
                        product.RelatedProductFiles.DeleteFile(FlyerEditor.Type);
                        product.RelatedProductFiles.Add(FlyerEditor.Type, FlyerEditor.SelectedId, ProductFileType.File);
                    }
                    else
                    {
                        Guid selectedId;
                        if (DuplicateProduct && (selectedId = GetDocumentFromDB(FlyerEditor.Type)) != Guid.Empty)
                        {
                            product.RelatedProductFiles.DeleteFile(FlyerEditor.Type);
                            product.RelatedProductFiles.Add(FlyerEditor.Type, selectedId, ProductFileType.File);
                        }
                    }
                }
                else
                {
                    if (FlyerEditor.UploadedFiles.Count > 0)
                    {
                        ProductUploadedFile file = new ProductUploadedFile()
                        {
                            FileName = FlyerEditor.UploadedFiles[0].FileName,
                            Extension = FlyerEditor.UploadedFiles[0].GetExtension(),
                            InputStream = FlyerEditor.UploadedFiles[0].InputStream
                        };

                        product.RelatedProductFiles.AddFileToUpload(FlyerEditor.Type,
                           file);
                    }
                }

                if (CatalogEditor.SelectedTab == "Select from existing")
                {
                    if (CatalogEditor.SelectedId != Guid.Empty)
                    {
                        product.RelatedProductFiles.DeleteFile(CatalogEditor.Type);
                        product.RelatedProductFiles.Add(CatalogEditor.Type, CatalogEditor.SelectedId, ProductFileType.File);
                    }
                    else
                    {
                        Guid selectedId;
                        if (DuplicateProduct && (selectedId = GetDocumentFromDB(CatalogEditor.Type)) != Guid.Empty)
                        {
                            product.RelatedProductFiles.DeleteFile(CatalogEditor.Type);
                            product.RelatedProductFiles.Add(CatalogEditor.Type, selectedId, ProductFileType.File);
                        }
                    }
                }
                else
                {
                    if (CatalogEditor.UploadedFiles.Count > 0)
                    {
                        ProductUploadedFile file = new ProductUploadedFile()
                        {
                            FileName = CatalogEditor.UploadedFiles[0].FileName,
                            Extension = CatalogEditor.UploadedFiles[0].GetExtension(),
                            InputStream = CatalogEditor.UploadedFiles[0].InputStream
                        };

                        product.RelatedProductFiles.AddFileToUpload(CatalogEditor.Type,
                           file);
                    }
                }

                ProductEditor.ProductManager.UploadFiles(product, ProductEditor.IsNewProduct);

                Response.Redirect(ProductEditor.ReturnBackUrl);
            }
            
        }

        protected Guid GetDocumentFromDB(string key)
        {
            Product origProd = ProductEditor.DuplicateProduct;

            //fill duplicated product with image from original product

            ProductFile pf = origProd.RelatedProductFiles.GetProductDocument(key);

            Document d = App.WorkWith().Document(pf.FileId).Get();
            if (d != null)
                return d.Id;
            else
                return Guid.Empty;

        }

        protected void FillImageFromDupProd(string key)
        {
            Product origProd = ProductEditor.DuplicateProduct;

            //fill duplicated product with image from original product

            ProductFile pf = origProd.RelatedProductFiles.GetProductImage(key);

            Guid imageId = Guid.Empty;
            Telerik.Sitefinity.Libraries.Model.Image img = App.WorkWith().Image(pf.FileId).Get();

            System.Net.WebRequest req = System.Net.WebRequest.Create("http://" + HttpContext.Current.Request.Url.Authority + img.MediaUrl);
            System.Net.WebResponse response = req.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            MemoryStream ms = new MemoryStream((int)response.ContentLength);

            const int BufferLength = 1000;
            byte[] b = new byte[BufferLength]; //Buffer
            int cnt = 0;

            do
            {
                //Read up to 1000 bytes from the response stream
                cnt = responseStream.Read(b, 0, BufferLength);

                //Write the number of bytes actually read
                ms.Write(b, 0, cnt);
            }
            while (cnt > 0);

            App.WorkWith()
                .Album(ProductSetup.ThumbnailsAlbumId)
                .CreateImage()
                .Do(i =>
                {

                    i.Title = string.Concat(ProductEditor.Product.Title, " ", pf.Key);
                    imageId = i.Id;
                })
                .CheckOut()
                .UploadContent(ms, img.Extension)
                .CheckIn().Publish()
                .SaveChanges();

            ProductEditor.Product.RelatedProductFiles.Add(pf.Key, imageId, ProductFileType.Image);
        }

        ProductEditor ProductEditor
        {
            get
            {
                ProductEditor pEditor = this.Parent.Parent as ProductEditor;
                return pEditor;
            }
        }
    }
 }