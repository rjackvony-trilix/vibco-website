﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoriesEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.CategoriesEditor" %>
<telerik:RadTreeView ID="RadTreeView" runat="server" DataSourceID="ObjectDataSource" 
    DataTextField="Name" DataValueField="Id" DataFieldID="Id" CheckBoxes="true" DataFieldParentID="ParentId"  
    Width="300px" Height="250px" OnNodeDataBound="RadTreeView_NodeDataBound">
    <DataBindings>
        <telerik:RadTreeNodeBinding Expanded="True" />
    </DataBindings>
</telerik:RadTreeView>

<asp:ObjectDataSource ID="ObjectDataSource" runat="server" TypeName="Products.ProductManager" SelectMethod="GetCategories" />