﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using Telerik.Web.UI;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Libraries.Model;

namespace SitefinityWebApp.Widgets.Admin.ProductEditorFields
{
    public partial class FileEditor : System.Web.UI.UserControl
    {

        private bool DuplicateProduct = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ProductEditor != null)
                {
                    Product product = null;
                    if (ProductEditor.ProductIdToDuplicate != Guid.Empty)
                    {
                        product = ProductEditor.DuplicateProduct;
                        DuplicateProduct = true;
                    }
                    else
                        product = ProductEditor.Product;

                    string url = ProductEditor.ProductManager.GetFileURL(product, Type);

                    Guid id = ProductEditor.Product.RelatedProductFiles.GetId(Type);
                    string title = ProductEditor.ProductManager.GetFileName(product, Type);
                    if (!string.IsNullOrEmpty(title))
                        FileComboBox.Text = title;

                    if (!string.IsNullOrEmpty(url))
                        hl.NavigateUrl = url;
                    else
                    {
                        hl.Visible = false;
                        hlDelimiter.Visible = false;
                        hlRemove.Visible = false;
                    }
                }
            }
            
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                LabelTitle.Text = string.Concat(value, ":") ;
            }
        }
        private string type;

        public string Method 
        {
            get { return FileComboBox.WebServiceSettings.Method; }
            set { FileComboBox.WebServiceSettings.Method = value; }
        }

        protected void hlRemove_Click(object sender, EventArgs e)
        {
            if (ProductEditor != null)
            {
                Product product = ProductEditor.Product;
                product.RelatedProductFiles.DeleteFile(Type);
                ProductEditor.ProductManager.SaveProductChanges(product, true);
                //ProductEditor.ProductManager.DeleteFile(product, Type);
                FileComboBox.SelectedValue = "";
                FileComboBox.Text = "";
                hl.Visible = false;
                hlDelimiter.Visible = false;
                hlRemove.Visible = false;
            }
        }

        protected ProductEditor ProductEditor
        {
            get
            {
                ProductEditor pEditor = this.Parent.Parent.Parent.Parent as ProductEditor;
                return pEditor;
            }
        }

        public string SelectedTab
        {
            get
            {
                return TabStrip.SelectedTab.Text;
            }
        }

        public Guid SelectedId
        {
            get
            {
                if (String.IsNullOrEmpty(FileComboBox.SelectedValue))
                    return Guid.Empty;
                return new Guid(FileComboBox.SelectedValue);
            }
        }

        public UploadedFileCollection UploadedFiles
        {
            get
            {
                return RadUploadDocs.UploadedFiles;
            }
        }

        protected void FileComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
        }
    }
}