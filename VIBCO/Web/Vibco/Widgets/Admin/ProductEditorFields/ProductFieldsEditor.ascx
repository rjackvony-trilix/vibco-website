﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFieldsEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.ProductFieldsEditor" %>
<%@ Register src="ProductListAttributesEditor.ascx" tagname="ProductListAttributesEditor" tagprefix="vibco" %>
<%@ Register src="ProductReviewsEditor.ascx" tagname="ProductReviewsEditor" tagprefix="vibco" %>
<%@ Register src="AccessoriesEditor.ascx" tagname="AccessoriesEditor" tagprefix="vibco" %>
<%@ Register Src="~/Widgets/Admin/ProductEditorFields/CategoriesEditor.ascx" tagname="CategoriesEditor" tagprefix="vibco" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.GenericContent.Web.UI" TagPrefix="sf" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sf" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sf" %>

<div class="sfForm sfFirstForm sfExpandedForm">
    <div class="sfFormIn">
        <h2>Basic Fields</h2>
        <div class="sfExpandedTarget">
            <ul>
                <li class="sfTitleField">
                    <div>
                        <asp:Label ID="LabelName" runat="server" Text="Name:" CssClass="sfTxtLbl" />
                        <div class="sfFieldWrp">
                            <asp:TextBox ID="TextBoxName" runat="server" ValidationGroup="ProductTitle" CssClass="sfTxt"/>
                        </div>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="RfvTextBoxName" Display="Dynamic"
                        ControlToValidate="TextBoxName" CssClass="sfError" 
                        Text="Name field cannot be empty." ValidationGroup="ProductTitle" ></asp:RequiredFieldValidator>
                    
                </li>
                <li class="sfTitleField">
                    <div>
                        <asp:Label ID="LabelKey" runat="server" Text="Key:" CssClass="sfTxtLbl" />
                        <div class="sfFieldWrp">
                            <asp:TextBox ID="TextBoxKey" runat="server" ValidationGroup="ProductTitle" CssClass="sfTxt"/>
                        </div>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="RfvTextBoxKey" Display="Dynamic"
                        ControlToValidate="TextBoxKey" CssClass="sfError" 
                        Text="Key field cannot be empty." ValidationGroup="ProductTitle" ></asp:RequiredFieldValidator>
                    <asp:CustomValidator runat="server" ID="CvTextBoxTitle" Display="Dynamic" 
                        ControlToValidate="TextBoxName" 
                        OnServerValidate="CvTextBoxTitle_ServerValidate" ValidationGroup="ProductTitle" CssClass="sfError"
                         ErrorMessage="Product Name and Key pair must be unique." />
                </li>
                <li class="sfEditorWrp sfClearfix sfFormSeparator sfContentField">
                    <div>
                        <asp:Label ID="LabelDescription" runat="server" Text="Description:" CssClass="sfTxtLbl" />
                        <div class="sfFieldWrp sfClearfix">
                            
                            <sf:ResourceLinks id="resourcesLinks" runat="server">
                                <sf:ResourceFile Name="Styles/Window.css" />
                            </sf:ResourceLinks>

                            <sf:FormManager ID="formManager" runat="server" />
                            <div style="width: 95%; overflow: hidden;">
                                <sf:HtmlField 
                                    ID="HtmlEditorDescription" 
                                    runat="server" 
                                    Width="99%"
                                    Height="370px" CssClass="Sitefinity"
                                    EditorContentFilters="DefaultFilters"
                                    EditorStripFormattingOptions="MSWord,Css,Font,Span,ConvertWordLists"
                                    DisplayMode="Write">
                                </sf:HtmlField>
                            </div>
                            <script type="text/javascript">
                                $("body").addClass("sfContentBlockDesigner");
                            </script>
                            

                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div runat="server" id="PanelCategoriesEditor" class="sfForm sfFormSeparator sfExpandedForm">
    <div class="sfFormIn">
        <h2>Categories</h2>
        <ul>
            <li class="sfEditorWrp sfClearfix">
               <vibco:CategoriesEditor ID="CategoriesEditor" runat="server" />
               <asp:Label ID="lblInvalidCategories" runat="server" CssClass="sfError" Visible="false" Text="At least one category is required."/>
            </li>
        </ul>
    </div>
</div>

<div runat="server" id="PanelListAttributes" class="sfForm sfExpandedForm">
    <div class="sfFormIn">
        <h2>Advanced Fields</h2>
        <ul>
            <li class="sfEditorWrp sfClearfix">
                <asp:Label ID="LabelDimensions" runat="server" Text="Dimensions:" CssClass="sfTxtLbl" />
                <div class="sfFieldWrp">
                    <vibco:ProductListAttributesEditor ID="DimensionAttributesEditor" runat="server" AttributeName="Dimensions" />
                </div>
            </li>
            <li class="sfEditorWrp sfFormSeparator sfClearfix">
                <asp:Label ID="LabelTechnicalData" runat="server" Text="Technical Data:" CssClass="sfTxtLbl" />
                <div class="sfFieldWrp">
                    <vibco:ProductListAttributesEditor ID="TechnicalDataAttributesEditor" runat="server" AttributeName="Technical Data" />
                </div>
            </li>
            <li class="sfEditorWrp sfFormSeparator sfClearfix">
                <asp:Label ID="LabelReviews" runat="server" Text="Reviews:" CssClass="sfTxtLbl" />
                <div class="sfFieldWrp">
                    <vibco:ProductReviewsEditor ID="ReviewsEditor" runat="server" AttributeName="Reviews" />
                </div>
            </li>
            <li class="sfEditorWrp sfFormSeparator sfClearfix">
                <asp:Label ID="LabelAccessories" runat="server" Text="Accessories:" CssClass="sfTxtLbl" />
                <div class="sfFieldWrp">
                    <vibco:AccessoriesEditor ID="AccessoriesEditor" runat="server" />
                </div>
            </li>
        </ul>
    </div>
</div>

<br />
<asp:LinkButton ID="ButtonSave" ValidationGroup="ProductTitle" CssClass="sfLinkBtn sfSave" runat="server" onclick="ButtonSave_Click">
    <asp:Label id="LabelSave"  CssClass="sfLinkBtnIn" runat="server">Save & Continue</asp:Label>
</asp:LinkButton>

<asp:LinkButton ID="ButtonSaveReturn" ValidationGroup="ProductTitle" CssClass="sfLinkBtn sfSave" runat="server" onclick="ButtonSave_Click">
    <asp:Label id="LabelSaveReturn"  CssClass="sfLinkBtnIn" runat="server">Save & Return</asp:Label>
</asp:LinkButton>

<asp:HyperLink ID="HyperLinkCancel" runat="server" CssClass="sfLinkBtn" >
    <asp:Label ID="LabelLinkCancel" runat="server" CssClass="sfLinkBtnIn" >Cancel</asp:Label>
</asp:HyperLink>