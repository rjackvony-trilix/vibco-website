﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoriesEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.AccessoriesEditor" %>
 
<telerik:RadAjaxPanel runat="server" ID="Panel">   
    <div>
        <telerik:RadComboBox ID="NewAccessoryComboBox" AppendDataBoundItems="true" Height="200px" Width="200px" runat="server" 
                            EmptyMessage="Select a Product" 
                            EnableLoadOnDemand="true" 
                            ShowMoreResultsBox="false"
                            EnableVirtualScrolling="false" 
                            SelectedValue='<%# Bind("Id")%>'>  
                                <WebServiceSettings Method="GetAccessories" Path="~/Services/ProductService.asmx" />
                        </telerik:RadComboBox>
        <br />
        <asp:Button ID="AddProductButton" runat="server" Text="Add" 
            onclick="AddProductButton_Click" />
    </div>
    <br />
    <telerik:RadGrid ID="RadGridList"  Width="100%" runat="server"
         DataSourceID="ObjectDataSource1" OnItemCommand="RadGridList_ItemCommand"
         AllowAutomaticInserts="false" AllowAutomaticDeletes="true" AutoGenerateColumns="false"
         GridLines="Horizontal" >
    
        <ValidationSettings EnableValidation="false" />
        <MasterTableView AllowAutomaticInserts="false" DataKeyNames="Id" CommandItemDisplay="Top" Width="100%"  >
            <CommandItemSettings ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Product Name" UniqueName="ProductNameColumn">  
                <EditItemTemplate> 
                    <telerik:RadComboBox ID="RadComboBox1" AppendDataBoundItems="true" Height="200px" runat="server" 
                        EmptyMessage="Select a Product" 
                        EnableLoadOnDemand="true" 
                        ShowMoreResultsBox="false"
                        EnableVirtualScrolling="false" 
                        SelectedValue='<%# Bind("Id")%>'>  
                            <WebServiceSettings Method="GetAccessories" Path="../../../Services/ProductService.asmx" />
                    </telerik:RadComboBox> 
                </EditItemTemplate> 
                <ItemTemplate> 
                    <%#Eval("Name")%> 
                </ItemTemplate> 
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this product?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="LinkButton" CommandName="Delete" Text="Delete"
                            UniqueName="DeleteColumn" />
            </Columns>
        </MasterTableView>
        
    </telerik:RadGrid>

    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    InsertMethod="Insert" SelectMethod="Select" DeleteMethod="Delete"
     OnInserting="ObjectDataSource1_Inserting" OnInserted="ObjectDataSource1_Inserted" OnObjectCreating="ObjectDataSource1_ObjectCreating"
    TypeName="Products.ProductAccessoriesManager" >
        <InsertParameters>
            <asp:Parameter Name="Id" Type="String" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Object" />
        </DeleteParameters>
    </asp:ObjectDataSource>
 </telerik:RadAjaxPanel>