﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilesEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.FilesEditor" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.Libraries.Web.UI.Designers" TagPrefix="sf" %>
<%@ Register src="FileEditor.ascx" tagname="FileEditor" tagprefix="vibco" %>

<!--<telerik:RadScriptManager ID="manager" runat="server"></telerik:RadScriptManager>-->
<div class="sfForm sfFirstForm sfExpandedForm">
    <div class="sfFormIn">
        <div class="sfExpandedTarget">
            <h2>Product Images</h2>
            <ul>
                <li class="sfEditorWrp sfClearfix">
                    <div>
<sf:MediaContentSelectorView
    id="selectorView"
    runat="server"
    ContentType="Telerik.Sitefinity.Libraries.Model.Image"
    ParentType="Telerik.Sitefinity.Libraries.Model.Album"
    LibraryBinderServiceUrl="~/Sitefinity/Services/Content/AlbumService.svc/"
    MediaContentBinderServiceUrl="~/Sitefinity/Services/Content/ImageService.svc/"
    MediaContentItemsListDescriptionTemplate="Telerik.Sitefinity.Resources.Templates.Designers.Libraries.Images.ImageItemDescriptionTemplate.htm"
    DisplayResizingOptionsControl="false"
    ShowOpenOriginalSizeCheckBox="false"
     >
</sf:MediaContentSelectorView>
<!--<asp:HiddenField ID="imageId" runat="server" ClientIDMode="Static" />-->
                        <asp:Label ID="LabelProductImage" runat="server" Text="Image:" CssClass="sfTxtLbl" />
                        <div class="sfFieldWrp">
                            <telerik:RadUpload ID="RadUploadImage" runat="server" AllowedFileExtensions=".jpg,.gif,.png,.jpeg" 
                                                MaxFileInputsCount="1" 
                                                Width="100%" 
                                                InputSize="45" 
                                                ControlObjectsVisibility="None"/>
                        </div>

                        <asp:Image ID="imgLabelProduct" runat="server" />
                    </div>.
                </li>
                <li runat="server" id="PanelSpecImage" class="sfEditorWrp sfClearfix">
                    <div>
                        <asp:Label ID="LabelProductSpecImage" runat="server" Text="Spec Image:" CssClass="sfTxtLbl" />
                        <div class="sfFieldWrp">
                            <telerik:RadUpload ID="RadUploadSpecImage" runat="server" AllowedFileExtensions=".jpg,.gif,.png,.jpeg" 
                                                MaxFileInputsCount="1" 
                                                Width="100%" 
                                                InputSize="45" 
                                                ControlObjectsVisibility="None"/>
                        </div>

                        <asp:Image ID="imgLabelProductSpec" runat="server" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="PanelFiles" runat="server" class="sfForm sfExpandedForm">
    <div class="sfFormIn">
        <h2>Product Files</h2>
        <ul>
            <li class="sfEditorWrp sfClearfix">
                <vibco:FileEditor ID="ApplicationBulletinEditor" Type="Application Bulletin" Method="GetApplicationBulletins" runat="server" />
                <%--<div>
                    <asp:Label ID="Label1" runat="server" Text="Application Bulletin:" CssClass="sfTxtLbl" />
                    <div class="sfFieldWrp">
                        <telerik:RadUpload ID="RadUploadApplicationBulletin" runat="server" AllowedFileExtensions=".pdf" 
                                            MaxFileInputsCount="1" 
                                            Width="100%" 
                                            InputSize="45" 
                                            ControlObjectsVisibility="None"/>
                    </div>
                    
                    <asp:HyperLink ID="hlAppBulletin" runat="server">Click to open</asp:HyperLink>
                    <asp:Label ID="hlAppBulletinDelimiter" runat="server" Text="Label"> | </asp:Label>
                    <asp:LinkButton ID="hlRemoveAppBulletin" runat="server" onclick="hlRemoveAppBulletin_Click">Remove file</asp:LinkButton>
                </div>--%>
            </li>
            <li class="sfEditorWrp sfClearfix">
                <vibco:FileEditor ID="QuickReferenceGuideEditor" Type="Quick Reference Guides" Method="GetQuickReferenceGuides" runat="server" />
                <%--<div>
                    <asp:Label ID="Label2" runat="server" Text="Quick Reference Guide:" CssClass="sfTxtLbl" />
                    <div class="sfFieldWrp">
                        <telerik:RadUpload ID="RadUploadQuickReferenceGuide" runat="server" AllowedFileExtensions=".pdf" 
                                            MaxFileInputsCount="1" 
                                            Width="100%" 
                                            InputSize="45" 
                                            ControlObjectsVisibility="None"/>
                    </div>

                    <asp:HyperLink ID="hlQuickReferenceGuide" runat="server">Click to open</asp:HyperLink>
                    <asp:Label ID="hlQuickReferenceGuideDelimiter" runat="server" Text="Label"> | </asp:Label>
                    <asp:LinkButton ID="hlRemoveQuickReferenceGuide" runat="server" onclick="hlQuickReferenceGuide_Click">Remove file</asp:LinkButton>
                </div>--%>
            </li>
            <li class="sfEditorWrp sfClearfix">
                <vibco:FileEditor ID="CompleteServiceManualEditor" Type="Complete Service Manual" Method="GetCompleteServiceManuals" runat="server" />
                <%--<div>
                    <asp:Label ID="Label3" runat="server" Text="Complete Service Manual:" CssClass="sfTxtLbl" />
                    <div class="sfFieldWrp">
                        <telerik:RadUpload ID="RadUploadCompleteServiceManual" runat="server" AllowedFileExtensions=".pdf" 
                                            MaxFileInputsCount="1" 
                                            Width="100%" 
                                            InputSize="45" 
                                            ControlObjectsVisibility="None"/>
                    </div>

                    <asp:HyperLink ID="hlCompleteServiceManual" runat="server">Click to open</asp:HyperLink>
                    <asp:Label ID="hlCompleteServiceManualDelimiter" runat="server" Text="Label"> | </asp:Label>
                    <asp:LinkButton ID="hlRemoveCompleteServiceManual" runat="server" onclick="hlCompleteServiceManual_Click">Remove file</asp:LinkButton>
                </div>--%>
            </li>
            <li class="sfEditorWrp sfClearfix">
                <vibco:FileEditor ID="FlyerEditor" Type="Flyer" Method="GetFlyers" runat="server" />
            </li>
            <li class="sfEditorWrp sfClearfix">
                <vibco:FileEditor ID="CatalogEditor" Type="Catalog" Method="GetCatalogs" runat="server" />
            </li>
        </ul>
    </div>
</div>

<asp:LinkButton ID="ButtonSave" CssClass="sfLinkBtn sfSave" runat="server" onclick="ButtonSave_Click">
    <asp:Label id="LabelSave"  CssClass="sfLinkBtnIn" runat="server">Save & Return</asp:Label>
</asp:LinkButton>

<asp:HyperLink ID="HyperLinkCancel" runat="server" CssClass="sfLinkBtn" >
    <asp:Label ID="LabelLinkCancel" runat="server" CssClass="sfLinkBtnIn" >Cancel</asp:Label>
</asp:HyperLink>