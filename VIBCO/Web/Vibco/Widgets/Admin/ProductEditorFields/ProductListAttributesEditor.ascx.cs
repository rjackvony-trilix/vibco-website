﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using Telerik.Web.UI;
using System.Threading;

namespace SitefinityWebApp.Widgets.Admin.ProductEditorFields
{
    public partial class ProductListAttributesEditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
           {
                NewParamNameComboBox.DataSource = AttributeParameters;
                NewParamNameComboBox.DataBind();
           }

            //ObjectDataSource1.ObjectCreating += new ObjectDataSourceObjectEventHandler(ObjectDataSource1_ObjectCreating);
            //RadGridList.ItemCreated += new Telerik.Web.UI.GridItemEventHandler(RadGridList_ItemCreated);
        }

        protected void ObjectDataSource1_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["ParamName"] = NewParamNameComboBox.SelectedValue;
            e.InputParameters["Value"] = NewValueTextBox.Text;
        }

        protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGridList.DataSource = AttributeParameters;
            RadGridList.DataBind();
        }

        protected void RadGridList_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (e.Item.OwnerTableView.IsItemInserted)
                {
                    LinkButton insertButton = (LinkButton)e.Item.FindControl("PerformInsertButton");
                    insertButton.CausesValidation = false;
                }
                else
                {
                    LinkButton updateButton = (LinkButton)e.Item.FindControl("UpdateButton");
                    updateButton.CausesValidation = false;
                }

                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;

                //GridDropDownListColumnEditor editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ParamNameColumn"));
                
            }
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                GridDataItem item = (GridDataItem)e.Item;
               // Literal litrlContrl = (Literal)item["ParamNameColumn"];
                //litrlContrl.Text = (string)DataBinder.Eval(item.DataItem, "ParamName");
            }  
        }



        protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            ProductListAttributesManager m = new ProductListAttributesManager(ViewState, AttributeName);
            e.ObjectInstance = m;
        }

        public string AttributeName
        {
            get;
            set;
        }

        public IList<string> AttributeParameters
        {
            get
            {
                if (ViewState["AttributeParameters" + AttributeName] == null)
                    ViewState["AttributeParameters" + AttributeName] = new List<string>();

                return (List<string>) ViewState["AttributeParameters" + AttributeName];
            }
            set
            {
                ViewState["AttributeParameters" + AttributeName] = value;
            }
        }


        public List<ListAttribute> Value
        {
            get
            {
                if (ViewState[AttributeName] == null)
                    ViewState[AttributeName] = new List<ListAttribute>();
                return (List<ListAttribute>)this.ViewState[AttributeName];
            }
            set
            {
                ViewState[AttributeName] = value;
            }
        }

        protected void ObjectDataSource1_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            RadGridList.DataSource = null;
            RadGridList.Rebind();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            ObjectDataSource1.Insert();
        }
        protected void AddParameterButton_Click(object sender, EventArgs e)
        {
           ProductManager pmanager = new ProductManager();
           pmanager.setParameter(AddParamTextBox.Text,AttributeName);
           Thread.Sleep(5000);
           switch (AttributeName)
           {
               case "Dimensions":
                   AttributeParameters = pmanager.GetDimensionParameters();
                   break;
               case "Technical Data":
                   AttributeParameters = pmanager.GetTechnicalDataParameters();
                   break;
           }
           NewParamNameComboBox.DataSource = AttributeParameters;
           NewParamNameComboBox.DataBind();
        }
        protected void DeleteParameterButton_Click(object sender, EventArgs e)
        {
           ProductManager pmanager = new ProductManager();
           pmanager.deleteParameter(AddParamTextBox.Text,AttributeName);
           Thread.Sleep(5000);
           switch (AttributeName)
           {
               case "Dimensions":
                   AttributeParameters = pmanager.GetDimensionParameters();
                   break;
               case "Technical Data":
                   AttributeParameters = pmanager.GetTechnicalDataParameters();
                   break;
           }
           NewParamNameComboBox.DataSource = AttributeParameters;
           NewParamNameComboBox.DataBind();
        }
        
        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }

        
    }
}