﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Products;

namespace SitefinityWebApp.Widgets.Admin.ProductEditorFields
{
    public partial class CategoriesEditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public RadTreeView TreeView
        {
            get
            {
                return RadTreeView;
            }
        }

        protected void RadTreeView_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            //if (e.Node.ParentNode == null)
            //    e.Node.Checkable = false;
            //else if (e.Node.ParentNode.ParentNode == null)
            //    e.Node.Checkable = false;
            //else
            //{
            ProductCategory nodeCategory = ((ProductCategory)e.Node.DataItem);
            if(GetValue().Contains(nodeCategory))
                e.Node.Checked = true;
            
            if(e.Node.Value == ProductSetup.ProductAccessoriesCategoryId.ToString())
                e.Node.Visible = false;

            //}
        }

        private IList<ProductCategory> GetValue()
        {
            if (ViewState["ProductCategories"] == null)
                ViewState["ProductCategories"] = new List<ProductCategory>();
            return (IList<ProductCategory>)this.ViewState["ProductCategories"];
        }

        public IList<ProductCategory> Value
        {
            get
            {
                IList<ProductCategory> productCategories = new List<ProductCategory>();
                foreach (RadTreeNode node in RadTreeView.CheckedNodes)
                {
                    ProductCategory checkedCategory = new ProductCategory()
                    {
                        Id = new Guid(node.Value),
                        Name = node.Text,
                        ParentId = new Guid(node.ParentNode.Value)
                    };
                    
                    productCategories.Add(checkedCategory);
                }
                return productCategories;
            }
            set
            {
                ViewState["ProductCategories"] = value;
            }
        }
    }
}