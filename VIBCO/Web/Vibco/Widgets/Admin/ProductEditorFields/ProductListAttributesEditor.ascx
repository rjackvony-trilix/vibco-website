﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListAttributesEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.ProductListAttributesEditor" %>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" >

<div>
    <telerik:RadComboBox runat="server" ID="NewParamNameComboBox" Width="200px" ></telerik:RadComboBox>
    <br />
    <asp:TextBox runat="server" ID="NewValueTextBox" Width="196px"></asp:TextBox>
    <br />
    <asp:Button runat="server" ID="AddButton" Text="Add Field" 
        onclick="AddButton_Click" ></asp:Button>
</div>
<div>
    <asp:TextBox runat="server" ID="AddParamTextBox" Width="196px"></asp:TextBox>
    <br />
    <asp:Button runat="server" ID="AddParameterButton" Text="Add Parameter" 
        onclick="AddParameterButton_Click" ></asp:Button>
    <asp:Button runat="server" ID="DeleteParameterButton" Text="Delete Parameter" 
        onclick="DeleteParameterButton_Click" ></asp:Button>
</div>
<br />

<telerik:RadGrid ID="RadGridList"  Width="100%" runat="server"
     DataSourceID="ObjectDataSource1" OnItemCreated="RadGridList_ItemCreated" OnNeedDataSource="RadGrid1_NeedDataSource"
     AllowAutomaticInserts="false" AllowAutomaticDeletes="true" AutoGenerateColumns="false"
     GridLines="Horizontal" >
    
     <ValidationSettings EnableValidation="false" />
     <MasterTableView AllowAutomaticInserts="false" DataKeyNames="AttributeId,ParamName,Value" CommandItemDisplay="Top" Width="100%"  >
        <CommandItemSettings ShowAddNewRecordButton="false" />
    <Columns>
    <telerik:GridBoundColumn DataField="ParamName" 
                    UniqueName="ParamNameColumn"  HeaderText="Parameter" />
     <telerik:GridBoundColumn DataField="Value" HeaderText="Value">
                    </telerik:GridBoundColumn>
    <telerik:GridButtonColumn ConfirmText="Delete this item?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="LinkButton" CommandName="Delete" Text="Delete"
                            UniqueName="DeleteColumn" />
     </Columns>
    </MasterTableView>
</telerik:RadGrid>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    InsertMethod="Insert" SelectMethod="Select" DeleteMethod="Delete" 
    OnInserting="ObjectDataSource1_Inserting" 
        OnInserted="ObjectDataSource1_Inserted" OnObjectCreating="ObjectDataSource1_ObjectCreating"
    TypeName="Products.ProductListAttributesManager" 
        onselecting="ObjectDataSource1_Selecting" >
    <InsertParameters>
        <asp:Parameter Name="ParamName" Type="String" />
        <asp:Parameter Name="Value" Type="String" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="AttributeId" Type="Object" />
        <asp:Parameter Name="ParamName" Type="String" />
        <asp:Parameter Name="Value" Type="String" />
    </DeleteParameters>
</asp:ObjectDataSource>
</telerik:RadAjaxPanel>


