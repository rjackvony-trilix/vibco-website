﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductReviewsEditor.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.ProductEditorFields.ProductReviewsEditor" %>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" >

<div>
    Name:<br />
    <asp:TextBox runat="server" ID="NewParamNameComboBox" Width="196px"></asp:TextBox>
    <br />
    Review:<br />
    <asp:TextBox TextMode="MultiLine" runat="server" ID="NewValueTextBox" Width="196px"></asp:TextBox>
    <br />
    <asp:Button runat="server" ID="AddButton" Text="Add" 
        onclick="AddButton_Click" ></asp:Button>
</div>
<br />

<telerik:RadGrid ID="RadGridList"  Width="100%" runat="server"
     DataSourceID="ObjectDataSource1" OnItemCreated="RadGridList_ItemCreated"
     AllowAutomaticInserts="false" AllowAutomaticDeletes="true" AutoGenerateColumns="false"
     GridLines="Horizontal" >
    
     <ValidationSettings EnableValidation="false" />
     <MasterTableView AllowAutomaticInserts="false" DataKeyNames="AttributeId" CommandItemDisplay="Top" Width="100%"  >
        <CommandItemSettings ShowAddNewRecordButton="false" />
    <Columns>
    <telerik:GridDropDownColumn DataField="ParamName" 
                    UniqueName="ParamNameColumn"  HeaderText="Name" />
     <telerik:GridBoundColumn DataField="Value" HeaderText="Review">
                    </telerik:GridBoundColumn>
    <telerik:GridButtonColumn ConfirmText="Delete this item?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="LinkButton" CommandName="Delete" Text="Delete"
                            UniqueName="DeleteColumn" />
     </Columns>
    </MasterTableView>
</telerik:RadGrid>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    InsertMethod="Insert" SelectMethod="Select" DeleteMethod="Delete" 
    OnInserting="ObjectDataSource1_Inserting" OnInserted="ObjectDataSource1_Inserted" OnObjectCreating="ObjectDataSource1_ObjectCreating"
    TypeName="Products.ProductListAttributesManager" >
    <InsertParameters>
        <asp:Parameter Name="ParamName" Type="String" />
        <asp:Parameter Name="Value" Type="String" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="AttributeId" Type="Object" />
        <asp:Parameter Name="ParamName" Type="String" />
        <asp:Parameter Name="Value" Type="String" />
    </DeleteParameters>
</asp:ObjectDataSource>

</telerik:RadAjaxPanel>


