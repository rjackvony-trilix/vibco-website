﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Products;
using System.Web.Profile;
using System.IO;
using System.Text;
using Telerik.Sitefinity.Modules.GenericContent;

namespace SitefinityWebApp.Widgets.Admin
{
    public partial class ProductGroupEditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HyperLinkReturn.NavigateUrl = ReturnBackUrl;
            HyperLinkCancel.NavigateUrl = ReturnBackUrl;
            if (ProductIds.Count() != 0)
            {
                LoadProducts();

                CheckProductsForWarning();

                LoadFieldValue();
            }
            else
            {
                Label1.Visible = true;
                RadUploadCSV.Visible = true;
                Label2.Visible = true;
                RadUploadDocs.Visible = true;
                selectID.Visible = true;
            }

            DimensionAttributesEditor.AttributeParameters = ProductManager.GetDimensionParameters();
            TechnicalDataAttributesEditor.AttributeParameters = ProductManager.GetTechnicalDataParameters();
        }
        private const int BUFFER_SIZE = 4096;
        

        // fires when import button is clicked
        protected void stuff_Click(object sender, EventArgs e)
        {
            var a = ProductManager.GetBaseProducts(5000, 0, String.Empty);
            IList<Guid> productIdsToDelete = new List<Guid>();
            foreach (var b in a)
            {
                var originalGuid = b.Id;
                var keyGuid = ProductManager.GetProductByKey(b.Title);
                var manager = ContentManager.GetManager();
                var items = manager.GetContent().Where(t => t.Title == b.Title);
                if (items.Count() > 1)
                {
                    foreach (var d in items)
                    {
                        var g = d.Id;
                        if (g != originalGuid)
                        {
                            productIdsToDelete.Add(g);

                        }
                    }
                }
            }  
        }
        protected void importButton_Click()
        {
            if (RadUploadCSV.UploadedFiles.Count > 0 && RadUploadDocs.UploadedFiles.Count > 0)
            {
                Stream responseStreamCSV = RadUploadCSV.UploadedFiles[0].InputStream;
                Stream responseStreamDoc = RadUploadDocs.UploadedFiles[0].InputStream;
                MemoryStream memoryStream = null;
                MemoryStream docmemoryStream = null;
                byte[] buffer = new byte[BUFFER_SIZE];
                byte[] docbuffer = new byte[BUFFER_SIZE];
                try
                {
                    int docReadBytes;
                    docmemoryStream = new MemoryStream();
                    while ((docReadBytes = responseStreamDoc.Read(docbuffer, 0, BUFFER_SIZE)) > 0)
                    {
                        docmemoryStream.Write(docbuffer, 0, docReadBytes);
                    }
                    string docContents =
                        Encoding.UTF8.GetString(docmemoryStream.GetBuffer(), 0, (int)docmemoryStream.Length);

                    int readBytes;
                    memoryStream = new MemoryStream();
                    while ((readBytes = responseStreamCSV.Read(buffer, 0, BUFFER_SIZE)) > 0)
                    {
                        memoryStream.Write(buffer, 0, readBytes);
                    }
                    string csvContents =
                        Encoding.UTF8.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);

                    string[] subscribersRows = csvContents.Split('\n');
            
                    foreach (string subscriberRow in subscribersRows)
                    {
                        string subscriberInfoRow = subscriberRow.Trim();
                        if (!string.IsNullOrEmpty(subscriberInfoRow))
                        {
                            string[] columns = subscriberInfoRow.Split('|');
                            //get column values
                            //here you can make it read additional columns and store them
                            string userName = columns[0].Trim();
                            Product prod = ProductManager.GetProductByKey(userName);
                            if (prod != null)
                            {

                                if (selectID.SelectedItem.Text != "Testimonials")
                                {

                                    ProductUploadedFile file = new ProductUploadedFile()
                                    {
                                        FileName = RadUploadDocs.UploadedFiles[0].FileName,
                                        Extension = RadUploadDocs.UploadedFiles[0].GetExtension(),
                                        InputStream = RadUploadDocs.UploadedFiles[0].InputStream
                                    };

                                    prod.RelatedProductFiles.AddFileToUpload(selectID.SelectedItem.Text,
                                        file);
                                    ProductManager.UploadFiles(prod, true);
                                }
                                else
                                {
                                    string[] docTestimonials = docContents.Split('\n');
                                    foreach (string docTestimonial in docTestimonials)
                                    {
                                        
                                        ListAttribute blah = new ListAttribute();
                                        string[] splitTestimonial = docTestimonial.Split('|');
                                        if (splitTestimonial.Length == 2)
                                        {
                                            blah.ParamName = splitTestimonial[1];
                                            blah.Value = splitTestimonial[0];
                                            blah.AttributeId = Guid.NewGuid();
                                            prod.Reviews.Items.Add(blah);
                                            ProductManager.SaveProductChanges(prod, true);
                                        }
                                    }
                                    //prod.Reviews.Items = ReviewsEditor.Value;
                                    

                                }
                            }
                            /*if (newUser != null)
                            {
                                
                            }*/
                        }
                    }
                }
                finally 
                {
                    if (memoryStream != null)
                        memoryStream.Close();
                }
            }
            //DeleteFiles();
        }
        private void DeleteFiles()
        {
            string targetFolder = Server.MapPath(RadUploadCSV.TargetFolder);

            DirectoryInfo targetDir = new DirectoryInfo(targetFolder);

            foreach (FileInfo file in targetDir.GetFiles())
            {
                if ((file.Attributes & FileAttributes.ReadOnly) == 0) file.Delete();
            }
        }
        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (EditedProducts != null)
            {
                foreach (Product product in EditedProducts)
                {
                    switch (EditorType)
                    {
                        case "Edit-Description":
                            product.Description = HtmlEditorDescription.Value.ToString();
                            break;
                        case "Edit-Name":
                            product.Title = product.Title.Replace(product.Name, TextBoxName.Text);
                            product.Name = TextBoxName.Text;
                            break;
                        case "Edit-Categories":
                            product.Categories = CategoriesEditor.Value;
                            break;
                        case "Edit-Dimensions":
                            product.Dimensions.Items = DimensionAttributesEditor.Value;
                            break;
                        case "Edit-Technical-Data":
                            product.TechnicalData.Items = TechnicalDataAttributesEditor.Value;
                            break;
                        case "Edit-Accessories":
                            product.Accessories.Items = AccessoriesEditor.Value;
                            break;
                        case "Group-Document":
                            
                            break;
                    }
                    ProductManager.SaveProductChanges(product, true);
                }
            }
            else
            {
                importButton_Click();
            }
            Response.Redirect(ReturnBackUrl);
        }

        private static bool AreEquals(ProductListField<ListAttribute> firstList, ProductListField<ListAttribute> secondList)
        {
            if (firstList.Items.Count != secondList.Items.Count)
                return false;

            for (int i = 0; i<firstList.Items.Count; i++)
            {
                if (firstList.Items[i].ParamName != secondList.Items[i].ParamName)
                    return false;
                if (firstList.Items[i].Value != secondList.Items[i].Value)
                    return false;
            }

            return true;
        }

        private static bool AreEquals(ProductListField<Accessory> firstList, ProductListField<Accessory> secondList)
        {
            if (firstList.Items.Count != secondList.Items.Count)
                return false;

            for (int i = 0; i < firstList.Items.Count; i++)
            {
                if (firstList.Items[i].Id != secondList.Items[i].Id)
                    return false;
            }

            return true;
        }

        private static bool AreEquals(IList<ProductCategory> firstList, IList<ProductCategory> secondList)
        {
            if (firstList.Count != secondList.Count)
                return false;

            for (int i = 0; i < firstList.Count; i++)
            {
                if (firstList[i].Id != secondList[i].Id)
                    return false;
            }

            return true;
        }
        

        private void LoadProducts()
        {
            EditedProducts = new List<Product>();
            foreach (Guid id in ProductIds)
                EditedProducts.Add(ProductManager.GetProduct(id));
        }

        private void LoadFieldValue()
        {
            if(IsPostBack)
                return;

            switch (EditorType)
            {
                case "Edit-Description":
                    HtmlEditorDescription.Visible = true;
                    break;
                case "Edit-Name":
                    TextBoxName.Visible = true;
                    break;
                case "Edit-Categories":
                    CategoriesEditor.Visible = true;
                    break;
                case "Edit-Dimensions":
                    DimensionAttributesEditor.Visible = true;
                    break;
                case "Edit-Technical-Data":
                    TechnicalDataAttributesEditor.Visible = true;
                    break;
                case "Edit-Accessories":
                    AccessoriesEditor.Visible = true;
                    break;
                case "Group-Documents":
                    
                    break;
            }

            if (LabelWarning.Visible)
                return;
            
            switch(EditorType)
            {
                case "Edit-Description":
                    HtmlEditorDescription.Value = EditedProducts[0].Description;
                    break;
                case "Edit-Name":
                    TextBoxName.Text = EditedProducts[0].Name;
                    break;
                case "Edit-Categories":
                    CategoriesEditor.Value = EditedProducts[0].Categories;
                    break;
                case "Edit-Dimensions":
                    DimensionAttributesEditor.Value = EditedProducts[0].Dimensions.Items;
                    break;
                case "Edit-Technical-Data":
                    TechnicalDataAttributesEditor.Value = EditedProducts[0].TechnicalData.Items;
                    break;
                case "Edit-Accessories":
                    AccessoriesEditor.Value = EditedProducts[0].Accessories.Items;
                    break;
            }
        }

        protected void CheckProductsForWarning()
        {
            for(int i=0; i< EditedProducts.Count - 1; i++)
            {
                switch(EditorType)
                {
                    case "Edit-Description":
                        if (EditedProducts[i].Description != EditedProducts[i+1].Description)
                            LabelWarning.Visible = true;
                        break;
                    case "Edit-Name":
                        if (EditedProducts[i].Name != EditedProducts[i + 1].Name)
                            LabelWarning.Visible = true;
                        break;
                    case "Edit-Categories":
                        if (!AreEquals(EditedProducts[i].Categories, EditedProducts[i + 1].Categories))
                            LabelWarning.Visible = true;
                        break;
                    case "Edit-Dimensions":
                        if (!AreEquals(EditedProducts[i].Dimensions, EditedProducts[i + 1].Dimensions))
                            LabelWarning.Visible = true;
                        break;
                    case "Edit-Technical-Data":
                        if (!AreEquals(EditedProducts[i].TechnicalData, EditedProducts[i + 1].TechnicalData))
                            LabelWarning.Visible = true;
                        break;
                    case "Edit-Accessories":
                        if (!AreEquals(EditedProducts[i].Accessories, EditedProducts[i + 1].Accessories))
                            LabelWarning.Visible = true;
                        break;
                }
            }
            
        }

        public ProductManager ProductManager
        {
            get
            {
                if(productManager == null)
                    productManager = new ProductManager();

                return productManager;
            }
        }
        private ProductManager productManager;

        public IList<Product> EditedProducts
        {
            get;
            set;
        }

        public string EditorType
        {
            get
            {
                return Request.QueryString["editor-type"];
            }
        }

        public Guid[] ProductIds
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["productids"]))
                    return new Guid[0];
                string[] productidstrs = Request.QueryString["productids"].Split(';');

                Guid[] result = new Guid[productidstrs.Length];
                
                int i=0;
                foreach(string idstr in productidstrs)
                {
                    result[i] = new Guid(idstr);
                    i++;
                }

                return result;
            }
        }

        public string ReturnBackUrl
        {
            get
            {         
                return "~/sitefinity/Content/products";
            }

        }
    }
}