﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BackDateNews.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.BackDateNews" %>
<div class="sfAllToolsWrapper">
    <div class="sfAllTools">
        <div class="sfActions">
            <ul>
                <li class="sfMainAction">
                    <br />
                    &nbsp;
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonSetup" runat="server" OnClick="ButtonNews_Click" >
                        <span class="sfLinkBtnIn" >Back Date News Articles </span>
                    </asp:LinkButton>
                    <br />*Must have already Set Publication Date on those News Items under Content->News*
             
                </li>
            </ul>
        </div>
    </div>
</div>