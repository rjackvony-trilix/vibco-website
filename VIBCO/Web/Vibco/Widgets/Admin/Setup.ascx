﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Setup.ascx.cs" Inherits="SitefinityWebApp.Widgets.Admin.Setup" %>
<div class="sfAllToolsWrapper">
    <div class="sfAllTools">
        <div class="sfActions">
            <ul>
                <li class="sfMainAction">
                    <br />
                    &nbsp;
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonSetup" runat="server" OnClick="ButtonSetup_Click" >
                        <span class="sfLinkBtnIn" >Setup Environment</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonUpdate1" runat="server" OnClick="ButtonUpdate1_Click" >
                        <span class="sfLinkBtnIn" >Update 1</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonUpdate2" runat="server" OnClick="ButtonUpdate2_Click" >
                        <span class="sfLinkBtnIn" >Update 2</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonUpdate3" runat="server" OnClick="ButtonUpdate3_Click" >
                        <span class="sfLinkBtnIn" >Update 3</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration" runat="server" OnClick="ButtonMigration_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 1</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration21" runat="server" OnClick="ButtonMigration21_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.1</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration22" runat="server" OnClick="ButtonMigration22_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.2</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration23" runat="server" OnClick="ButtonMigration23_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.3</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration24" runat="server" OnClick="ButtonMigration24_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.4</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration25" runat="server" OnClick="ButtonMigration25_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.5</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration26" runat="server" OnClick="ButtonMigration26_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 2.5</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigration3" runat="server" OnClick="ButtonMigration3_Click" >
                        <span class="sfLinkBtnIn" >Migrate Products 3</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrationUpdate1" runat="server" OnClick="ButtonMigrationUpdate1_Click" >
                        <span class="sfLinkBtnIn" >Update Migrated Products 1</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrationUpdate2" runat="server" OnClick="ButtonMigrationUpdate2_Click" >
                        <span class="sfLinkBtnIn" >Update Migrated Products 2</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonUpdate4" runat="server" OnClick="ButtonUpdate4_Click" >
                        <span class="sfLinkBtnIn" >Update 4</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonUpdate5" runat="server" OnClick="ButtonUpdate5_Click" >
                        <span class="sfLinkBtnIn" >Update 5</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrateAccessoires" runat="server" OnClick="ButtonMigrateAccessoires_Click" >
                        <span class="sfLinkBtnIn" >Migrate Accessoires</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrationUpdate3" runat="server" OnClick="ButtonMigrationUpdate3_Click" >
                        <span class="sfLinkBtnIn" >Update Migrated Products 3</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrationUpdate4" runat="server" OnClick="ButtonMigrationUpdate4_Click" >
                        <span class="sfLinkBtnIn" >Update Migrated Products 4</span>
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton class="sfLinkBtn sfNew" ID="ButtonMigrationUpdate5" runat="server" OnClick="ButtonMigrationUpdate5_Click" >
                        <span class="sfLinkBtnIn" >Update Migrated Products 5</span>
                    </asp:LinkButton>
                </li>
            </ul>
        </div>
    </div>
</div>