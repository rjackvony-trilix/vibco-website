﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductViewMobile.ascx.cs" Inherits="SitefinityWebApp.Widgets.ProductViewMobile" %>
<h1><asp:Label runat="server" ID="LabelTitle"></asp:Label></h1>
<asp:Image ID="imgLabelProduct" runat="server" width="200"/><br />
<asp:Label runat="server" ID="LabelDescription"></asp:Label>
 <br />
 <h4><span>Tech Data</span></h4>
    <table width="300" class="tblProduct">
      <asp:ListView ID="lvTechData" runat="server">
      <ItemTemplate>
       <tr  class="tblOdd">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
        <tr class="tblEven">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table> 
     <br />
  <h4><span>Dimensions</span></h4>
      <asp:Image ID="imgSpec" runat="server" Width="300px"/>
    <table width="300" class="tblProduct">
      <asp:ListView ID="lvDimensions" runat="server">
      <ItemTemplate>
        <tr class="tblOdd">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
        <tr class="tblEven">
          <td><%# Eval("ParamName")%></td>
          <td><%# Eval("Value")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table>
    <br />
    <h4>RECOMMENDED <span>ACCESSORIES</span></h4>
    <table width="300" class="tblProduct">
      <asp:ListView ID="lvAccessoires" runat="server">
      <ItemTemplate>
        <tr class="tblOdd">
          <td><%# Eval("Name")%></td>
        </tr>
      </ItemTemplate>
      <AlternatingItemTemplate>
      <tr class="tblEven">
          <td><%# Eval("Name")%></td>
        </tr>
      </AlternatingItemTemplate>
      </asp:ListView>
    </table>
    <br />
    <h4>Documents</h4>
    <asp:HyperLink ID="hlAppBulletin" runat="server">Application Bulletin</asp:HyperLink><br />
  <asp:HyperLink ID="hlQuickReferenceGuide" runat="server">Quick Reference Guides</asp:HyperLink><br />
  <asp:HyperLink ID="hlCompleteServiceManual" runat="server">Complete Service Manual</asp:HyperLink><br />
  <asp:HyperLink ID="hlFlyer" runat="server">Flyer</asp:HyperLink><br />
  <asp:HyperLink ID="hlCatalog" runat="server">Catalog</asp:HyperLink><br />


 