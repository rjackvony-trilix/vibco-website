﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Xml.Linq;
using Telerik.Sitefinity;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;

namespace SitefinityHandlers
{
    /// <summary>
    /// Iterates sitefinity pages and generates a sitemap.xml file as a response
    /// </summary>
    public class sitemap : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private const bool CACHE_ENABLED = true;                                        //Whether the data should be cached at all
        private const double CACHE_DURATION = .1;                                      //The number of minutes to cache the data since it was first accessed
        private const string CACHE_KEY = "SiteMap.XmlContentCache";                     //The key to retrieve the data from the cache
        private const string URL_BASE = "http://www.vibco.com/";                     //The base url for pages on the site
        private const string XML_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>"; //XDocumenet.ToString() omits the declaration
        public void ProcessRequest(HttpContext context)
        {   
            string xmlData;            
            if (CACHE_ENABLED)
            {
                //check if the data already exist in the cache
                xmlData = context.Cache[CACHE_KEY] as string;
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    xmlData = GetXmlPageData();
                    context.Cache.Insert(CACHE_KEY, xmlData, null, DateTime.Now.AddMinutes(CACHE_DURATION), Cache.NoSlidingExpiration);
                }
            }
            else
            {
                //cache is disabled, just get the data
                xmlData = GetXmlPageData();
            }
            context.Response.Clear();
            context.Response.ContentType = "text/xml";
		    context.Response.Charset = "utf-8";
            context.Response.AddHeader("Content-Length", xmlData.Length.ToString());
            context.Response.Write(xmlData);
		    context.Response.Flush();
		    context.Response.End();
        }

        private string GetXmlPageData()
        {
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XDocument xDoc = new XDocument();
            XElement xRoot = new XElement(ns + "urlset");
            var pages = App.WorkWith().Pages().Get().ToList();
            foreach (var page in pages )
            {
                ProcessSitefinityPage(ref xRoot, page, ns, URL_BASE);
            }
            //Example of appending custom pages
            /*App.WorkWith().ContentItems().Where(c => c.Organizer.TaxonExists("Tags", new Guid("D59719BA-E72A-487D-96EF-0AA4239845BC"))).ForEach(ci => 
            {
                string itemTitle = ci.Title;
                //append product page to the rootNode
                xRoot.Add(new XElement(ns + "url",
                                              new XElement(ns + "loc", URL_BASE + "products/product?productkey=" + itemTitle), //this is the only required node
                                              new XElement(ns + "changefreq", "weekly"), //always, hourly, daily, weekly monthly, yearly
                                              //new XElement(ns + "lastmod", aDate.ToString("yyyy-MM-dd")),
                                              new XElement(ns + "priority", "0.5") //0.5 is the default
                                        ));
            });
            */
            int i = 0;
            App.WorkWith().ContentItems().Where(c => c.ExpirationDate > DateTime.Today).ForEach(c =>
                {
                    if (c.Organizer.TaxonExists("Tags", new Guid("D59719BA-E72A-487D-96EF-0AA4239845BC")))
                    {
                        string itemTitle = c.Title;
                        //append product page to the rootNode
                        xRoot.Add(new XElement(ns + "url",
                                              new XElement(ns + "loc", URL_BASE + "products/product?productkey=" + itemTitle)/*, //this is the only required node
                                              new XElement(ns + "changefreq", "weekly"), //always, hourly, daily, weekly monthly, yearly
                                              //new XElement(ns + "lastmod", aDate.ToString("yyyy-MM-dd")),
                                              new XElement(ns + "priority", "0.5")*/)); //0.5 is the default
                        i++;
                    }
                });
            xDoc.Add(xRoot);
            return XML_HEADER + xDoc.ToString();
        }
		
		private void ProcessSitefinityPage(ref XElement xRoot, PageNode page, XNamespace ns, string urlBase)
        {
            if (page.ShowInNavigation && !page.IsBackend)
            {
                if (page.NodeType == NodeType.Standard && 
                    page.Page.Status == ContentLifecycleStatus.Live)
                {
                    string fullURL = page.GetFullUrl().TrimStart(new char[] { '~', '/' });
                    xRoot.Add(new XElement(ns + "url",
                                           new XElement(ns + "loc", URL_BASE + fullURL)/*, //this is the only required node
                                           new XElement(ns + "lastmod", page.LastModified.ToString("yyyy-MM-dd")),
                                           new XElement(ns + "changefreq", "weekly"), //always, hourly, daily, weekly monthly, yearly
                                           new XElement(ns + "priority", "0.5")*/ //0.5 is the default
                                  ));
                }else if(page.NodeType == NodeType.Group)
                {
                    urlBase = (page.Ordinal == 0.0f) ? urlBase : urlBase + page.UrlName.ToLower() + "/";
                    foreach (var subPage in page.Nodes)
                    {
                        ProcessSitefinityPage(ref xRoot, subPage, ns, urlBase);
                    }
                }
            }
        }
		
        private void AppendExamplePages(ref XElement xRoot, XNamespace ns, string urlBase)
        {
            //get your item list (custom pages, blogs etc)
            var pages = new List<string>() {"example1", "example2", "example3"};
            foreach (string pageUrl in pages)
            {
                //append your page to the rootNode
                xRoot.Add(new XElement(ns + "url",
                                              new XElement(ns + "loc", urlBase + pageUrl), //this is the only required node
                                              new XElement(ns + "changefreq", "weekly"), //always, hourly, daily, weekly monthly, yearly
                                              //new XElement(ns + "lastmod", aDate.ToString("yyyy-MM-dd")),
                                              new XElement(ns + "priority", "0.5") //0.5 is the default
                   ));
            }
        }
    }
}