﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Abstractions;

namespace SitefinityWebApp
{
    public partial class UniqueIDs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = PageManager.GetManager();
            var propName = "ID";
            var controlCount = manager.GetControls<PageControl>().Count();

            for (int i = 0; i < controlCount; i += 100)
            {
                var controls = manager.GetControls<PageControl>().Skip(i).Take(100).ToList();
                foreach (var cb in controls)
                {
                    var idProp = cb.Properties.FirstOrDefault(p => p.Name == propName);
                    if (idProp == null)
                    {
                        idProp = manager.CreateProperty();
                        idProp.Name = propName;
                        var newId = "ControlPrefix" + Guid.NewGuid().ToString();
                        idProp.Value = newId.Replace('-', '_');
                        cb.Properties.Add(idProp);
                    }
                    else
                    {
                        var newId = "ControlPrefix" + Guid.NewGuid().ToString();
                        idProp.Value = newId.Replace('-', '_');
                    }
                }

                manager.SaveChanges();
            }

            controlCount = manager.GetControls<PageDraftControl>().Count();

            for (int i = 0; i < controlCount; i += 100)
            {
                var draftControls = manager.GetControls<PageDraftControl>().Skip(i).Take(100).ToList();
                foreach (var cb in draftControls)
                {
                    var idProp = cb.Properties.FirstOrDefault(p => p.Name == propName);
                    if (idProp == null)
                    {
                        idProp = manager.CreateProperty();
                        idProp.Name = propName;
                        var newId = "ControlPrefix" + Guid.NewGuid().ToString();
                        idProp.Value = newId.Replace('-', '_');
                        cb.Properties.Add(idProp);
                    }
                    else
                    {
                        var newId = "ControlPrefix" + Guid.NewGuid().ToString();
                        idProp.Value = newId.Replace('-', '_');
                    }
                }

                manager.SaveChanges();
            }
        }
    }
}