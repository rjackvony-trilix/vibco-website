﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Libraries;

namespace SitefinityWebApp.Controls
{
    public partial class FlashEmbed : System.Web.UI.UserControl
    {
        public string FlashFileName
        {
            get;
            set;
        }
        private string VRHeight;
        public string SetHeight
        {
            get
            {
                if (string.IsNullOrEmpty(VRHeight)) return "400";
                else return VRHeight;
            }
            set
            {
                VRHeight = value;
            }
        }
        private string VRWidth;
        public string SetWidth
        {
            get
            {
                if (string.IsNullOrEmpty(VRWidth)) return "708";
                else return VRWidth;
            }
            set
            {
                VRWidth = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetFlashFile()
        {

          //  if (this.Page.Items["IsInIndexMode"] == null)
           // {
            try
            {
                var z = FlashFileName;

                var a = App.WorkWith().Documents().Get().ToList().Where(i => i.Title == z && i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live);

                return a.SingleOrDefault().MediaUrl;
            }
            catch
            {
                return "";
            }
          //  }
           // else
           // {
           //     return String.Empty;
           // }
        }


    }
}