﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Breadcrumbs.ascx.cs" Inherits="SitefinityWebApp.Controls.Breadcrumbs" %>

<asp:SiteMapPath ID="SiteMapPath1" SiteMapProvider="SitefinitySiteMap1" runat="server" NodeStyle-CssClass="BreadcrumbsNode"   PathSeparatorStyle-CssClass="BreadcrumbsNavPathSep" ParentLevelsDisplayed="-1" PathSeparator=" . &nbsp;" RenderCurrentNodeAsLink="true"  ></asp:SiteMapPath>
