﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Sitefinity.Web;

namespace SitefinityWebApp.Controls
{
    public partial class SiteTreeview : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SiteMapTree.StartingNodeOffset = 0;
            //RadTreeviewTree.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ((SiteMapDataSource)RadTreeviewTree.DataSource).StartingNodeOffset = 1;
            ((SiteMapDataSource)RadTreeviewTree.DataSource).StartFromCurrentNode = false;

            RadTreeviewTree.DataBind();

            String location = Request.Url.AbsoluteUri;
           


            location = location.Substring(location.LastIndexOf('/') + 1);
            //var currentNode = RadTreeviewTree.FindNodeByUrl(location);
            var currentNode = RadTreeviewTree.GetAllNodes().Where(c => c.NavigateUrl.Substring(c.NavigateUrl.LastIndexOf('/')+1) == location).FirstOrDefault();
            if(currentNode != null) //top level already
                RecursiveExpand(currentNode.ParentNode);

                     


        }
        private bool RecursiveExpand(RadTreeNode Node)
        {
            if (Node == null)
                return false;

            Node.Expanded = true;
            RecursiveExpand(Node.ParentNode);
            return true;

        }
        
        public void RadTreeview1_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            PageSiteNode pageSiteNode = (PageSiteNode)e.Node.DataItem;

       
            if (pageSiteNode.ShowInNavigation == false)
            {
                String url2find = pageSiteNode.Url;
                url2find = url2find.Remove(0, 1);

                if (RadTreeviewTree.FindNodeByUrl(url2find) != null)
                {
                    try
                    {
                        RadTreeviewTree.FindNodeByUrl(url2find).Remove();
                       
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

        }
    }
}