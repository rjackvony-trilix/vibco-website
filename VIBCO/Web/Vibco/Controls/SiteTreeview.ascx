﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteTreeview.ascx.cs" Inherits="SitefinityWebApp.Controls.SiteTreeview" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>

<script language="javascript" type="text/javascript">

    function treeLoad() {
        var tree = $find("<%= RadTreeviewTree.ClientID %>");
         
        if (tree) {
            var location = window.location.pathname;
            location = location.substring(location.lastIndexOf('/') + 1);
         
            var currentNode = tree.findNodeByUrl(location);
            
            if (currentNode) {
                currentNode.set_selected(true);
                currentNode.expand();
            }
            
        }
    }
    </script>

<telerik:RadTreeview ID="RadTreeviewTree" runat="server"  
    OnNodeDataBound="RadTreeview1_NodeDataBound"  BorderStyle="None" 
    EnableEmbeddedSkins="false" Skin="Vibco" ShowLineImages="false" 
    OnClientLoad="treeLoad">
</telerik:RadTreeview>

