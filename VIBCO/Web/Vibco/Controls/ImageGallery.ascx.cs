﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Libraries;
using Telerik.Web.UI;

namespace SitefinityWebApp.Controls
{
    public partial class ImageGallery : System.Web.UI.UserControl
    {
        private string sGalleryName;
        private int iMargin;
        
        public int MarginTop
        {
            get { return iMargin; }
            set { iMargin = value; }
        }
        public string ImageGalleryName
        {
            get { return sGalleryName; }
            set { sGalleryName = value; }
        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
           
           

            RadRotator1.DataSource = App.WorkWith().Images().Get().ToList().Where(i => i.Parent.Title == ImageGalleryName && i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live);
            RadRotator1.DataBind();
        }

        protected void RadRotator1_ItemDataBound(object sender, RadRotatorEventArgs e)
        {
            System.Web.UI.WebControls.Image img = ((System.Web.UI.WebControls.Image)e.Item.FindControl("Image1"));
            img.ImageUrl = ((Telerik.Sitefinity.Libraries.Model.Image)e.Item.DataItem).MediaUrl;
            img.Width = System.Web.UI.WebControls.Unit.Pixel(737);
            img.Style.Add("margin-top", iMargin.ToString()+"px");
            
            
        }
         
    }
}