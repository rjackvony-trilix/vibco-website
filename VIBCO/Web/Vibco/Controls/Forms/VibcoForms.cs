﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Web.UI;
using System.IO;
using System.Text;
using System.Configuration;
using System.Net;

namespace SitefinityWebApp.Forms
{
    public class VibcoForms
    {


        public VibcoForms()
        {
        }

        public string ParseContactInformation(Panel p)
        {
            String strContactInfo = "";
            strContactInfo = "Name: " + ((TextBox)p.FindControl("tbName")).Text + Environment.NewLine + "Email: " + ((TextBox)p.FindControl("tbEmail")).Text + Environment.NewLine + "Company:" +
            ((TextBox)p.FindControl("tbCompany")).Text + Environment.NewLine + "Industry: "+ ((TextBox)p.FindControl("tbIndustry")).Text + Environment.NewLine+Environment.NewLine+
            "Address: " + Environment.NewLine + ((TextBox)p.FindControl("tbAddress")).Text + Environment.NewLine;

            if (((TextBox)p.FindControl("tbAddress2")).Text != String.Empty)
                strContactInfo += ((TextBox)p.FindControl("tbAddress2")).Text + Environment.NewLine;

            strContactInfo += ((TextBox)p.FindControl("tbCity")).Text + ", " + ((TextBox)p.FindControl("tbState")).Text + " " + ((TextBox)p.FindControl("tbZip")).Text + Environment.NewLine +
                  ((TextBox)p.FindControl("tbCountry")).Text + Environment.NewLine + Environment.NewLine + "Phone: " + ((TextBox)p.FindControl("tbPhone")).Text + Environment.NewLine +
                  "Fax: " + ((TextBox)p.FindControl("tbFax")).Text + Environment.NewLine + "Mobile: " + ((TextBox)p.FindControl("tbMobile")).Text + Environment.NewLine + "Receive Newsletter: " +
                  ((RadioButtonList)p.FindControl("rbNewsletter")).SelectedValue + Environment.NewLine;
           
            return strContactInfo;
        }

        public string GetUsersEmail(Panel p)
        {
            return ((TextBox)p.FindControl("tbEmail")).Text;
        }

        public string ParseNumericTextBoxMultiPage(RadMultiPage mp,bool IsCSV = false)
        {
            String strReturn="";
            foreach ( RadPageView pv in mp.PageViews)
            {
                strReturn += ParseNumericPageView(pv,IsCSV);
            }
            return strReturn;
        }
        public string ParseFreeTrialPS(Panel p, bool IsCSV = false)
        {
            String strReturn = "";
            if (IsCSV)
            {
                strReturn = "Power Source" + Environment.NewLine;
                strReturn += "Pneumatic,"+ "Air PSI," + ((TextBox)p.FindControl("tbAirPSI")).Text + ",Air CFM," + ((TextBox)p.FindControl("tbAirCFM")).Text + Environment.NewLine;
                strReturn += "Hydraulic,"+ "Hydraulic PSI,"+ ((TextBox)p.FindControl("tbHPSI")).Text +  ",Hydraulic GPM," + ((TextBox)p.FindControl("tbHGPM")).Text + Environment.NewLine;
                strReturn += "Electric," + "Current," + ((RadComboBox)p.FindControl("rcbCurrent")).SelectedValue +  ",Volts," + ((TextBox)p.FindControl("tbVolts")).Text + Environment.NewLine;
                strReturn += "Phase," + ((RadComboBox)p.FindControl("rcbPhase")).SelectedValue + ",Cycle," + ((TextBox)p.FindControl("tbCycle")).Text + Environment.NewLine;
               
            }
            else
            {
                  strReturn = "Power Source" + Environment.NewLine + Environment.NewLine;

                strReturn += "Pneumatic" + Environment.NewLine + "Air PSI: " + ((TextBox)p.FindControl("tbAirPSI")).Text + Environment.NewLine + "Air CFM: " + ((TextBox)p.FindControl("tbAirCFM")).Text + Environment.NewLine;
                strReturn += "Hydraulic" + Environment.NewLine + "Hydraulic PSI: " + ((TextBox)p.FindControl("tbHPSI")).Text + Environment.NewLine + "Hydraulic GPM: " + ((TextBox)p.FindControl("tbHGPM")).Text + Environment.NewLine;
                strReturn += "Electric" + Environment.NewLine + "Current: " + ((RadComboBox)p.FindControl("rcbCurrent")).SelectedValue + Environment.NewLine + "Volts: " + ((TextBox)p.FindControl("tbVolts")).Text + Environment.NewLine;
                strReturn += "Phase: " + ((RadComboBox)p.FindControl("rcbPhase")).SelectedValue + Environment.NewLine + "Cycle: " + ((TextBox)p.FindControl("tbCycle")).Text + Environment.NewLine;
                strReturn += Environment.NewLine;
            }
            return strReturn;
        }

        public string ParseCheckBoxGroup(CheckBoxList cbl)
        {
            string strReturn = "";
            foreach (ListItem c in cbl.Items)
            {
                if (c.Selected)
                    strReturn += c.Value + ", ";
            }
            if (strReturn.Length >= 2)
                return strReturn.Remove(strReturn.Length - 2);
            else
                return "";
            
        }
        public string ParseFreeTrialMT(Panel p,bool IsCSV = false)
        {
            String strReturn = "";
            if (IsCSV)
            {
                strReturn = "Material" + Environment.NewLine;

                strReturn += "Material Type," + "\"" + ((TextBox)p.FindControl("tbType")).Text + "\"" + ",Weight," + ((TextBox)p.FindControl("tbWeight")).Text + Environment.NewLine;
                strReturn += "Moisture %," + ((TextBox)p.FindControl("tbMoisture")).Text + ",Discharge," + ((RadComboBox)p.FindControl("rcbDischage")).SelectedValue + Environment.NewLine;
                strReturn += "Flow Problem," + ((RadComboBox)p.FindControl("rcbFlow")).SelectedValue + Environment.NewLine;
                
            }
            else
            {
                  strReturn = "Material" + Environment.NewLine + Environment.NewLine;

                strReturn += "Material Type: " + ((TextBox)p.FindControl("tbType")).Text + Environment.NewLine + "Weight " + ((TextBox)p.FindControl("tbWeight")).Text + Environment.NewLine;
                strReturn += "Moisture %: " + ((TextBox)p.FindControl("tbMoisture")).Text + Environment.NewLine + "Discharge: " + ((RadComboBox)p.FindControl("rcbDischage")).SelectedValue + Environment.NewLine;
                strReturn += "Flow Problem: " + ((RadComboBox)p.FindControl("rcbFlow")).SelectedValue + Environment.NewLine;
                strReturn += Environment.NewLine;
            }
            return strReturn;
        }

        public string Parse25Story(Panel p, bool IsCSV = false)
        {
            String strReturn ="";
            if (IsCSV)
            {
                strReturn = ((Label)p.FindControl("lblUseMyInfo")).Text + "," + ((RadioButtonList)p.FindControl("rbUseMyInfo")).SelectedValue + Environment.NewLine;
                strReturn += "Challenge," + "\"" + ((TextBox)p.FindControl("tbChallenge")).Text + "\"" + Environment.NewLine;
                strReturn += "Vibco Equipment," + "\"" + ((TextBox)p.FindControl("tbEquipment")).Text + "\"" + Environment.NewLine;
                strReturn += "Solution," + "\"" + ((TextBox)p.FindControl("tbSolution")).Text + "\"" + Environment.NewLine;
                strReturn += "Results," + "\"" + ((TextBox)p.FindControl("tbResults")).Text + "\"" + Environment.NewLine;
            }
            else
            {
                strReturn = ((Label)p.FindControl("lblUseMyInfo")).Text + " " + ((RadioButtonList)p.FindControl("rbUseMyInfo")).SelectedValue + Environment.NewLine + Environment.NewLine;
                strReturn += "Challenge: " + ((TextBox)p.FindControl("tbChallenge")).Text + Environment.NewLine;
                strReturn += "Vibco Equipment: " + ((TextBox)p.FindControl("tbEquipment")).Text + Environment.NewLine;
                strReturn += "Solution: " + ((TextBox)p.FindControl("tbSolution")).Text + Environment.NewLine;
                strReturn += "Results: " + ((TextBox)p.FindControl("tbResults")).Text + Environment.NewLine;
                strReturn += Environment.NewLine;
            } 

            return strReturn;

        }

        public string ParsePanel(Panel p, bool IsCSV = false)
        {
            String strReturn = "";

            foreach (Control c in p.Controls)
            {
                if (c.GetType() == typeof(Label))
                    strReturn += (IsCSV) ? ((Label)c).Text.Trim().Replace(":","") + "," : ((Label)c).Text + " ";
                if (c.GetType() == typeof(TextBox))
                    strReturn += (IsCSV) ? ((TextBox)c).Text + "," : ((TextBox)c).Text + Environment.NewLine;
                if (c.GetType() == typeof(RadComboBox))
                    strReturn += (IsCSV) ? ((RadComboBox)c).SelectedValue + "," : ((RadComboBox)c).SelectedValue + Environment.NewLine;
                if (c.GetType() == typeof(RadioButtonList))
                    strReturn += (IsCSV) ?  ((RadioButtonList)c).SelectedValue + "," : ((RadioButtonList)c).SelectedValue + Environment.NewLine;
                if (c.GetType() == typeof(CheckBoxList))
                    strReturn += (IsCSV) ? ParseCheckBoxGroup(((CheckBoxList)c)) + "," : ParseCheckBoxGroup(((CheckBoxList)c)) + Environment.NewLine;
                
            }
            strReturn += Environment.NewLine;
            return strReturn;
        }
        private string ParseNumericPageView(RadPageView pv,bool IsCSV = false)
        {
            String strReturn = ((HtmlGenericControl)pv.FindControl("title" + pv.Index.ToString())).InnerText + Environment.NewLine;
           
            for (int i = 0; i < pv.Controls.Count; i++)
            {
                if (pv.Controls[i] is RadNumericTextBox)
                {
                    
                    RadNumericTextBox rntb = (RadNumericTextBox)pv.Controls[i];
                    if (rntb.Text != "0" && rntb.Text != String.Empty)
                    {
                        if (IsCSV)
                            strReturn += rntb.Label.Trim().Replace(":","") + "," + rntb.Text + ",";
                        else
                            strReturn += rntb.Label + rntb.Text + Environment.NewLine;

                    } 
                }
            }

            strReturn += Environment.NewLine;
            return strReturn;

        }
      
        public string ParsePanelCSV(Panel p)
        {
            String strReturn = "";

            foreach (Control c in p.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                    strReturn += ((TextBox)c).Text + ",";
                if (c.GetType() == typeof(RadComboBox))
                    strReturn += ((RadComboBox)c).SelectedValue + ",";
                if (c.GetType() == typeof(RadioButtonList))
                    strReturn += ((RadioButtonList)c).SelectedValue + ",";
                if (c.GetType() == typeof(CheckBoxList))
                    strReturn += ParseCheckBoxGroup(((CheckBoxList)c)) + ",";

            }
            strReturn += Environment.NewLine;
            return strReturn;
        }


        public MemoryStream CreateFormCSV(string strCSV)
        {

             
            byte[] byteArray = Encoding.ASCII.GetBytes(strCSV);
            MemoryStream ms = new MemoryStream(byteArray);
            return ms;

        }

      
        public MemoryStream CreateUserInfoCSV(Panel p)
        {

            string strCSV = DateTime.Now.ToString("MMMM dd yyyy hh:mm tt") + "," + ParsePanelCSV(p);
            byte[] byteArray = Encoding.ASCII.GetBytes(strCSV);
            MemoryStream ms = new MemoryStream(byteArray);
            return ms;
            
        }
      

        public bool SendMail(string strBody,string strSubject)
        {
            MailMessage message = new MailMessage(ConfigurationManager.AppSettings["OurEmail"], "info@vibco.com");
            message.To.Add("karlw@vibco.com");

            message.Subject = strSubject;
            message.Body = strBody;
           
            
            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);
            //client.UseDefaultCredentials = true;
            client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["OurEmail"], ConfigurationManager.AppSettings["OurEmailPass"]);
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            
                try
                {
                    client.Send(message);
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            
        }
        public bool SendMail(string strBody,string strSubject,AttachmentCollection attachments)
        {
            MailMessage message = new MailMessage(ConfigurationManager.AppSettings["OurEmail"], "info@vibco.com");
            message.To.Add("karlw@vibco.com");
            
            message.Subject = strSubject;
            strBody = strSubject + " Form Submission" + Environment.NewLine + "Submitted on: " + DateTime.Now.ToString("MMMM dd, yyyy") + " at " + DateTime.Now.ToString("hh:mm tt") + Environment.NewLine + Environment.NewLine + strBody;
            message.Body = strBody;
            foreach (Attachment a in attachments)
                message.Attachments.Add(a);
            
            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);
            client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["OurEmail"], ConfigurationManager.AppSettings["OurEmailPass"]);
            client.UseDefaultCredentials = false;
            //client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            
                try
                {
                    client.Send(message);
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            
        }

        public bool SendThankYouMail(string strTo)
        {

            MailMessage message = new MailMessage(ConfigurationManager.AppSettings["OurEmail"], strTo);
            message.Subject = "Thank you for contacting VIBCO";
            message.Body = "Thank you for contacting VIBCO today.  A member of our support team will be in touch with you within 1 business day." + Environment.NewLine + Environment.NewLine + "In the meantime, if you need immediate assistance, please call 401-539-2392.  If you are in Canada, please contact our Canadian Sales Office directly at 905-828-4191." + Environment.NewLine + Environment.NewLine + "Best regards," + Environment.NewLine + Environment.NewLine + "The VIBCO Application Assistance Team";

            
            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);
            client.UseDefaultCredentials = true;


            try
            {
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
         
    }
}