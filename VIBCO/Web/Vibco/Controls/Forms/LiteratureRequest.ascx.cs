﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SitefinityWebApp.Forms;
using System.Net.Mail;

namespace SitefinityWebApp.Controls.Forms
{
    public partial class LiteratureRequest : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }
        public string ThankYouUrl
        {
            set;
            get;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Validate("AllValidators");
            if (Page.IsValid)
            {
                VibcoForms f = new VibcoForms();
                string strBody = "";
               strBody += f.ParseContactInformation(ContactInfoPanel) + Environment.NewLine;
            

                strBody += f.ParseNumericTextBoxMultiPage(mpLit);

                strBody += Environment.NewLine + "Comments: " + tbComments.Text + Environment.NewLine;

                MailMessage temp = new MailMessage();
                AttachmentCollection acl = temp.Attachments;
                acl.Add(new Attachment(f.CreateUserInfoCSV(ContactInfoPanel), "UserInfo.csv"));

                string strCSV = f.ParseNumericTextBoxMultiPage(mpLit, true);
                strCSV += "Comments," + tbComments.Text;
               
                acl.Add(new Attachment(f.CreateFormCSV(strCSV), "LitRequestFormData.csv"));

                if (f.SendMail(strBody, "Website Literature Request", acl))
                {
                    f.SendThankYouMail(f.GetUsersEmail(ContactInfoPanel));
                    Response.Redirect(ThankYouUrl);
                }
            }
            
        }
    }
}