﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VibcoSwag.ascx.cs" Inherits="SitefinityWebApp.Controls.Forms.VibcoSwag" %>

<div class="FormWrapper">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  ValidationGroup="AllValidators" CssClass="FormValidation"/>

  <asp:Panel ID="ContactInfoPanel" runat="server"  >
<div class="formHeaderLayout"><h2>Your Contact Infromation</h2>
<p>Please note that most contact information fields are required. </p>
<hr />
</div>

<div class="formLayout">
    <asp:Label ID="Label1" CssClass="FormLabel" runat="server" Text="Name: "></asp:Label><sup>*</sup><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbName" runat="server"></asp:TextBox><br />
     <div class="formdotted"></div>
    <asp:Label ID="Label2" CssClass="FormLabel" runat="server" Text="Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmail" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label3" CssClass="FormLabel" runat="server" Text="Confirm Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmailConfrim" runat="server"></asp:TextBox> <br />
     <div class="formsolid"></div>
    <asp:Label ID="Label4" CssClass="FormLabel" runat="server" Text="Company: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbCompany" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label5" CssClass="FormLabel" runat="server" Text="Industry: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbIndustry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label6" CssClass="FormLabel" runat="server" Text="Address: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbAddress" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label7" CssClass="FormLabel" runat="server" Text="Address Line 2: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbAddress2" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label8" CssClass="FormLabel" runat="server" Text="City: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCity" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label9" CssClass="FormLabel" runat="server" Text="State/Province: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbState" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label10" CssClass="FormLabel" runat="server" Text="Zip/Postal Code: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbZip" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label11" CssClass="FormLabel" runat="server" Text="Country: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCountry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label12" CssClass="FormLabel" runat="server" Text="Phone: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbPhone" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label13" CssClass="FormLabel" runat="server" Text="Fax: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbFax" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label14" CssClass="FormLabel" runat="server" Text="Mobile: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbMobile" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label15"  CssClass="FromRadio" runat="server" Text="Would you like to recieve our eNewsletter?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbNewsletter"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList>
    <br />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  Display="None" runat="server" ErrorMessage="Name is Required." ControlToValidate="tbName" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  Display="None" runat="server" ErrorMessage="Email address is Required." ControlToValidate="tbEmail" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  Display="None" runat="server" ErrorMessage="Confrim Email address is Required." ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Email Addresses do not match." ControlToCompare="tbEmail" ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:CompareValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ValidationGroup="AllValidators" Display="None"  
        ErrorMessage="Email: Not a valid Email Address" ControlToValidate="tbEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ValidationGroup="AllValidators" Display="None" 
        ErrorMessage="Confrim Email: Not a valid Email Address" 
        ControlToValidate="tbEmailConfrim" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  Display="None" runat="server" ErrorMessage="Address is Required." ControlToValidate="tbAddress" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="None" runat="server" ErrorMessage="City is Required." ControlToValidate="tbCity" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  Display="None" runat="server" ErrorMessage="State/Province is Required." ControlToValidate="tbState" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  Display="None" runat="server" ErrorMessage="Zip/Postal Code is Required." ControlToValidate="tbZip" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  Display="None" runat="server" ErrorMessage="Country is Required." ControlToValidate="tbCountry" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  Display="None" runat="server" ErrorMessage="Please select if you would like to recieve our eNewsletter." ControlToValidate="rbNewsletter" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>

    </div>   
 
    </asp:Panel>
    <br />
    
  <asp:Panel ID="Panel1" runat="server"  >
<div class="formHeaderLayout"><h2>VIBCO Gear and Cool Stuff</h2>
<hr />
</div>

<div class="formLayout">
<h3>Stickers</h3>  <!-- just add <img > tag in text area to show images when availabel-->
<br />
    <asp:CheckBoxList ID="cbStickers" runat="server" CssClass="FormlabelLarge" >
    <asp:ListItem Text="Hard Hat Set 1" Value = "Hard Hat Set 1"/>
    <asp:ListItem Text="Hard Hat Set 2" Value = "Hard Hat Set 2"/>
    </asp:CheckBoxList><br />
      <div class="formsolid"></div>
    <h3>Tattoos</h3>
    <br />
    <asp:CheckBoxList ID="cbTattoos" runat="server" CssClass="FormlabelLarge">
    <asp:ListItem Text="Big Bertha" Value = "Big Bertha"/>
    <asp:ListItem Text="Air Bertha" Value = "Air Bertha"/>
    <asp:ListItem Text="Hydra Bertha" Value = "Hydra Bertha"/>
    <asp:ListItem Text="SandBuster" Value = "SandBuster"/>
    <asp:ListItem Text="Vibration Nation Wings" Value = "Vibration Nation Wings"/>
    <asp:ListItem Text="Bulldog" Value = "Bulldog"/>
    <asp:ListItem Text="Skull" Value = "Skull"/>
    <asp:ListItem Text="Heart" Value = "Heart"/>
    <asp:ListItem Text="Flames" Value = "Flames"/>

    </asp:CheckBoxList>  <br />

</div></asp:Panel>
 
</div>
    <asp:Button ID="Button1" CssClass="submitButton" runat="server" Text="" 
    onclick="Button1_Click" ValidationGroup="AllValidators" />