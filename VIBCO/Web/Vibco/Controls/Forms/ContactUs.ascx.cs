﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SitefinityWebApp.Forms;
using System.Net.Mail;

namespace SitefinityWebApp.Controls.Forms
{
    public partial class ContactUs : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string ThankYouUrl
        {
            set;
            get;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Validate("AllValidators");
            if (Page.IsValid)
            {
                VibcoForms f = new VibcoForms();
                string strBody = "";

                strBody += f.ParseContactInformation(ContactInfoPanel) + Environment.NewLine;


                strBody += Environment.NewLine + "Comments: " + tbComments.Text + Environment.NewLine;

                MailMessage temp = new MailMessage();
                AttachmentCollection acl = temp.Attachments;
                acl.Add(new Attachment(f.CreateUserInfoCSV(ContactInfoPanel), "UserInfo.csv"));


                if (f.SendMail(strBody, "Contact Us",acl))
                {
                    f.SendThankYouMail(f.GetUsersEmail(ContactInfoPanel));
                    Response.Redirect(ThankYouUrl);
                }
            }

        }
    }
}