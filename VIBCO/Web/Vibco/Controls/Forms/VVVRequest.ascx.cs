﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using SitefinityWebApp.Forms;
using Telerik.Web.UI;
using System.Net.Mail;

namespace SitefinityWebApp.Controls.Forms
{
    public partial class VVVRequest : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string ThankYouUrl
        {
            set;
            get;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Validate("AllValidators");
            if (Page.IsValid)
            {
                VibcoForms f = new VibcoForms();
                string strBody = "";
                strBody += f.ParseContactInformation(ContactInfoPanel) + Environment.NewLine;


                strBody += f.ParseFreeTrialPS(PowerSource);
                strBody += f.ParseFreeTrialMT(Material);

                strBody += Environment.NewLine + "Comments: " + tbComments.Text + Environment.NewLine;

                MailMessage temp = new MailMessage();
                AttachmentCollection acl = temp.Attachments;
                acl.Add(new Attachment(f.CreateUserInfoCSV(ContactInfoPanel), "UserInfo.csv"));

                string strCSV = f.ParseFreeTrialPS(PowerSource,true);
                strCSV += f.ParseFreeTrialMT(Material,true);
                strCSV += "Comments," + tbComments.Text;
                acl.Add(new Attachment(f.CreateFormCSV(strCSV), "VVVFormData.csv"));


                if (RadUpload1.UploadedFiles.Count > 0)
                {
                    foreach (UploadedFile validFile in RadUpload1.UploadedFiles)
                    {
                        Attachment a = new Attachment(validFile.InputStream, validFile.FileName);
                        acl.Add(a);
                    }
                }

                if (f.SendMail(strBody, "Van Visit Request", acl))
                {
                    f.SendThankYouMail(f.GetUsersEmail(ContactInfoPanel));
                    Response.Redirect(ThankYouUrl);
                }
            }

        }
    }
}