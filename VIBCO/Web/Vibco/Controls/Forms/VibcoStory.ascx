﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VibcoStory.ascx.cs" Inherits="SitefinityWebApp.Controls.Forms.VibcoStory" %>
    
      <script language="javascript">

          function cbTermsClientValidation(source, args)
    
      {

          args.IsValid = document.all["cbTerms"].checked;

       }
       function cbConsentClientValidation(source, args) {

           args.IsValid = document.all["cbConsent"].checked;

       }
  
      </script> 



<div class="FormWrapper">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  ValidationGroup="AllValidators" CssClass="FormValidation"/>

  <asp:Panel ID="ContactInfoPanel" runat="server"  >
<div class="formHeaderLayout"><h2>Your Contact Infromation</h2>
<p>Please note that most contact information fields are required. </p>
<hr />
</div>

<div class="formLayout">
    <asp:Label ID="Label1" CssClass="FormLabel" runat="server" Text="Name: "></asp:Label><sup>*</sup><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbName" runat="server"></asp:TextBox><br />
     <div class="formdotted"></div>
    <asp:Label ID="Label2" CssClass="FormLabel" runat="server" Text="Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmail" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label3" CssClass="FormLabel" runat="server" Text="Confirm Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmailConfrim" runat="server"></asp:TextBox> <br />
     <div class="formsolid"></div>
    <asp:Label ID="Label4" CssClass="FormLabel" runat="server" Text="Company: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbCompany" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label5" CssClass="FormLabel" runat="server" Text="Industry: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbIndustry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label6" CssClass="FormLabel" runat="server" Text="Address: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbAddress" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label7" CssClass="FormLabel" runat="server" Text="Address Line 2: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbAddress2" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label8" CssClass="FormLabel" runat="server" Text="City: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCity" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label9" CssClass="FormLabel" runat="server" Text="State/Province: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbState" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label10" CssClass="FormLabel" runat="server" Text="Zip/Postal Code: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbZip" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label11" CssClass="FormLabel" runat="server" Text="Country: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCountry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label12" CssClass="FormLabel" runat="server" Text="Phone: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbPhone" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label13" CssClass="FormLabel" runat="server" Text="Fax: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbFax" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label14" CssClass="FormLabel" runat="server" Text="Mobile: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbMobile" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label15"  CssClass="FromRadio" runat="server" Text="Would you like to recieve our eNewsletter?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbNewsletter"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList>
    <br />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  Display="None" runat="server" ErrorMessage="Name is Required." ControlToValidate="tbName" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  Display="None" runat="server" ErrorMessage="Email address is Required." ControlToValidate="tbEmail" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  Display="None" runat="server" ErrorMessage="Confrim Email address is Required." ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Email Addresses do not match." ControlToCompare="tbEmail" ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:CompareValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ValidationGroup="AllValidators" Display="None"  
        ErrorMessage="Email: Not a valid Email Address" ControlToValidate="tbEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ValidationGroup="AllValidators" Display="None" 
        ErrorMessage="Confrim Email: Not a valid Email Address" 
        ControlToValidate="tbEmailConfrim" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  Display="None" runat="server" ErrorMessage="Address is Required." ControlToValidate="tbAddress" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="None" runat="server" ErrorMessage="City is Required." ControlToValidate="tbCity" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  Display="None" runat="server" ErrorMessage="State/Province is Required." ControlToValidate="tbState" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  Display="None" runat="server" ErrorMessage="Zip/Postal Code is Required." ControlToValidate="tbZip" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  Display="None" runat="server" ErrorMessage="Country is Required." ControlToValidate="tbCountry" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  Display="None" runat="server" ErrorMessage="Please select if you would like to recieve our eNewsletter." ControlToValidate="rbNewsletter" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    

    </div>   
 
    </asp:Panel>
    <br />
    <div class="formHeaderLayout">
        <hr /></div>
    <asp:Panel ID="vstory" runat="server">
  
 <div class="formLayout" id="TextBoxes"> 
         <asp:Label ID="lblUseMyInfo"  CssClass="FromRadio" runat="server" Text="May we use the information in a Product Application Bulletin?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbUseMyInfo"  RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList><br /><br />
     <p class="FormLabelP">Challenge - Tell us about the situation BEFORE you installed VIBCO equipment. Be sure to include details on flow rates, material, noise levels, quality issues or anything else that helps us to understand your challenge."</p>
    <asp:TextBox ID="tbChallenge" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
    <p class="FormLabelP">VIBCO Equipment - Tell us which VIBCO product solved your problem. Please provide specific equipment quantities and model numbers, if available. Also, please let us know if you used VIBCO's technical support services (sizing and selection assistance, virtual van visit, etc.).</p>
    <asp:TextBox ID="tbEquipment" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
    <p class="FormLabelP">Solution - Tell us how a VIBCO product resolved your problem. Be sure to include details on how you installed or used your VIBCO equipment.</p>
    <asp:TextBox ID="tbSolution" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
    <p class="FormLabelP">Results - Tell us about the situation AFTER you installed a VIBCO Vibrator. Be sure to include details on increased productivity, money saved, improved quality or any other benefits you received by installing or using a VIBCO vibration product.</p>
    <asp:TextBox ID="tbResults" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />

       </div>
         </asp:Panel>
     <div class="formHeaderLayout">   <h2 id="title2" runat="server">Photo Upload</h2>
        <p>While not required, a photo is often the best way for us to assess your needs and make a proper recommendation. It will help to take the guesswork out of the process and can save time and money. Please upload photos to attach to your inquiry. For more information on how photos can help, go to our <a href="/industries-applications/vibco-van-visit">Virutal Van Vist</a> page. </p>
        <p><b>NOTE: Only .jpg , .jpeg , .gif , .bmp , .pdf , .png , .dwg , .tiff , .svg , .psd , .raw , .eps , .step , .iges , .igs , .dxf files are allowed for upload. </b></p>
        <hr /></div>
 <div class="formLayout" id="UploadForm"> 
   <telerik:RadUpload
                                ID="RadUpload1"
                                runat="server"
                                InitialFileInputsCount="3"
                                MaxFileInputsCount="10" 
                                
         AllowedFileExtensions=".jpg,.jpeg,.gif,.bmp,.pdf,.png,.dwg,.tiff,.svg,.psd,.raw,.eps,.step,.iges,.igs,.dxf" 
         Skin="Simple" />


      </div>
      <br />

      <div class="formHeaderLayout">
        <hr /></div>
 <div class="formLayout" id="Div1"> 
   <asp:Label ID="Label17"  CssClass="FromRadio" runat="server" Text="Consent:"></asp:Label><sup>*</sup><br />
     <asp:CheckBox ID="cbConsent" runat="server" CssClass="FormLabelP"  Text=" By checking this box, I agree that VIBCO may use my story and photos to create training, informational, or promotional materials and that any information submitted becomes the sole property of VIBCO Vibrators."/><br />

 

 <asp:Label ID="Label18"  CssClass="FromRadio" runat="server" Text="Terms and Conditions:"></asp:Label><sup>*</sup><br />
     <asp:CheckBox ID="cbTerms" runat="server" CssClass="FormLabelP"   Text=" By checking this box, I agree that VIBCO may reject photos based on quality or content and that I will not receive payment if VIBCO rejects my photo."/><br />

 </div>
 <br />
      </div>
    <asp:Button ID="Button1" CssClass="submitButton" runat="server" Text="" 
    onclick="Button1_Click" ValidationGroup="AllValidators" />

<asp:RequiredFieldValidator ID="RequiredFieldValidator10"  Display="None" runat="server" ErrorMessage="Please select if we may use the information in a Product Application Bulletin." ControlToValidate="rbUseMyInfo" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
<asp:CustomValidator ID="CustomValidator" ValidationGroup="AllValidators" runat="server"  ErrorMessage="Please check that you agree to Consent." Text="Please check that you agree to Consent." OnServerValidate="cbConsent_ServerValidate" Display="None"  ClientValidationFunction="cbConsentClientValidation"></asp:CustomValidator>  
<asp:CustomValidator ID="CustomValidator1" ValidationGroup="AllValidators" runat="server" ErrorMessage="Please check that you agree to the Terms and Conditions" Text="Please check that you agree to the Terms and Conditions." OnServerValidate="cbTerms_ServerValidate" Display="None" ClientValidationFunction="cbTermsClientValidation"></asp:CustomValidator>


