﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SitefinityWebApp.Forms;
using Telerik.Web.UI;
using System.Net.Mail;

namespace SitefinityWebApp.Controls.Forms
{
    public partial class ApplicationDataInquiry : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string ThankYouUrl
        {
            set;
            get;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Validate("AllValidators");
            
            if (Page.IsValid)
            {
                VibcoForms f = new VibcoForms();

                string strBody = "";
                strBody += f.ParseContactInformation(ContactInfoPanel) + Environment.NewLine;


                strBody += f.ParseFreeTrialPS(PowerSource);
                strBody += f.ParseFreeTrialMT(Material);

                strBody += "Application Description" + Environment.NewLine + Environment.NewLine;
                strBody += "Bin" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(Bin);
                strBody += "Chute" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(Chute);
                strBody += "Walls and Columns" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(WallsandColumns);
                strBody += "Concrete Forms: Precast, Ornamental and Other Concrete Forms" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(ConcreteForms);
                strBody += "Vibrating Table" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(VibratingTable);
                strBody += "Dump Bodies, Spreaders and Other Vehicles" + Environment.NewLine + Environment.NewLine;
                strBody += f.ParsePanel(DumpBodies);

                strBody += Environment.NewLine + "Comments: " + tbComments.Text + Environment.NewLine;

                MailMessage temp = new MailMessage();
                AttachmentCollection acl = temp.Attachments;
                acl.Add(new Attachment(f.CreateUserInfoCSV(ContactInfoPanel), "UserInfo.csv"));
                
                string strCSV = f.ParseFreeTrialPS(PowerSource, true);
                strCSV += f.ParseFreeTrialMT(Material, true);
                strCSV += "Application Description" + Environment.NewLine;
                strCSV += "Bin" + Environment.NewLine;
                strCSV += f.ParsePanel(Bin,true);
                strCSV += "Chute" + Environment.NewLine;
                strCSV += f.ParsePanel(Chute,true);
                strCSV += "Walls and Columns" + Environment.NewLine;
                strCSV += f.ParsePanel(WallsandColumns,true);
                strCSV += "Concrete Forms: Precast, Ornamental and Other Concrete Forms" + Environment.NewLine;
                strCSV += f.ParsePanel(ConcreteForms,true);
                strCSV += "Vibrating Table" + Environment.NewLine;
                strCSV += f.ParsePanel(VibratingTable,true);
                strCSV += "Dump Bodies, Spreaders and Other Vehicles" + Environment.NewLine;
                strCSV += f.ParsePanel(DumpBodies,true);
                strCSV += "Comments," + tbComments.Text;
                
                acl.Add(new Attachment(f.CreateFormCSV(strCSV), "AppDataInquiryFormData.csv"));

                if(RadUpload1.UploadedFiles.Count > 0)
                 {
                    foreach(UploadedFile validFile in RadUpload1.UploadedFiles)
                    {
                        Attachment a = new Attachment(validFile.InputStream, validFile.FileName);
                        acl.Add(a);
                    }
                }


                if (f.SendMail(strBody, "Application Data Inquiry",acl))
                {
                    f.SendThankYouMail(f.GetUsersEmail(ContactInfoPanel));
                    Response.Redirect(ThankYouUrl);
                }
            }

        }
    }
}