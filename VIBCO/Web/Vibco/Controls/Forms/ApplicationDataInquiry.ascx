﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationDataInquiry.ascx.cs" Inherits="SitefinityWebApp.Controls.Forms.ApplicationDataInquiry" %>
<div class="FormWrapper">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  ValidationGroup="AllValidators" CssClass="FormValidation"/>

  <asp:Panel ID="ContactInfoPanel" runat="server"  >
<div class="formHeaderLayout"><h2>Your Contact Infromation</h2>
<p>Please note that most contact information fields are required. </p>
<hr />
</div>

<div class="formLayout">
    <asp:Label ID="Label1" CssClass="FormLabel" runat="server" Text="Name: "></asp:Label><sup>*</sup><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbName" runat="server"></asp:TextBox><br />
     <div class="formdotted"></div>
    <asp:Label ID="Label2" CssClass="FormLabel" runat="server" Text="Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmail" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label3" CssClass="FormLabel" runat="server" Text="Confirm Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmailConfrim" runat="server"></asp:TextBox> <br />
     <div class="formsolid"></div>
    <asp:Label ID="Label4" CssClass="FormLabel" runat="server" Text="Company: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbCompany" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label5" CssClass="FormLabel" runat="server" Text="Industry: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbIndustry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label6" CssClass="FormLabel" runat="server" Text="Address: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbAddress" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label7" CssClass="FormLabel" runat="server" Text="Address Line 2: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbAddress2" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label8" CssClass="FormLabel" runat="server" Text="City: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCity" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label9" CssClass="FormLabel" runat="server" Text="State/Province: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbState" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label10" CssClass="FormLabel" runat="server" Text="Zip/Postal Code: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbZip" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label11" CssClass="FormLabel" runat="server" Text="Country: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCountry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label12" CssClass="FormLabel" runat="server" Text="Phone: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbPhone" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label13" CssClass="FormLabel" runat="server" Text="Fax: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbFax" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label14" CssClass="FormLabel" runat="server" Text="Mobile: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbMobile" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label15"  CssClass="FromRadio" runat="server" Text="Would you like to recieve our eNewsletter?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbNewsletter"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList>
    <br />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  Display="None" runat="server" ErrorMessage="Name is Required." ControlToValidate="tbName" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  Display="None" runat="server" ErrorMessage="Email address is Required." ControlToValidate="tbEmail" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  Display="None" runat="server" ErrorMessage="Confrim Email address is Required." ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Email Addresses do not match." ControlToCompare="tbEmail" ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:CompareValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ValidationGroup="AllValidators" Display="None"  
        ErrorMessage="Email: Not a valid Email Address" ControlToValidate="tbEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ValidationGroup="AllValidators" Display="None" 
        ErrorMessage="Confrim Email: Not a valid Email Address" 
        ControlToValidate="tbEmailConfrim" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  Display="None" runat="server" ErrorMessage="Address is Required." ControlToValidate="tbAddress" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="None" runat="server" ErrorMessage="City is Required." ControlToValidate="tbCity" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  Display="None" runat="server" ErrorMessage="State/Province is Required." ControlToValidate="tbState" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  Display="None" runat="server" ErrorMessage="Zip/Postal Code is Required." ControlToValidate="tbZip" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  Display="None" runat="server" ErrorMessage="Country is Required." ControlToValidate="tbCountry" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  Display="None" runat="server" ErrorMessage="Please select if you would like to recieve our eNewsletter." ControlToValidate="rbNewsletter" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>

    </div>   
 
    </asp:Panel>
    <br />
   <div class="formHeaderLayout"> <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="formTabStrip" EnableEmbeddedSkins="false" MultiPageID="mpLit"
                SelectedIndex="0" Width="100%" >
                  <Tabs>
                    <telerik:RadTab Text="General Application Information" PageViewID="GenApp">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Application Description" PageViewID="AppDesc">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Upload Your Application Photo" PageViewID="Upload">
                    </telerik:RadTab>
                    
                 </Tabs>
    </telerik:RadTabStrip></div>
    <telerik:RadMultiPage ID="mpLit" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="GenApp" runat="server">
      
     <div class="formHeaderLayout">   <h2 id="title0" runat="server">General Application Information</h2>
        <p>Please provide as much information as possible about your available power supply and the material that you will be vibrating.</p>
        <hr /></div>
 <div class="formLayout" id="GenearlAppInfo"> 
    <asp:Panel ID="PowerSource" runat="server">
  
     <div class="formHeaderLayout">   <h2 id="H1" runat="server">Power Source</h2>
         
        <hr /></div>
 <div class="formLayout" id="PowerSource"> 
 <h3>Pneumatic:</h3><br />
    <asp:Label ID="Label18" CssClass="FormLabel" runat="server" Text="Air PSI: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbAirPSI" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label19" CssClass="FormLabel" runat="server" Text="Air CFM: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbAirCFM" runat="server"></asp:TextBox><br />
           <div class="formsolid"></div>
 <h3>Hydraulic:</h3><br />
    <asp:Label ID="Label20" CssClass="FormLabel" runat="server" Text="Hydraulic PSI: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbHPSI" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label21" CssClass="FormLabel" runat="server" Text="Hydraulic GPM: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbHGPM" runat="server"></asp:TextBox><br />
    
           <div class="formsolid"></div>
            <h3>Electric:</h3><br />
     <asp:Label ID="Label16" CssClass="FormLabel" runat="server" Text="Current: "></asp:Label>
     <telerik:RadComboBox ID="rcbCurrent"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="AC" Value="AC" />
             <telerik:RadComboBoxItem runat="server" Text="DC" Value="DC" />
         </Items>
     </telerik:RadComboBox>     <br />
     <asp:Label ID="Label22" CssClass="FormLabel" runat="server" Text="Volts: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbVolts" runat="server"></asp:TextBox><br />
       
      <asp:Label ID="Label17" CssClass="FormLabel" runat="server" Text="Phase: "></asp:Label>
      <telerik:RadComboBox ID="rcbPhase"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Single" Value="AC" />
             <telerik:RadComboBoxItem runat="server" Text="Three" Value="DC" />
         </Items>
     </telerik:RadComboBox>      <br />
    <asp:Label ID="Label23" CssClass="FormLabel" runat="server" Text="Cycle: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbCycle" runat="server"></asp:TextBox><br /> 
          <div class="formsolid"></div>
           
      </div>
        </asp:Panel>
            <asp:Panel ID="Material" runat="server">
  
     <div class="formHeaderLayout">   <h2 id="H2" runat="server">Material</h2>
         
        <hr /></div>
 <div class="formLayout" id="Div1"> 
  
    <asp:Label ID="Label24" CssClass="FormLabel" runat="server" Text="Material Type: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbType" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label25" CssClass="FormLabel" runat="server" Text="Weight: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbWeight" runat="server"></asp:TextBox><br />
           
    <asp:Label ID="Label26" CssClass="FormLabel" runat="server" Text="Moisture %: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbMoisture" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label27" CssClass="FormLabel" runat="server" Text="Discharge: "></asp:Label> <telerik:RadComboBox ID="rcbDischage"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Screw Conveyor" Value="Screw Conveyor" />
             <telerik:RadComboBoxItem runat="server" Text="Belt Feeder" Value="Belt Feeder" />
             <telerik:RadComboBoxItem runat="server" Text="Clam Shell" Value="Clam Shell" />
             <telerik:RadComboBoxItem runat="server" Text="Gravity" Value="Gravity" />
             <telerik:RadComboBoxItem runat="server" Text="Knife Gate" Value="Knife Gate" />
             <telerik:RadComboBoxItem runat="server" Text="Slide Gate" Value="Slide Gate" />
             <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
         </Items>
     </telerik:RadComboBox>      <br />
     <asp:Label ID="Label28" CssClass="FormLabel" runat="server" Text="Flow Problem: "></asp:Label> <telerik:RadComboBox ID="rcbFlow"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Bridging" Value="Bridging" />
             <telerik:RadComboBoxItem runat="server" Text="Clogging" Value="Clogging" />
             <telerik:RadComboBoxItem runat="server" Text="Ratholing" Value="Ratholing" />
             <telerik:RadComboBoxItem runat="server" Text="Arching" Value="Arching" />
             <telerik:RadComboBoxItem runat="server" Text="Sticking" Value="Sticking" />
             <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             

         </Items>
     </telerik:RadComboBox>      <br />
      </div>
        </asp:Panel>
      </div>
 

        </telerik:RadPageView>
         <telerik:RadPageView ID="AppDesc" runat="server">
      
     <div class="formHeaderLayout">   <h2 id="title1" runat="server">Application Description</h2>
        <p>Please locate your application type (bin, chute, concrete form, vibrating table, etc.) and provide as much information as possible <b>for that option only</b>. If you are providing information for multiple application types, please indicate that you are requesting information on multiple applications in the comments area.</p>
        <hr /></div>

 <div class="formLayout" id="AppDescription"> 
    <asp:Panel ID="Bin" runat="server">
       <div class="formHeaderLayout">   <h2 id="H3" runat="server">Bin</h2>
        <hr /></div>
 <div class="formLayout" id="BinForm"> 
       <div style="width:33%;float:left;"><img width="125" height="153" src="/images/form-images/2011/02/21/inquiry_form_bin.jpg?Status=Master" alt="inquiry_form_bin"/>   </div>
       <div style="width:66%;float:right;">
         <asp:Label ID="Label29" CssClass="FormLabel" runat="server" Text="Bin Type: "></asp:Label> <telerik:RadComboBox ID="rcbBinType"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Rectangular" Value="Rectangular" />
             <telerik:RadComboBoxItem runat="server" Text="Conical" Value="Conical" />
             <telerik:RadComboBoxItem runat="server" Text="Cylindrical" Value="Cylindrical" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
           </Items>
     </telerik:RadComboBox>      <br />
     <asp:Label ID="Label30" CssClass="FormLabel" runat="server" Text="Length: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox1" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label31" CssClass="FormLabel" runat="server" Text="Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox2" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label32" CssClass="FormLabel" runat="server" Text="Height: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox3" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label33" CssClass="FormLabel" runat="server" Text="Wall Thickness: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox4" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label34" CssClass="FormLabel" runat="server" Text="Discharge Opening: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox5" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label35" CssClass="FormLabel" runat="server" Text="Discharge: "></asp:Label><telerik:RadComboBox ID="RadComboBox1"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Center" Value="Center" />
             <telerik:RadComboBoxItem runat="server" Text="Side" Value="Side" />
             <telerik:RadComboBoxItem runat="server" Text="End" Value="End" />
             
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label36" CssClass="FormLabel" runat="server" Text="Bin Construction: "></asp:Label><telerik:RadComboBox ID="RadComboBox2"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
             <telerik:RadComboBoxItem runat="server" Text="Wood" Value="Wood" />
             <telerik:RadComboBoxItem runat="server" Text="Concrete" Value="Concrete" />
            <telerik:RadComboBoxItem runat="server" Text="Stainless Steel" Value="Stainless Steel" />
            <telerik:RadComboBoxItem runat="server" Text="Aluminum" Value="Aluminum" />
            <telerik:RadComboBoxItem runat="server" Text="Fiberglass" Value="Fiberglass" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
           </Items>
     </telerik:RadComboBox> <br />
     </div>
     <br class="clearall" />
    

    </div></asp:Panel>
    
     <asp:Panel ID="Chute" runat="server">
       <div class="formHeaderLayout">   <h2 id="H4" runat="server">Chute</h2>
        <hr /></div>
 <div class="formLayout" id="ChuteForm"> 
       <div style="width:33%;float:left;"><img width="125" height="70" src="/images/form-images/2011/02/21/inquiry_form_chute.jpg?Status=Master" alt="inquiry_form_chute"/>  </div>
       <div style="width:66%;float:right;">
          
     <asp:Label ID="Label38" CssClass="FormLabel" runat="server" Text="Length: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox6" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label39" CssClass="FormLabel" runat="server" Text="Top Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox7" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label40" CssClass="FormLabel" runat="server" Text="End Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox8" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label41" CssClass="FormLabel" runat="server" Text="Wall Thickness: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox9" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label42" CssClass="FormLabel" runat="server" Text="Height of Side Wall: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox10" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label37" CssClass="FormLabel" runat="server" Text="Degree of Slope: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox11" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label43" CssClass="FormLabel" runat="server" Text="Mounting: "></asp:Label><telerik:RadComboBox ID="RadComboBox4"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Hanging" Value="Hanging" />
             <telerik:RadComboBoxItem runat="server" Text="In Place" Value="In Place" />
             <telerik:RadComboBoxItem runat="server" Text="Mobile" Value="Mobile" />
             <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label44" CssClass="FormLabel" runat="server" Text="Chute Material: "></asp:Label><telerik:RadComboBox ID="RadComboBox5"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
             <telerik:RadComboBoxItem runat="server" Text="Wood" Value="Wood" />
             <telerik:RadComboBoxItem runat="server" Text="Concrete" Value="Concrete" />
            <telerik:RadComboBoxItem runat="server" Text="Stainless Steel" Value="Stainless Steel" />
            <telerik:RadComboBoxItem runat="server" Text="Aluminum" Value="Aluminum" />
            <telerik:RadComboBoxItem runat="server" Text="Fiberglass" Value="Fiberglass" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
           </Items>
     </telerik:RadComboBox> <br />
     </div>
     <br class="clearall" />
    

    </div></asp:Panel>

     <asp:Panel ID="WallsandColumns" runat="server">
       <div class="formHeaderLayout">   <h2 id="H5" runat="server">Walls and Columns</h2>
        <hr /></div>
 <div class="formLayout" id="WallColForm"> 
       <div style="width:33%;float:left;"><img width="125" height="104" src="/images/form-images/2011/02/21/inquiry_form_wall_column.jpg?Status=Master" alt="inquiry_form_wall_column"/>  </div>
       <div style="width:66%;float:right;">
          
     <asp:Label ID="Label45" CssClass="FormLabel" runat="server" Text="Height: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox12" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label46" CssClass="FormLabel" runat="server" Text="Form Manufacturer: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox13" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label47" CssClass="FormLabel" runat="server" Text="Form Model #: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox14" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label48" CssClass="FormLabel" runat="server" Text="Pour Date: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox15" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label49" CssClass="FormLabel" runat="server" Text="Wall Thickness: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox16" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label50" CssClass="FormLabel" runat="server" Text="Finish: "></asp:Label><telerik:RadComboBox ID="RadComboBox7"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Both Sides" Value="Both Sides" />
             <telerik:RadComboBoxItem runat="server" Text="One Side" Value="One Side" />
             
             
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label51" CssClass="FormLabel" runat="server" Text="Vibrator Mount: "></asp:Label><telerik:RadComboBox ID="RadComboBox3"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Clamp" Value="Clamp" />
             <telerik:RadComboBoxItem runat="server" Text="Wedge" Value="Wedge" />
             <telerik:RadComboBoxItem runat="server" Text="Lug" Value="Lug" />
            
             
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label52" CssClass="FormLabel" runat="server" Text="Form Material: "></asp:Label><telerik:RadComboBox ID="RadComboBox6"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
             <telerik:RadComboBoxItem runat="server" Text="Wood" Value="Wood" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
           </Items>
     </telerik:RadComboBox> <br />
     </div>
     <br class="clearall" />
    

    </div></asp:Panel>

    <asp:Panel ID="ConcreteForms" runat="server">
       <div class="formHeaderLayout">   <h2 id="H6" runat="server">Concrete Forms: Precast, Ornamental and Other Concrete Forms</h2>
        <hr /></div>
 <div class="formLayout" id="ConcreteForm"> 
       <div style="width:33%;float:left;"><img width="125" height="125" src="/images/form-images/2011/02/21/inquiry_form_precast.jpg?Status=Master" alt="inquiry_form_precast"/>  </div>
       <div style="width:66%;float:right;">
            <asp:Label ID="Label61"  CssClass="FromRadio" runat="server" Text="Is this a request for assistance with a concrete counter top application?"></asp:Label><br />
    <asp:RadioButtonList ID="RadioButtonList1"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList> <br />

     <asp:Label ID="Label53" CssClass="FormLabel" runat="server" Text="Length: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox17" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label54" CssClass="FormLabel" runat="server" Text="Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox18" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label55" CssClass="FormLabel" runat="server" Text="Depth: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox19" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label56" CssClass="FormLabel" runat="server" Text="Total Weight of Filled Form: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox20" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label60" CssClass="FormLabel" runat="server" Text="Form or Mold Material: "></asp:Label><telerik:RadComboBox ID="RadComboBox10"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
             <telerik:RadComboBoxItem runat="server" Text="Wood" Value="Wood" />
             <telerik:RadComboBoxItem runat="server" Text="Concrete" Value="Concrete" />
             <telerik:RadComboBoxItem runat="server" Text="Latex Lined" Value="Latex Lined" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
           </Items>
     </telerik:RadComboBox> <br />
     </div>
     <br class="clearall" />
    

    </div></asp:Panel>

    <asp:Panel ID="VibratingTable" runat="server">
       <div class="formHeaderLayout">   <h2 id="H7" runat="server">Vibrating Table</h2>
        <hr /></div>
 <div class="formLayout" id="VibTableForm"> 
       <div style="width:33%;float:left;"><img width="125" height="126" src="/images/form-images/2011/02/21/inquiry_form_table.jpg?Status=Master" alt="inquiry_form_table"/>  </div>
       <div style="width:66%;float:right;">
            <asp:Label ID="Label57"  CssClass="FromRadio" runat="server" Text="Is this a request for information on a complete vibrating table or for a vibrator for an existing vibrating table?"></asp:Label><br />
    <asp:RadioButtonList ID="RadioButtonList2"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormRBLLarge"  >
    <asp:ListItem Value="Complete New Table" Text="Complete New Table&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="Existing Vibrating Table" Text="Existing Vibrating Table"  ></asp:ListItem>
    </asp:RadioButtonList> <br /><br />

     <asp:Label ID="Label58" CssClass="FormLabel" runat="server" Text="Length: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox21" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label59" CssClass="FormLabel" runat="server" Text="Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox22" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label62" CssClass="FormLabel" runat="server" Text="Height: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox23" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label64" CssClass="FormLabel" runat="server" Text="Table Construction: "></asp:Label><telerik:RadComboBox ID="RadComboBox8"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
                    <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
             <telerik:RadComboBoxItem runat="server" Text="Wood" Value="Wood" />
            <telerik:RadComboBoxItem runat="server" Text="Stainless Steel" Value="Stainless Steel" />
            <telerik:RadComboBoxItem runat="server" Text="Aluminum" Value="Aluminum" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             
           </Items>
     </telerik:RadComboBox> <br />

         <asp:Label ID="Label63" CssClass="FormLabel" runat="server" Text="Table Type: "></asp:Label><telerik:RadComboBox ID="RadComboBox9"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
                    <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Grid Top" Value="Grid Top" />
             <telerik:RadComboBoxItem runat="server" Text="Flat Top" Value="Flat Top" />
            <telerik:RadComboBoxItem runat="server" Text="Test Table" Value="Test Table" />
            <telerik:RadComboBoxItem runat="server" Text="Platform" Value="Platform" />
            <telerik:RadComboBoxItem runat="server" Text="Drum Packer" Value="Drum Packer" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label65"  CssClass="FromRadio" runat="server" Text="Are you using this table for concrete?"></asp:Label><br />
    <asp:RadioButtonList ID="RadioButtonList3"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel" >
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList> <br />
     </div>
     <br class="clearall" />
    

    </div></asp:Panel>
     <asp:Panel ID="DumpBodies" runat="server">
       <div class="formHeaderLayout">   <h2 id="H8" runat="server">Dump Bodies, Spreaders and Other Vehicles</h2>
        <hr /></div>
 <div class="formLayout" id="DumpForm"> 
       <div style="width:33%;float:left;"><img width="125" height="122" src="/images/form-images/2011/02/21/inquiry_form_truck.jpg?Status=Master" alt="inquiry_form_truck"/>  </div>
       <div style="width:66%;float:right;">
            
     <asp:Label ID="Label67" CssClass="FormLabel" runat="server" Text="Length: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox24" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label68" CssClass="FormLabel" runat="server" Text="Width: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox25" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label69" CssClass="FormLabel" runat="server" Text="Height: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox26" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label66" CssClass="FormLabel" runat="server" Text="Yardage: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="TextBox27" runat="server"></asp:TextBox><br />
     <asp:Label ID="Label70" CssClass="FormLabel" runat="server" Text="Body Style: "></asp:Label><telerik:RadComboBox ID="RadComboBox11"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
                    <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="V-Box Spreader" Value="V-Box Spreader" />
             <telerik:RadComboBoxItem runat="server" Text="Tailgate Spreader" Value="Tailgate Spreader" />
            <telerik:RadComboBoxItem runat="server" Text="Bulk Trailer" Value="Bulk Trailer" />
                   
           </Items>
     </telerik:RadComboBox> <br />

         <asp:Label ID="Label71" CssClass="FormLabel" runat="server" Text="Body Construction: "></asp:Label><telerik:RadComboBox ID="RadComboBox12"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
                    <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Aluminum" Value="Aluminum" />
             <telerik:RadComboBoxItem runat="server" Text="Steel" Value="Steel" />
            <telerik:RadComboBoxItem runat="server" Text="Stainless Steel" Value="Stainless Steel" />
            <telerik:RadComboBoxItem runat="server" Text="Plastic" Value="Plastic" />
            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             
           </Items>
     </telerik:RadComboBox> <br />

     <asp:Label ID="Label72" CssClass="FormLabel" runat="server" Text="Available Power: "></asp:Label><telerik:RadComboBox ID="RadComboBox13"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
                    <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="12 Volt" Value="12 Volt" />
             <telerik:RadComboBoxItem runat="server" Text="24 Volt" Value="24 Volt" />
            <telerik:RadComboBoxItem runat="server" Text="Pneumatic" Value="Pneumatic" />
            <telerik:RadComboBoxItem runat="server" Text="Hydraulic" Value="Hydraulic" />
           </Items>
     </telerik:RadComboBox> <br />
     <asp:Label ID="Label73" CssClass="FormLabel" runat="server" Text="Body Features: "></asp:Label>
     <asp:CheckBoxList ID="cbStickers" runat="server" CssClass="FormlabelLarge"  >
    <asp:ListItem Text="Framed" Value = "Framed"/>
    <asp:ListItem Text="Heated" Value = "Heated"/>
    <asp:ListItem Text="Lined" Value = "Lined"/>
    <asp:ListItem Text="Cross Member" Value = "Cross Member"/>
    <asp:ListItem Text="Cross Memberless" Value = "Cross Memberless"/>
        </asp:CheckBoxList><br />

     </div>
     <br class="clearall" />
    

    </div></asp:Panel>
      </div>
 

        </telerik:RadPageView>
           <telerik:RadPageView ID="Upload" runat="server">
      
     <div class="formHeaderLayout">   <h2 id="title2" runat="server">Upload Your Application Photo</h2>
        <p>While not required, a photo is often the best way for us to assess your needs and make a proper recommendation. It will help to take the guesswork out of the process and can save time and money. Please upload photos to attach to your inquiry. For more information on how photos can help, go to our <a href="/industries-applications/vibco-van-visit">Virutal Van Vist</a> page. </p>
        <p><b>NOTE: Only .jpg , .jpeg , .gif , .bmp , .pdf , .png , .dwg , .tiff , .svg , .psd , .raw , .eps , .step , .iges , .igs , .dxf files are allowed for upload. </b></p>
        <hr /></div>
 <div class="formLayout" id="UploadForm"> 
   <telerik:RadUpload
                                ID="RadUpload1"
                                runat="server"
                                InitialFileInputsCount="3"
                                MaxFileInputsCount="10" 
                                
         AllowedFileExtensions=".jpg,.jpeg,.gif,.bmp,.pdf,.png,.dwg,.tiff,.svg,.psd,.raw,.eps,.step,.iges,.igs,.dxf" 
         Skin="Simple" />


      </div>
 

        </telerik:RadPageView>

         </telerik:RadMultiPage>

   <asp:Panel ID="Panel1" runat="server">
<div class="formHeaderLayout"><h2>Comments</h2>
<p>Please provide any additional comments or information that you think will help us to understand and assess your vibrator needs.</p>
<hr /></div>
<div class="formLayout" id="Comments">
    <asp:TextBox ID="tbComments" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
 </div>
    </asp:Panel>

</div>
    <asp:Button ID="Button1" CssClass="submitButton" runat="server" Text="" 
    onclick="Button1_Click" ValidationGroup="AllValidators" />
