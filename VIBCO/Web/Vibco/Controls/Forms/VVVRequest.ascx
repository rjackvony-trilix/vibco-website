﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VVVRequest.ascx.cs" Inherits="SitefinityWebApp.Controls.Forms.VVVRequest" %>
<div class="FormWrapper">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  ValidationGroup="AllValidators" CssClass="FormValidation"/>

  <asp:Panel ID="ContactInfoPanel" runat="server"  >
<div class="formHeaderLayout"><h2>Your Contact Infromation</h2>
<p>Please note that most contact information fields are required. </p>
<hr />
</div>

<div class="formLayout">
    <asp:Label ID="Label1" CssClass="FormLabel" runat="server" Text="Name: "></asp:Label><sup>*</sup><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbName" runat="server"></asp:TextBox><br />
     <div class="formdotted"></div>
    <asp:Label ID="Label2" CssClass="FormLabel" runat="server" Text="Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmail" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label3" CssClass="FormLabel" runat="server" Text="Confirm Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmailConfrim" runat="server"></asp:TextBox> <br />
     <div class="formsolid"></div>
    <asp:Label ID="Label4" CssClass="FormLabel" runat="server" Text="Company: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbCompany" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label5" CssClass="FormLabel" runat="server" Text="Industry: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbIndustry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label6" CssClass="FormLabel" runat="server" Text="Address: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbAddress" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label7" CssClass="FormLabel" runat="server" Text="Address Line 2: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbAddress2" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label8" CssClass="FormLabel" runat="server" Text="City: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCity" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label9" CssClass="FormLabel" runat="server" Text="State/Province: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbState" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label10" CssClass="FormLabel" runat="server" Text="Zip/Postal Code: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbZip" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label11" CssClass="FormLabel" runat="server" Text="Country: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCountry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label12" CssClass="FormLabel" runat="server" Text="Phone: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbPhone" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label13" CssClass="FormLabel" runat="server" Text="Fax: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbFax" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label14" CssClass="FormLabel" runat="server" Text="Mobile: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbMobile" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label15"  CssClass="FromRadio" runat="server" Text="Would you like to recieve our eNewsletter?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbNewsletter"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList>
    <br />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  Display="None" runat="server" ErrorMessage="Name is Required." ControlToValidate="tbName" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  Display="None" runat="server" ErrorMessage="Email address is Required." ControlToValidate="tbEmail" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  Display="None" runat="server" ErrorMessage="Confrim Email address is Required." ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Email Addresses do not match." ControlToCompare="tbEmail" ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:CompareValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ValidationGroup="AllValidators" Display="None"  
        ErrorMessage="Email: Not a valid Email Address" ControlToValidate="tbEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ValidationGroup="AllValidators" Display="None" 
        ErrorMessage="Confrim Email: Not a valid Email Address" 
        ControlToValidate="tbEmailConfrim" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  Display="None" runat="server" ErrorMessage="Address is Required." ControlToValidate="tbAddress" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="None" runat="server" ErrorMessage="City is Required." ControlToValidate="tbCity" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  Display="None" runat="server" ErrorMessage="State/Province is Required." ControlToValidate="tbState" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  Display="None" runat="server" ErrorMessage="Zip/Postal Code is Required." ControlToValidate="tbZip" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  Display="None" runat="server" ErrorMessage="Country is Required." ControlToValidate="tbCountry" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  Display="None" runat="server" ErrorMessage="Please select if you would like to recieve our eNewsletter." ControlToValidate="rbNewsletter" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>

    </div>   
 
    </asp:Panel>
    <br />
       <asp:Panel ID="PowerSource" runat="server">
  
     <div class="formHeaderLayout">   <h2 id="H1" runat="server">Power Source</h2>
         
        <hr /></div>
 <div class="formLayout" id="PowerSource"> 
 <h3>Pneumatic:</h3><br />
    <asp:Label ID="Label18" CssClass="FormLabel" runat="server" Text="Air PSI: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbAirPSI" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label19" CssClass="FormLabel" runat="server" Text="Air CFM: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbAirCFM" runat="server"></asp:TextBox><br />
           <div class="formsolid"></div>
 <h3>Hydraulic:</h3><br />
    <asp:Label ID="Label20" CssClass="FormLabel" runat="server" Text="Hydraulic PSI: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbHPSI" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label21" CssClass="FormLabel" runat="server" Text="Hydraulic GPM: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbHGPM" runat="server"></asp:TextBox><br />
    
           <div class="formsolid"></div>
            <h3>Electric:</h3><br />
     <asp:Label ID="Label16" CssClass="FormLabel" runat="server" Text="Current: "></asp:Label>
     <telerik:RadComboBox ID="rcbCurrent"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="AC" Value="AC" />
             <telerik:RadComboBoxItem runat="server" Text="DC" Value="DC" />
         </Items>
     </telerik:RadComboBox>     <br />
     <asp:Label ID="Label22" CssClass="FormLabel" runat="server" Text="Volts: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbVolts" runat="server"></asp:TextBox><br />
       
      <asp:Label ID="Label17" CssClass="FormLabel" runat="server" Text="Phase: "></asp:Label>
      <telerik:RadComboBox ID="rcbPhase"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Single" Value="AC" />
             <telerik:RadComboBoxItem runat="server" Text="Three" Value="DC" />
         </Items>
     </telerik:RadComboBox>      <br />
    <asp:Label ID="Label23" CssClass="FormLabel" runat="server" Text="Cycle: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInputSmall" ID="tbCycle" runat="server"></asp:TextBox><br /> 
          <div class="formsolid"></div>
           
      </div>
        </asp:Panel>
            <asp:Panel ID="Material" runat="server">
  
     <div class="formHeaderLayout">   <h2 id="H2" runat="server">Material</h2>
         
        <hr /></div>
 <div class="formLayout" id="Div1"> 
  
    <asp:Label ID="Label24" CssClass="FormLabel" runat="server" Text="Material Type: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbType" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label25" CssClass="FormLabel" runat="server" Text="Weight: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbWeight" runat="server"></asp:TextBox><br />
           
    <asp:Label ID="Label26" CssClass="FormLabel" runat="server" Text="Moisture %: "></asp:Label><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbMoisture" runat="server"></asp:TextBox><br />
    <asp:Label ID="Label27" CssClass="FormLabel" runat="server" Text="Discharge: "></asp:Label> <telerik:RadComboBox ID="rcbDischage"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Screw Conveyor" Value="Screw Conveyor" />
             <telerik:RadComboBoxItem runat="server" Text="Belt Feeder" Value="Belt Feeder" />
             <telerik:RadComboBoxItem runat="server" Text="Clam Shell" Value="Clam Shell" />
             <telerik:RadComboBoxItem runat="server" Text="Gravity" Value="Gravity" />
             <telerik:RadComboBoxItem runat="server" Text="Knife Gate" Value="Knife Gate" />
             <telerik:RadComboBoxItem runat="server" Text="Slide Gate" Value="Slide Gate" />
             <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
         </Items>
     </telerik:RadComboBox>      <br />
     <asp:Label ID="Label28" CssClass="FormLabel" runat="server" Text="Flow Problem: "></asp:Label> <telerik:RadComboBox ID="rcbFlow"  CssClass="FormDropDown"  runat="server" 
         Skin="Simple">
         <Items>
             <telerik:RadComboBoxItem runat="server" Text="---" Value="---" />
             <telerik:RadComboBoxItem runat="server" Text="Bridging" Value="Bridging" />
             <telerik:RadComboBoxItem runat="server" Text="Clogging" Value="Clogging" />
             <telerik:RadComboBoxItem runat="server" Text="Ratholing" Value="Ratholing" />
             <telerik:RadComboBoxItem runat="server" Text="Arching" Value="Arching" />
             <telerik:RadComboBoxItem runat="server" Text="Sticking" Value="Sticking" />
             <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
             

         </Items>
     </telerik:RadComboBox>      <br />
      </div>
        </asp:Panel>
          <div class="formHeaderLayout">   <h2 id="title2" runat="server">Photo Upload</h2>
        <p>While not required, a photo is often the best way for us to assess your needs and make a proper recommendation. It will help to take the guesswork out of the process and can save time and money. Please upload photos to attach to your inquiry. For more information on how photos can help, go to our <a href="/industries-applications/vibco-van-visit">Virutal Van Vist</a> page. </p>
        <p><b>NOTE: Only .jpg , .jpeg , .gif , .bmp , .pdf , .png , .dwg , .tiff , .svg , .psd , .raw , .eps , .step , .iges , .igs , .dxf files are allowed for upload. </b></p>
        <hr /></div>
 <div class="formLayout" id="UploadForm"> 
   <telerik:RadUpload
                                ID="RadUpload1"
                                runat="server"
                                InitialFileInputsCount="3"
                                MaxFileInputsCount="10" 
                                
         AllowedFileExtensions=".jpg,.jpeg,.gif,.bmp,.pdf,.png,.dwg,.tiff,.svg,.psd,.raw,.eps,.step,.iges,.igs,.dxf" 
         Skin="Simple" />


      </div>

       <asp:Panel ID="Panel1" runat="server">
<div class="formHeaderLayout"><h2>Comments</h2>
<p>Please provide any comments and/or suggestions that will help us to assess your needs and provide you with the best possible solution. </p>
<hr /></div>
<div class="formLayout" id="Comments">
    <asp:TextBox ID="tbComments" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
 </div>
    </asp:Panel>

</div>
    <asp:Button ID="Button1" CssClass="submitButton" runat="server" Text="" 
    onclick="Button1_Click" ValidationGroup="AllValidators" />

