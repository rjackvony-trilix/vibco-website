﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LiteratureRequest.ascx.cs" Inherits="SitefinityWebApp.Controls.Forms.LiteratureRequest" %>


<div class="FormWrapper">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  ValidationGroup="AllValidators" CssClass="FormValidation"/>

  <asp:Panel ID="ContactInfoPanel" runat="server"  >
<div class="formHeaderLayout"><h2>Your Contact Infromation</h2>
<p>Please note that most contact information fields are required. </p>
<hr />
</div>

<div class="formLayout">
    <asp:Label ID="Label1" CssClass="FormLabel" runat="server" Text="Name: "></asp:Label><sup>*</sup><asp:TextBox CausesValidation="true" CssClass="FormInput" ID="tbName" runat="server"></asp:TextBox><br />
     <div class="formdotted"></div>
    <asp:Label ID="Label2" CssClass="FormLabel" runat="server" Text="Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmail" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label3" CssClass="FormLabel" runat="server" Text="Confirm Email: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbEmailConfrim" runat="server"></asp:TextBox> <br />
     <div class="formsolid"></div>
    <asp:Label ID="Label4" CssClass="FormLabel" runat="server" Text="Company: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbCompany" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label5" CssClass="FormLabel" runat="server" Text="Industry: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbIndustry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label6" CssClass="FormLabel" runat="server" Text="Address: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbAddress" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label7" CssClass="FormLabel" runat="server" Text="Address Line 2: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbAddress2" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label8" CssClass="FormLabel" runat="server" Text="City: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCity" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label9" CssClass="FormLabel" runat="server" Text="State/Province: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbState" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label10" CssClass="FormLabel" runat="server" Text="Zip/Postal Code: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbZip" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label11" CssClass="FormLabel" runat="server" Text="Country: "></asp:Label><sup>*</sup><asp:TextBox CssClass="FormInput" ID="tbCountry" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label12" CssClass="FormLabel" runat="server" Text="Phone: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbPhone" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label13" CssClass="FormLabel" runat="server" Text="Fax: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbFax" runat="server"></asp:TextBox> <br />
    <asp:Label ID="Label14" CssClass="FormLabel" runat="server" Text="Mobile: "></asp:Label><asp:TextBox CssClass="FormInput" ID="tbMobile" runat="server"></asp:TextBox> <br />
      <div class="formsolid"></div>
    <asp:Label ID="Label15"  CssClass="FromRadio" runat="server" Text="Would you like to recieve our eNewsletter?: "></asp:Label><sup>*</sup><br />
    <asp:RadioButtonList ID="rbNewsletter"   RepeatDirection="Horizontal"  RepeatLayout="Flow" runat="server" CssClass="FormLabel">
    <asp:ListItem Value="Yes" Text="Yes&nbsp;&nbsp; " ></asp:ListItem>
    <asp:ListItem Value="No" Text="No"  ></asp:ListItem>
    </asp:RadioButtonList>
    <br />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  Display="None" runat="server" ErrorMessage="Name is Required." ControlToValidate="tbName" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  Display="None" runat="server" ErrorMessage="Email address is Required." ControlToValidate="tbEmail" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  Display="None" runat="server" ErrorMessage="Confrim Email address is Required." ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Email Addresses do not match." ControlToCompare="tbEmail" ControlToValidate="tbEmailConfrim" ValidationGroup="AllValidators"></asp:CompareValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ValidationGroup="AllValidators" Display="None"  
        ErrorMessage="Email: Not a valid Email Address" ControlToValidate="tbEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ValidationGroup="AllValidators" Display="None" 
        ErrorMessage="Confrim Email: Not a valid Email Address" 
        ControlToValidate="tbEmailConfrim" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  Display="None" runat="server" ErrorMessage="Address is Required." ControlToValidate="tbAddress" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="None" runat="server" ErrorMessage="City is Required." ControlToValidate="tbCity" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  Display="None" runat="server" ErrorMessage="State/Province is Required." ControlToValidate="tbState" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  Display="None" runat="server" ErrorMessage="Zip/Postal Code is Required." ControlToValidate="tbZip" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  Display="None" runat="server" ErrorMessage="Country is Required." ControlToValidate="tbCountry" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  Display="None" runat="server" ErrorMessage="Please select if you would like to recieve our eNewsletter." ControlToValidate="rbNewsletter" ValidationGroup="AllValidators"></asp:RequiredFieldValidator>

    </div>   
 
    </asp:Panel>
    <br />
   <div class="formHeaderLayout"> <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="formTabStrip" EnableEmbeddedSkins="false" MultiPageID="mpLit"
                SelectedIndex="0" Width="100%" >
                  <Tabs>
                    <telerik:RadTab Text="Industrial<br>Products Literature" PageViewID="IndustrialLit">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Construction<br>Products Literature" PageViewID="ConstructionLit">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Full Detail<br>Installation Manuals" PageViewID="FullDetailLit">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Quick Reference<br>Guides" PageViewID="QuickRef">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Sales Kits<br>&nbsp;" PageViewID="SalesKits">
                    </telerik:RadTab>
                 </Tabs>
    </telerik:RadTabStrip></div>
    <telerik:RadMultiPage ID="mpLit" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="IndustrialLit" runat="server">
      
     <div class="formHeaderLayout">   <h2 id="title0" runat="server">Industrial Products Literature</h2>
        <p>Please enter the quantity desired for any VIBCO literature item you would like to receive.</p>
        <hr /></div>
 <div class="formLayout" id="IndustrialProducts"> 
    <telerik:RadNumericTextBox MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox1"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" Label="Air Cannons: " LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox2"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" Label="Ball Vibrators: " LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="EZ Source Quick Reference Guide: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox3"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="General Catalog: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox4"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Junior Heavy Duty Electric Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox5"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
       <telerik:RadNumericTextBox Label="Piston Vibrators - Model 55: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox6"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Piston Vibrators - Model 70: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox7"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Plating Tank Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox8"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Railroad Car Shakers: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox9"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="SCR Adjustable Speed and Force Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox10"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="SVR High Frequency Rotary Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox11"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Silent Turbine Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox12"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="VIBRA-Beam: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox13"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="Vibrating Tables: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox14"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
 </div>
 

        </telerik:RadPageView>

        <telerik:RadPageView ID="ConstructionLit" runat="server">
      <div class="formHeaderLayout">
        <h2 id="title1" runat="server">Construction Products Literature</h2>
        <p>Please enter the quantity desired for any VIBCO literature item you would like to receive.</p>
        <hr /></div>
 <div class="formLayout" id="Div1"> 
    <telerik:RadNumericTextBox Label="Air Cannons: "  MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox15"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Big Bertha Dumpbody Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox16"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server"  LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Bulldog Concrete Pump Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox17"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="Burial Vault Rotary Piston Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox18"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="CCL High Frequency Turbine Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox19"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
       <telerik:RadNumericTextBox Label="DC Vibrators - 12 and 24 Volt: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox20"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="EZ Source Quick Reference Guide: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox21"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="External Concrete Vibrators Handbook: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox22"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="GR-3200 Walk Behind Vibratory Rollers: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox23"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="General Catalog: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox24"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Internal Concrete Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox25"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Junior Heavy Duty Electric Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox26"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Piston Vibrators - Model 55: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox27"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="Piston Vibrators - Model 70: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox28"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Plate Compactors & Rollers: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox29"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Pothole Packers & Asphalt Maintenance: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox30"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="ReadyMix Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox31"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="SVR High Frequency Rotary Vibrator: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox32"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Sand & Gravel: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox33"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="TP-10 Plate Compactor: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox34"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="VR-18 & VR-12 Reversible Plate Compactors: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox35"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
        <telerik:RadNumericTextBox Label="Vibrators for Concrete Walls and Columns: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox36"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />

 </div>
 
        </telerik:RadPageView>

        <telerik:RadPageView ID="FullDetailLit" runat="server">
        
      <div class="formHeaderLayout">  <h2 id="title2" runat="server">Full Detail Installation Manuals</h2>
        <p>Please enter the quantity desired for any VIBCO literature item you would like to receive.</p>
        <hr /></div>
 <div class="formLayout" id="Div2"> 
    <telerik:RadNumericTextBox Label="12 & 24 Volt Electric Vibrators: "  MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox37"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Ball Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox38"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server"  LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Hydraulic Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox39"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="Internal Concrete Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox40"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Large Electric Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox41"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
       <telerik:RadNumericTextBox Label="Piston Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox42"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Plate Compactors and Rollers: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox43"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Silent Turbine Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox44"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Small Impact Electric Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox45"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       </div>
 
        </telerik:RadPageView>

        <telerik:RadPageView ID="QuickRef" runat="server">
         
      <div class="formHeaderLayout">  <h2 id="title3" runat="server">Quick Reference Guides</h2>
        <p>Please enter the quantity desired for any VIBCO literature item you would like to receive.</p>
        <hr /></div>
 <div class="formLayout" id="Div3"> 
      <telerik:RadNumericTextBox Label="Ball Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox47"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server"  LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Big Bertha Dumpbody Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox48"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="DC Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox49"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Explosion Proof Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox50"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
       <telerik:RadNumericTextBox Label="FC Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox51"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Heavy Duty Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox52"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
     <telerik:RadNumericTextBox Label="Hydraulic Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox53"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Piston Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox54"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       <telerik:RadNumericTextBox Label="SCR Adjustable Speed and Force Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox46"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       <telerik:RadNumericTextBox Label="SFC: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox55"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       <telerik:RadNumericTextBox Label="Silent Turbine Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox56"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       <telerik:RadNumericTextBox Label="Small Impact Electric Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox57"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       <telerik:RadNumericTextBox Label="US Vibrators: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox58"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br /> 
       </div>
  
        </telerik:RadPageView>

        <telerik:RadPageView ID="SalesKits" runat="server">
      
        <div class="formHeaderLayout"><h2 id="title4" runat="server">Sales Kits</h2>
        <p>Please enter the quantity desired for any VIBCO literature item you would like to receive.</p>
        <hr /></div>
 <div class="formLayout" id="Div4"> 
      <telerik:RadNumericTextBox Label="Big Bertha Counter Mat Kit: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox59"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server"  LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
      <telerik:RadNumericTextBox Label="Big Bertha Demonstration Stand Kit: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox60"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
    <telerik:RadNumericTextBox Label="High Performance Sales Kit: " MinValue="0"  ShowSpinButtons="true" ID="RadNumericTextBox61"  SpinDownCssClass="Spinnerdown" SpinUpCssClass="Spinnerup" EnableEmbeddedBaseStylesheet="false"  runat="server" LabelCssClass="FormlabelLarge" EnableEmbbeddedSkins="false"  CssClass="FormInputSmall" Width="320px" >
      <HoveredStyle  BorderColor="#aca9a1" /><EnabledStyle BorderColor="#aca9a1" /><FocusedStyle  BorderColor="#f35c27" /><NumberFormat DecimalDigits="0" />
      </telerik:RadNumericTextBox><br />
        </div>
 
        </telerik:RadPageView>
    </telerik:RadMultiPage>

   <asp:Panel ID="Panel1" runat="server">
<div class="formHeaderLayout"><h2>Comments</h2>
<p>Please provide comments and/or suggestions about VIBCO Literature. Let us know if there is an industrial or construction application that you don't see listed. </p>
<hr /></div>
<div class="formLayout" id="Comments">
    <asp:TextBox ID="tbComments" runat="server" TextMode="MultiLine" CssClass="FormInput" Width="400px" Height="100px"></asp:TextBox><br />
 </div>
    </asp:Panel>

</div>
    <asp:Button ID="Button1" CssClass="submitButton" runat="server" Text="" 
    onclick="Button1_Click" ValidationGroup="AllValidators" />