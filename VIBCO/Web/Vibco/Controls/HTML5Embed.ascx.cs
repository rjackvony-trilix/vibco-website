﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SitefinityWebApp.Controls
{
    public partial class HTML5Embed : System.Web.UI.UserControl
    {
        public string XMLFileName
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public string GetXMLFile()
        {
            if (XMLFileName.Contains(".xml"))
                return "/html5/xml/" + XMLFileName;
            else
                return "/html5/xml/"+XMLFileName+ ".xml";

        }

    }
}