﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Libraries;
using Products;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
namespace SitefinityWebApp.Controls
{
    public partial class onthejob : System.Web.UI.UserControl
    {
        public string FlashFileName
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string GetFlashFile()
        {
            ProductManager productManager = new ProductManager();
            Product product = null;
            String str = "";
            if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                product = productManager.GetProduct(Guid.Parse(Request.QueryString["productid"]));
            else if (!string.IsNullOrEmpty(Request.QueryString["productkey"]))
                product = productManager.GetProductByKey(Request.QueryString["productkey"]);
            else
                return "<p>We are sorry but there are no virtual reality files associated with this product.</p>";

            ProductCategory pc = product.Categories[0];
            productManager.GetCategory(pc.ParentId);

            TaxonomyManager taxManager = TaxonomyManager.GetManager();
            
            //Get the tag called "One"
            while (str == "")
            {
                try
                {
                    var taxon = taxManager.GetTaxa<FlatTaxon>().Where(t => t.Name == pc.Name).Single();
                    string itemTypeName = "Telerik.Sitefinity.Libraries.Model.Document";
                    Type itemType = TypeResolutionService.ResolveType(itemTypeName);
                    var manager = ManagerBase.GetMappedManager(itemType, "");
                    ContentDataProviderBase contentProvider = manager.Provider as ContentDataProviderBase;

                    var v = GetItems(taxon, contentProvider, itemType);
                    foreach (Document n in v)
                    {
                        if (n.Library.Title == "Flash Files")
                        {
                            str += "<li><a href=\"/products/virtual-reality?name=" + n.Title + "\">" + pc.Name + "</a></li><br>";
                        }
                    } if (str == "")
                    {
                        pc = productManager.GetCategory(pc.ParentId);
                        if (pc == null)
                        {
                            str = "<p>We are sorry but there are no virtual reality files associated with this product.</p>";
                            break;
                        }
                        continue;
                    }
                }
                catch
                {
                    pc = productManager.GetCategory(pc.ParentId);
                    if (pc == null)
                    {
                        str = "<p>We are sorry but there are no virtual reality files associated with this product.</p>";
                        break;
                    }
                    continue;
                }
            }
            //productManager.GetCategory(pc.ParentId).Name
            str = "<ul>" + str + "</ul>";
            return str;
  
        }
        private IEnumerable GetItems(ITaxon taxon, ContentDataProviderBase contentProvider, Type itemType)
        {
            TaxonomyPropertyDescriptor prop = GetPropertyDescriptor(itemType, taxon);
            int? totalCount = 0;
            var items = contentProvider.GetItemsByTaxon(taxon.Id, prop.MetaField.IsSingleTaxon, prop.Name, itemType, "Status = Master", string.Empty, 0, 100, ref totalCount);
            return items;
        }
        private TaxonomyPropertyDescriptor GetPropertyDescriptor(Type itemType, ITaxon taxon)
        {
            return TaxonomyManager.GetPropertyDescriptor(itemType, taxon);
        }
        private void GetRecursiveCategories(ref string strReturn, ProductCategory pc)
        {
            if (pc == null)
                return;
            ProductManager pm = new ProductManager();
            strReturn = pc.Name.Replace("/", "-").Replace(" & ", "-").Replace(" ", "-") + "/" + strReturn;  //alwasy returns same as first one
            GetRecursiveCategories(ref strReturn, pm.GetCategory(pc.ParentId));


        }
    }
}