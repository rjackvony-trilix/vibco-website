﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Libraries;
using Products;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
namespace SitefinityWebApp.Controls
{
    public partial class virtualreality : System.Web.UI.UserControl
    {
        public string FlashFileName
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string GetFlashFile()
        {
            try
            {
                var z = Request.QueryString["name"];

                var a = App.WorkWith().Documents().Get().ToList().Where(i => i.Title == z && i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live);

                return a.SingleOrDefault().MediaUrl;
            }
            catch
            {
                return "";
            } 
        }
    }
}