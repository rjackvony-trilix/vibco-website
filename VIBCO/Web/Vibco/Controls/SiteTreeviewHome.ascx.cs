﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Sitefinity.Web;

namespace SitefinityWebApp.Controls
{
    public partial class SiteTreeviewHome : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ((SiteMapDataSource)RadTreeview1.DataSource).StartingNodeOffset = 0;
            ((SiteMapDataSource)RadTreeview1.DataSource).StartFromCurrentNode = false;

            RadTreeview1.DataBind();

            foreach (RadTreeNode node in RadTreeview1.GetAllNodes())
            {
                
                    node.Expanded = true;

            }

        }
        public void RadTreeview1_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            PageSiteNode pageSiteNode = (PageSiteNode)e.Node.DataItem;
            if (pageSiteNode.ShowInNavigation == false)
            {
                String url2find = pageSiteNode.Url;
                url2find = url2find.Remove(0, 1);

                if (RadTreeview1.FindNodeByUrl(url2find) != null)
                {
                    try
                    {
                        RadTreeview1.FindNodeByUrl(url2find).Remove();

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

        }
    }
}