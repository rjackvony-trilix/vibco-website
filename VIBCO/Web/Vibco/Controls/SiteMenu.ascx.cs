﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Web;
using Telerik.Web.UI;
/*using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Services.Search.Configuration;
using Telerik.Sitefinity.Services.Search.Data;
using SitefinityWebApp.Widgets;*/

namespace SitefinityWebApp.Controls
{
    public partial class SiteMenu : System.Web.UI.UserControl
    {
        public string  MenuPadding
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = MenuPadding;

            /*var section = Config.Get<SearchConfig>();
            foreach (var provider in section.Providers.Values)
            {
                if (provider.Name == "LuceneSearchProvider")
                {
                    if (provider.ProviderType == typeof(LuceneSearchProvider))
                    {
                        provider.ProviderType = typeof(VIBCOSearchProvider);
                        ConfigManager.GetManager().SaveSection(section);
                        break;
                    }
                }
            }*/
           
        }

     

    

        public void RadMenu1_ItemDataBound(object sender, RadMenuEventArgs e)
        {
            if (this.Page.Items["IsInIndexMode"] == null)
            {
                SiteMapNode node = e.Item.DataItem as SiteMapNode;
             
                 
                if (node != null)
                {
                    e.Item.Attributes.Add("PageDescrip", node.Description);
               
                }

                PageSiteNode pageSiteNode = (PageSiteNode)e.Item.DataItem;

                SiteMapNode currentNode = SiteMapBase.GetCurrentProvider().CurrentNode;
            
           
               while (currentNode != null && currentNode.ParentNode != null)
                {
                    if (currentNode.ParentNode.Title != currentNode.RootNode.Title)
                        currentNode = currentNode.ParentNode;
                    else
                        break;
                }
            
             
            
                if (currentNode != null)
                {
                    RadMenuItem item = this.RadMenu1.FindItemByUrl(this.ResolveUrl(currentNode.Url));
                    if (item != null)
                    {
                        item.Selected = true;
                        item.SelectedCssClass = "rmFocused";

                    }
                }
                if (pageSiteNode.ShowInNavigation == false)
                {
                    if(RadMenu1.FindItemByText(pageSiteNode.Title) != null)
                        RadMenu1.Items.Remove(RadMenu1.FindItemByText(pageSiteNode.Title));
                }

                if (pageSiteNode.NodeType == Telerik.Sitefinity.Pages.Model.NodeType.InnerRedirect)
                {
                    if (RadMenu1.FindItemByText(pageSiteNode.Title) != null)
                        RadMenu1.Items.Remove(RadMenu1.FindItemByText(pageSiteNode.Title));
                }
            }

        }
    }
}