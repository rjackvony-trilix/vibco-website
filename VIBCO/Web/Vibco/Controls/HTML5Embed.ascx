﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HTML5Embed.ascx.cs" Inherits="SitefinityWebApp.Controls.HTML5Embed" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />

		<script type="text/javascript">
		    var BrowserDetect = {
		        init: function () {
		            this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		            this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		            this.OS = this.searchString(this.dataOS) || "an unknown OS";
		        },
		        searchString: function (data) {
		            for (var i = 0; i < data.length; i++) {
		                var dataString = data[i].string;
		                var dataProp = data[i].prop;
		                this.versionSearchString = data[i].versionSearch || data[i].identity;
		                if (dataString) {
		                    if (dataString.indexOf(data[i].subString) != -1)
		                        return data[i].identity;
		                }
		                else if (dataProp)
		                    return data[i].identity;
		            }
		        },
		        searchVersion: function (dataString) {
		            var index = dataString.indexOf(this.versionSearchString);
		            if (index == -1) return;
		            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
		        },
		        dataBrowser: [
		{
		    string: navigator.userAgent,
		    subString: "Chrome",
		    identity: "Chrome"
		},
		{ string: navigator.userAgent,
		    subString: "OmniWeb",
		    versionSearch: "OmniWeb/",
		    identity: "OmniWeb"
		},
		{
		    string: navigator.vendor,
		    subString: "Apple",
		    identity: "Safari",
		    versionSearch: "Version"
		},
		{
		    prop: window.opera,
		    identity: "Opera"
		},
		{
		    string: navigator.vendor,
		    subString: "iCab",
		    identity: "iCab"
		},
		{
		    string: navigator.vendor,
		    subString: "KDE",
		    identity: "Konqueror"
		},
		{
		    string: navigator.userAgent,
		    subString: "Firefox",
		    identity: "Firefox"
		},
		{
		    string: navigator.vendor,
		    subString: "Camino",
		    identity: "Camino"
		},
		{		// for newer Netscapes (6+)
		    string: navigator.userAgent,
		    subString: "Netscape",
		    identity: "Netscape"
		},
		{
		    string: navigator.userAgent,
		    subString: "MSIE",
		    identity: "Explorer",
		    versionSearch: "MSIE"
		},
		{
		    string: navigator.userAgent,
		    subString: "Gecko",
		    identity: "Mozilla",
		    versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
		    string: navigator.userAgent,
		    subString: "Mozilla",
		    identity: "Netscape",
		    versionSearch: "Mozilla"
		}
	],
		        dataOS: [
		{
		    string: navigator.platform,
		    subString: "Win",
		    identity: "Windows"
		},
		{
		    string: navigator.platform,
		    subString: "Mac",
		    identity: "Mac"
		},
		{
		    string: navigator.userAgent,
		    subString: "iPhone",
		    identity: "iPhone/iPod"
		},
		{
		    string: navigator.platform,
		    subString: "Linux",
		    identity: "Linux"
		}
	]

		    };
		    BrowserDetect.init();

		    function updateOrientation() {
		        switch (window.orientation) {
		            case 90:
		            case -90:
		                if (window.pageYOffset == 0) {
		                    window.scrollTo(0, 1);
		                }
		                break;
		            default:
		                if (window.pageYOffset == 0) {
		                    window.scrollTo(0, 1);
		                }
		                break;
		        }
		    }

		    function pano2vrSkin(player, skinlayer) {
		        var me = this;
		        var flag = false;
		        this.player = player;
		        this.player.skinObj = this;
		        this.divSkin = (skinlayer) ? skinlayer : player.divSkin;
		        this.elementMouseDown = new Array();
		        this.elementMouseOver = new Array();
		        this.updateSize = function (startElement) {
		            var stack = new Array();
		            stack.push(startElement);
		            while (stack.length > 0) {
		                e = stack.pop();
		                if (e.ggUpdatePosition) {
		                    e.ggUpdatePosition();
		                }
		                if (e.hasChildNodes()) {
		                    for (i = 0; i < e.childNodes.length; i++) {
		                        stack.push(e.childNodes[i]);
		                    }
		                }
		            }
		        }

		        this.findElements = function (id) {
		            var r = new Array();
		            var stack = new Array();
		            stack.push(me.divSkin);
		            while (stack.length > 0) {
		                e = stack.pop();
		                if (e.ggId == id) {
		                    r.push(e);
		                }
		                if (e.hasChildNodes()) {
		                    for (i = 0; i < e.childNodes.length; i++) {
		                        stack.push(e.childNodes[i]);
		                    }
		                }
		            }
		            return r;
		        }

		        this.addSkin = function () {
		            this.controller = document.createElement('div');
		            this.controller.ggId = 'controller'
		            this.controller.ggUpdatePosition = function () {
		                this.style.webkitTransition = 'none';
		                w = this.parentNode.offsetWidth;
		                this.style.left = (-142 + w / 2) + 'px';
		                h = this.parentNode.offsetHeight;
		                this.style.top = (-65 + h) + 'px';
		            }
		            hs = 'position:absolute;';
		            hs += 'left: -142px;';
		            hs += 'top:  -65px;';
		            hs += 'width: 286px;';
		            hs += 'height: 50px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            this.controller.setAttribute('style', hs);
		            this.up = document.createElement('div');
		            this.up.ggId = 'up'
		            hs = 'position:absolute;';
		            hs += 'left: 25px;';
		            hs += 'top:  -5px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.up.setAttribute('style', hs);
		            this.up__img = document.createElement('img');
		            this.up__img.setAttribute('src', '/html5/images/Controls/up.svg');
		            this.up__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.up.appendChild(this.up__img);
		            this.up.onmouseover = function () {
		                me.up__img.src = '/html5/images/Controls/upo.svg';
		            }
		            this.up.onmouseout = function () {
		                me.up__img.src = '/html5/images/Controls/up.svg';
		                me.elementMouseDown['up'] = false;
		            }
		            this.up.onmousedown = function () {
		                me.elementMouseDown['up'] = true;
		            }
		            this.up.onmouseup = function () {
		                me.elementMouseDown['up'] = false;
		            }
		            this.controller.appendChild(this.up);
		            this.down = document.createElement('div');
		            this.down.ggId = 'down'
		            hs = 'position:absolute;';
		            hs += 'left: 25px;';
		            hs += 'top:  25px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.down.setAttribute('style', hs);
		            this.down__img = document.createElement('img');
		            this.down__img.setAttribute('src', '/html5/images/Controls/down.svg');
		            this.down__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.down.appendChild(this.down__img);
		            this.down.onmouseover = function () {
		                me.down__img.src = '/html5/images/Controls/downo.svg';
		            }
		            this.down.onmouseout = function () {
		                me.down__img.src = '/html5/images/Controls/down.svg';
		                me.elementMouseDown['down'] = false;
		            }
		            this.down.onmousedown = function () {
		                me.elementMouseDown['down'] = true;
		            }
		            this.down.onmouseup = function () {
		                me.elementMouseDown['down'] = false;
		            }
		            this.controller.appendChild(this.down);
		            this.left = document.createElement('div');
		            this.left.ggId = 'left'
		            hs = 'position:absolute;';
		            hs += 'left: 0px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.left.setAttribute('style', hs);
		            this.left__img = document.createElement('img');
		            this.left__img.setAttribute('src', '/html5/images/Controls/left.svg');
		            this.left__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.left.appendChild(this.left__img);
		            this.left.onmouseover = function () {
		                me.left__img.src = '/html5/images/Controls/lefto.svg';
		            }
		            this.left.onmouseout = function () {
		                me.left__img.src = '/html5/images/Controls/left.svg';
		                me.elementMouseDown['left'] = false;
		            }
		            this.left.onmousedown = function () {
		                me.elementMouseDown['left'] = true;
		            }
		            this.left.onmouseup = function () {
		                me.elementMouseDown['left'] = false;
		            }
		            this.controller.appendChild(this.left);
		            this.right = document.createElement('div');
		            this.right.ggId = 'right'
		            hs = 'position:absolute;';
		            hs += 'left: 50px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.right.setAttribute('style', hs);
		            this.right__img = document.createElement('img');
		            this.right__img.setAttribute('src', '/html5/images/Controls/right.svg');
		            this.right__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.right.appendChild(this.right__img);
		            this.right.onmouseover = function () {
		                me.right__img.src = '/html5/images/Controls/righto.svg';
		            }
		            this.right.onmouseout = function () {
		                me.right__img.src = '/html5/images/Controls/right.svg';
		                me.elementMouseDown['right'] = false;
		            }
		            this.right.onmousedown = function () {
		                me.elementMouseDown['right'] = true;
		            }
		            this.right.onmouseup = function () {
		                me.elementMouseDown['right'] = false;
		            }
		            this.controller.appendChild(this.right);
		            this.zoomin = document.createElement('div');
		            this.zoomin.ggId = 'zoomin'
		            hs = 'position:absolute;';
		            hs += 'left: 90px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.zoomin.setAttribute('style', hs);
		            this.zoomin__img = document.createElement('img');
		            this.zoomin__img.setAttribute('src', '/html5/images/Controls/zoomin.svg');
		            this.zoomin__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.zoomin.appendChild(this.zoomin__img);
		            this.zoomin.onmouseover = function () {
		                me.tt_zoomin.style.webkitTransition = 'none';
		                me.tt_zoomin.style.visibility = 'inherit';
		                me.zoomin__img.src = '/html5/images/Controls/zoomino.svg';
		            }
		            this.zoomin.onmouseout = function () {
		                me.tt_zoomin.style.webkitTransition = 'none';
		                me.tt_zoomin.style.visibility = 'hidden';
		                me.zoomin__img.src = '/html5/images/Controls/zoomin.svg';
		                me.elementMouseDown['zoomin'] = false;
		            }
		            this.zoomin.onmousedown = function () {
		                me.elementMouseDown['zoomin'] = true;
		            }
		            this.zoomin.onmouseup = function () {
		                me.elementMouseDown['zoomin'] = false;
		            }
		            this.tt_zoomin = document.createElement('div');
		            this.tt_zoomin.ggId = 'tt_zoomin'
		            hs = 'position:absolute;';
		            hs += 'left: -56px;';
		            hs += 'top:  35px;';
		            hs += 'width: 148px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_zoomin.setAttribute('style', hs);
		            this.tt_zoomin.innerHTML = "Zoom In";
		            this.zoomin.appendChild(this.tt_zoomin);
		            this.controller.appendChild(this.zoomin);
		            this.zoomout = document.createElement('div');
		            this.zoomout.ggId = 'zoomout'
		            hs = 'position:absolute;';
		            hs += 'left: 120px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.zoomout.setAttribute('style', hs);
		            this.zoomout__img = document.createElement('img');
		            this.zoomout__img.setAttribute('src', '/html5/images/Controls/zoomout.svg');
		            this.zoomout__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.zoomout.appendChild(this.zoomout__img);
		            this.zoomout.onmouseover = function () {
		                me.tt_zoomout.style.webkitTransition = 'none';
		                me.tt_zoomout.style.visibility = 'inherit';
		                me.zoomout__img.src = '/html5/images/Controls/zoomouto.svg';
		            }
		            this.zoomout.onmouseout = function () {
		                me.tt_zoomout.style.webkitTransition = 'none';
		                me.tt_zoomout.style.visibility = 'hidden';
		                me.zoomout__img.src = '/html5/images/Controls/zoomout.svg';
		                me.elementMouseDown['zoomout'] = false;
		            }
		            this.zoomout.onmousedown = function () {
		                me.elementMouseDown['zoomout'] = true;
		            }
		            this.zoomout.onmouseup = function () {
		                me.elementMouseDown['zoomout'] = false;
		            }
		            this.tt_zoomout = document.createElement('div');
		            this.tt_zoomout.ggId = 'tt_zoomout'
		            hs = 'position:absolute;';
		            hs += 'left: -56px;';
		            hs += 'top:  35px;';
		            hs += 'width: 148px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_zoomout.setAttribute('style', hs);
		            this.tt_zoomout.innerHTML = "Zoom Out";
		            this.zoomout.appendChild(this.tt_zoomout);
		            this.controller.appendChild(this.zoomout);
		            this.autorotate = document.createElement('div');
		            this.autorotate.ggId = 'autorotate'
		            hs = 'position:absolute;';
		            hs += 'left: 160px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.autorotate.setAttribute('style', hs);
		            this.autorotate__img = document.createElement('img');
		            this.autorotate__img.setAttribute('src', '/html5/images/Controls/autorotate.svg');
		            this.autorotate__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.autorotate.appendChild(this.autorotate__img);
		            this.autorotate.onclick = function () {
		                me.player.toggleAutorotate();
		            }
		            this.autorotate.onmouseover = function () {
		                me.tt_autorotate.style.webkitTransition = 'none';
		                me.tt_autorotate.style.visibility = 'inherit';
		                me.autorotate__img.src = '/html5/images/Controls/autorotateo.svg';
		            }
		            this.autorotate.onmouseout = function () {
		                me.tt_autorotate.style.webkitTransition = 'none';
		                me.tt_autorotate.style.visibility = 'hidden';
		                me.autorotate__img.src = '/html5/images/Controls/autorotate.svg';
		            }
		            this.tt_autorotate = document.createElement('div');
		            this.tt_autorotate.ggId = 'tt_autorotate'
		            hs = 'position:absolute;';
		            hs += 'left: -66px;';
		            hs += 'top:  35px;';
		            hs += 'width: 168px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_autorotate.setAttribute('style', hs);
		            this.tt_autorotate.innerHTML = "Start\/Stop Autorotation";
		            this.autorotate.appendChild(this.tt_autorotate);
		            this.controller.appendChild(this.autorotate);
		            this.info = document.createElement('div');
		            this.info.ggId = 'info'
		            hs = 'position:absolute;';
		            hs += 'left: 190px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            this.info.setAttribute('style', hs);
		            this.info__img = document.createElement('img');
		            this.info__img.setAttribute('src', '/html5/images/Controls/info.svg');
		            this.info__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.info.appendChild(this.info__img);
		            this.info.onclick = function () {
		                flag = (me.userdata.style.visibility == 'hidden');
		                me.userdata.style.webkitTransition = 'none';
		                me.userdata.style.visibility = flag ? 'inherit' : 'hidden';
		            }
		            this.info.onmouseover = function () {
		                me.tt_info.style.webkitTransition = 'none';
		                me.tt_info.style.visibility = 'inherit';
		                me.info__img.src = '/html5/images/Controls/infoo.svg';
		            }
		            this.info.onmouseout = function () {
		                me.tt_info.style.webkitTransition = 'none';
		                me.tt_info.style.visibility = 'hidden';
		                me.info__img.src = '/html5/images/Controls/info.svg';
		            }
		            this.tt_info = document.createElement('div');
		            this.tt_info.ggId = 'tt_info'
		            hs = 'position:absolute;';
		            hs += 'left: -56px;';
		            hs += 'top:  35px;';
		            hs += 'width: 148px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_info.setAttribute('style', hs);
		            this.tt_info.innerHTML = "Show Information";
		            this.info.appendChild(this.tt_info);
		            this.controller.appendChild(this.info);
		            this.movemode = document.createElement('div');
		            this.movemode.ggId = 'movemode'
		            hs = 'position:absolute;';
		            hs += 'left: 220px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.movemode.setAttribute('style', hs);
		            this.movemode__img = document.createElement('img');
		            this.movemode__img.setAttribute('src', '/html5/images/Controls/movemode.svg');
		            this.movemode__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.movemode.appendChild(this.movemode__img);
		            this.movemode.onclick = function () {
		                me.player.changeViewMode(2);
		            }
		            this.movemode.onmouseover = function () {
		                me.tt_movemode.style.webkitTransition = 'none';
		                me.tt_movemode.style.visibility = 'inherit';
		                me.movemode__img.src = '/html5/images/Controls/movemodeo.svg';
		            }
		            this.movemode.onmouseout = function () {
		                me.tt_movemode.style.webkitTransition = 'none';
		                me.tt_movemode.style.visibility = 'hidden';
		                me.movemode__img.src = '/html5/images/Controls/movemode.svg';
		            }
		            this.tt_movemode = document.createElement('div');
		            this.tt_movemode.ggId = 'tt_movemode'
		            hs = 'position:absolute;';
		            hs += 'left: -66px;';
		            hs += 'top:  35px;';
		            hs += 'width: 168px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_movemode.setAttribute('style', hs);
		            this.tt_movemode.innerHTML = "Change Control Mode";
		            this.movemode.appendChild(this.tt_movemode);
		            this.controller.appendChild(this.movemode);
		            this.fullscreen = document.createElement('div');
		            this.fullscreen.ggId = 'fullscreen'
		            hs = 'position:absolute;';
		            hs += 'left: 250px;';
		            hs += 'top:  10px;';
		            hs += 'width: 32px;';
		            hs += 'height: 32px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'cursor: pointer;';
		            this.fullscreen.setAttribute('style', hs);
		            this.fullscreen__img = document.createElement('img');
		            this.fullscreen__img.setAttribute('src', '/html5/images/Controls/fullscreen.svg');
		            this.fullscreen__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		            this.fullscreen.appendChild(this.fullscreen__img);
		            this.fullscreen.onclick = function () {
		                me.player.toggleFullscreen();
		            }
		            this.fullscreen.onmouseover = function () {
		                me.tt_fullscreen.style.webkitTransition = 'none';
		                me.tt_fullscreen.style.visibility = 'inherit';
		                me.fullscreen__img.src = '/html5/images/Controls/fullscreeno.svg';
		            }
		            this.fullscreen.onmouseout = function () {
		                me.tt_fullscreen.style.webkitTransition = 'none';
		                me.tt_fullscreen.style.visibility = 'hidden';
		                me.fullscreen__img.src = '/html5/images/Controls/fullscreen.svg';
		            }
		            this.tt_fullscreen = document.createElement('div');
		            this.tt_fullscreen.ggId = 'tt_fullscreen'
		            hs = 'position:absolute;';
		            hs += 'left: -56px;';
		            hs += 'top:  35px;';
		            hs += 'width: 148px;';
		            hs += 'height: 18px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: center;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.tt_fullscreen.setAttribute('style', hs);
		            this.tt_fullscreen.innerHTML = "Fullscreen";
		            this.fullscreen.appendChild(this.tt_fullscreen);
		            this.controller.appendChild(this.fullscreen);
		            this.divSkin.appendChild(this.controller);
		            this.loading = document.createElement('div');
		            this.loading.ggId = 'loading'
		            this.loading.ggUpdatePosition = function () {
		                this.style.webkitTransition = 'none';
		                w = this.parentNode.offsetWidth;
		                this.style.left = (-105 + w / 2) + 'px';
		                h = this.parentNode.offsetHeight;
		                this.style.top = (-30 + h / 2) + 'px';
		            }
		            hs = 'position:absolute;';
		            hs += 'left: -105px;';
		            hs += 'top:  -30px;';
		            hs += 'width: 210px;';
		            hs += 'height: 60px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            this.loading.setAttribute('style', hs);
		            this.loading.onclick = function () {
		                me.loading.style.webkitTransition = 'none';
		                me.loading.style.visibility = 'hidden';
		            }
		            this.loadingbg = document.createElement('div');
		            this.loadingbg.ggId = 'loadingbg'
		            hs = 'position:absolute;';
		            hs += 'left: 0px;';
		            hs += 'top:  0px;';
		            hs += 'width: 210px;';
		            hs += 'height: 60px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'opacity: 0.5;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'border-radius: 10px;';
		            hs += '-webkit-border-radius: 10px;';
		            hs += '-moz-border-radius: 10px;';
		            hs += 'background-color: #000000;';
		            this.loadingbg.setAttribute('style', hs);
		            this.loading.appendChild(this.loadingbg);
		            this.loadingbrd = document.createElement('div');
		            this.loadingbrd.ggId = 'loadingbrd'
		            hs = 'position:absolute;';
		            hs += 'left: -1px;';
		            hs += 'top:  -1px;';
		            hs += 'width: 208px;';
		            hs += 'height: 58px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'opacity: 0.5;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 2px solid #ffffff;';
		            hs += 'border-radius: 10px;';
		            hs += '-webkit-border-radius: 10px;';
		            hs += '-moz-border-radius: 10px;';
		            this.loadingbrd.setAttribute('style', hs);
		            this.loading.appendChild(this.loadingbrd);
		            this.loadingtext = document.createElement('div');
		            this.loadingtext.ggId = 'loadingtext'
		            hs = 'position:absolute;';
		            hs += 'left: 16px;';
		            hs += 'top:  12px;';
		            hs += 'width: auto;';
		            hs += 'height: auto;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.loadingtext.setAttribute('style', hs);
		            this.loadingtext.ggUpdateText = function () {
		                this.innerHTML = "Loading... " + (me.player.getPercentLoaded() * 100.0).toFixed(0) + "%";
		            }
		            this.loadingtext.ggUpdateText();
		            this.loading.appendChild(this.loadingtext);
		            this.loadingbar = document.createElement('div');
		            this.loadingbar.ggId = 'loadingbar'
		            hs = 'position:absolute;';
		            hs += 'left: 15px;';
		            hs += 'top:  35px;';
		            hs += 'width: 181px;';
		            hs += 'height: 12px;';
		            hs += '-webkit-transform-origin: 0% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 1px solid #808080;';
		            hs += 'border-radius: 5px;';
		            hs += '-webkit-border-radius: 5px;';
		            hs += '-moz-border-radius: 5px;';
		            hs += 'background-color: #ffffff;';
		            this.loadingbar.setAttribute('style', hs);
		            this.loading.appendChild(this.loadingbar);
		            this.divSkin.appendChild(this.loading);
		            this.userdata = document.createElement('div');
		            this.userdata.ggId = 'userdata'
		            this.userdata.ggUpdatePosition = function () {
		                this.style.webkitTransition = 'none';
		                w = this.parentNode.offsetWidth;
		                this.style.left = (-120 + w / 2) + 'px';
		                h = this.parentNode.offsetHeight;
		                this.style.top = (-80 + h / 2) + 'px';
		            }
		            hs = 'position:absolute;';
		            hs += 'left: -120px;';
		            hs += 'top:  -80px;';
		            hs += 'width: 240px;';
		            hs += 'height: 140px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: hidden;';
		            this.userdata.setAttribute('style', hs);
		            this.userdata.onclick = function () {
		                me.userdata.style.webkitTransition = 'none';
		                me.userdata.style.visibility = 'hidden';
		            }
		            this.userdatabg = document.createElement('div');
		            this.userdatabg.ggId = 'userdatabg'
		            hs = 'position:absolute;';
		            hs += 'left: 0px;';
		            hs += 'top:  0px;';
		            hs += 'width: 240px;';
		            hs += 'height: 140px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'opacity: 0.5;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'border-radius: 10px;';
		            hs += '-webkit-border-radius: 10px;';
		            hs += '-moz-border-radius: 10px;';
		            hs += 'background-color: #000000;';
		            this.userdatabg.setAttribute('style', hs);
		            this.userdata.appendChild(this.userdatabg);
		            this.userdatabrd = document.createElement('div');
		            this.userdatabrd.ggId = 'userdatabrd'
		            hs = 'position:absolute;';
		            hs += 'left: -1px;';
		            hs += 'top:  -1px;';
		            hs += 'width: 238px;';
		            hs += 'height: 138px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'opacity: 0.5;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 2px solid #ffffff;';
		            hs += 'border-radius: 10px;';
		            hs += '-webkit-border-radius: 10px;';
		            hs += '-moz-border-radius: 10px;';
		            this.userdatabrd.setAttribute('style', hs);
		            this.userdata.appendChild(this.userdatabrd);
		            this.title = document.createElement('div');
		            this.title.ggId = 'title'
		            hs = 'position:absolute;';
		            hs += 'left: 10px;';
		            hs += 'top:  10px;';
		            hs += 'width: 218px;';
		            hs += 'height: 20px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.title.setAttribute('style', hs);
		            this.title.ggUpdateText = function () {
		                this.innerHTML = "<b>" + me.player.userdata.title + "<\/b>";
		            }
		            this.title.ggUpdateText();
		            this.userdata.appendChild(this.title);
		            this.description = document.createElement('div');
		            this.description.ggId = 'description'
		            hs = 'position:absolute;';
		            hs += 'left: 10px;';
		            hs += 'top:  30px;';
		            hs += 'width: 218px;';
		            hs += 'height: 20px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.description.setAttribute('style', hs);
		            this.description.ggUpdateText = function () {
		                this.innerHTML = me.player.userdata.description;
		            }
		            this.description.ggUpdateText();
		            this.userdata.appendChild(this.description);
		            this.author = document.createElement('div');
		            this.author.ggId = 'author'
		            hs = 'position:absolute;';
		            hs += 'left: 10px;';
		            hs += 'top:  50px;';
		            hs += 'width: 218px;';
		            hs += 'height: 20px;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.author.setAttribute('style', hs);
		            this.author.ggUpdateText = function () {
		                this.innerHTML = me.player.userdata.author;
		            }
		            this.author.ggUpdateText();
		            this.userdata.appendChild(this.author);
		            this.datetime = document.createElement('div');
		            this.datetime.ggId = 'datetime'
		            hs = 'position:absolute;';
		            hs += 'left: 10px;';
		            hs += 'top:  70px;';
		            hs += 'width: auto;';
		            hs += 'height: auto;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.datetime.setAttribute('style', hs);
		            this.datetime.ggUpdateText = function () {
		                this.innerHTML = me.player.userdata.datetime;
		            }
		            this.datetime.ggUpdateText();
		            this.userdata.appendChild(this.datetime);
		            this.copyright = document.createElement('div');
		            this.copyright.ggId = 'copyright'
		            hs = 'position:absolute;';
		            hs += 'left: 10px;';
		            hs += 'top:  110px;';
		            hs += 'width: auto;';
		            hs += 'height: auto;';
		            hs += '-webkit-transform-origin: 50% 50%;';
		            hs += 'visibility: inherit;';
		            hs += 'border: 0px solid #000000;';
		            hs += 'color: #ffffff;';
		            hs += 'text-align: left;';
		            hs += 'white-space: nowrap;';
		            hs += 'padding: 0px 1px 0px 1px;'
		            hs += 'overflow: hidden;';
		            this.copyright.setAttribute('style', hs);
		            this.copyright.ggUpdateText = function () {
		                this.innerHTML = "&#169; " + me.player.userdata.copyright;
		            }
		            this.copyright.ggUpdateText();
		            this.userdata.appendChild(this.copyright);
		            this.divSkin.appendChild(this.userdata);
		            this.divSkin.ggUpdateSize = function (w, h) {
		                me.updateSize(me.divSkin);
		            }
		            this.divSkin.ggLoaded = function () {
		                me.loading.style.webkitTransition = 'none';
		                me.loading.style.visibility = 'hidden';
		            }
		            this.divSkin.ggReLoaded = function () {
		                me.loading.style.webkitTransition = 'none';
		                me.loading.style.visibility = 'inherit';
		            }
		            this.divSkin.ggEnterFullscreen = function () {
		            }
		            this.divSkin.ggExitFullscreen = function () {
		            }
		            this.skinTimerEvent();
		        };
		        this.hotspotProxyClick = function (id) {
		        }
		        this.hotspotProxyOver = function (id) {
		        }
		        this.hotspotProxyOut = function (id) {
		        }
		        this.skinTimerEvent = function () {
		            setTimeout(function () { me.skinTimerEvent(); }, 10);
		            if (me.elementMouseDown['up']) {
		                me.player.changeTilt(1);
		            }
		            if (me.elementMouseDown['down']) {
		                me.player.changeTilt(-1);
		            }
		            if (me.elementMouseDown['left']) {
		                me.player.changePan(-1);
		            }
		            if (me.elementMouseDown['right']) {
		                me.player.changePan(1);
		            }
		            if (me.elementMouseDown['zoomin']) {
		                me.player.changeFov(-1);
		            }
		            if (me.elementMouseDown['zoomout']) {
		                me.player.changeFov(1);
		            }
		            this.loadingtext.ggUpdateText();
		            var hs = '';
		            if (me.loadingbar.ggTransform) {
		                hs += me.loadingbar.ggTransform + ' ';
		            }
		            hs += 'scale(' + (1 * me.player.getPercentLoaded() + 0) + ',1.0) ';
		            me.loadingbar.style.webkitTransform = hs;
		            this.title.ggUpdateText();
		            this.description.ggUpdateText();
		            this.author.ggUpdateText();
		            this.datetime.ggUpdateText();
		            this.copyright.ggUpdateText();
		        };
		        function SkinHotspotClass(skinObj, hotspot) {
		            var me = this;
		            var flag = false;
		            this.player = skinObj.player;
		            this.skin = skinObj;
		            this.hotspot = hotspot;
		            this.div = document.createElement('div');
		            this.div.setAttribute('style', 'position:absolute; left:0px;top:0px;visibility: inherit;');
		            {
		                this.div = document.createElement('div');
		                this.div.ggId = 'hotspot'
		                hs = 'position:absolute;';
		                hs += 'left: 340px;';
		                hs += 'top:  20px;';
		                hs += 'width: 5px;';
		                hs += 'height: 5px;';
		                hs += '-webkit-transform-origin: 50% 50%;';
		                hs += 'visibility: inherit;';
		                this.div.setAttribute('style', hs);
		                this.div.onclick = function () {
		                    me.player.openUrl(me.hotspot.url, me.hotspot.target);
		                    me.skin.hotspotProxyClick(me.hotspot.id);
		                }
		                this.div.onmouseover = function () {
		                    me.player.hotspot = me.hotspot;
		                    me.hstext.style.webkitTransition = 'none';
		                    me.hstext.style.visibility = 'inherit';
		                    me.skin.hotspotProxyOver(me.hotspot.id);
		                }
		                this.div.onmouseout = function () {
		                    me.player.hotspot = me.player.emptyHotspot;
		                    me.hstext.style.webkitTransition = 'none';
		                    me.hstext.style.visibility = 'hidden';
		                    me.skin.hotspotProxyOut(me.hotspot.id);
		                }
		                this.hsimage = document.createElement('div');
		                this.hsimage.ggId = 'hsimage'
		                hs = 'position:absolute;';
		                hs += 'left: -16px;';
		                hs += 'top:  -16px;';
		                hs += 'width: 32px;';
		                hs += 'height: 32px;';
		                hs += '-webkit-transform-origin: 50% 50%;';
		                hs += 'visibility: inherit;';
		                this.hsimage.setAttribute('style', hs);
		                this.hsimage__img = document.createElement('img');
		                this.hsimage__img.setAttribute('src', '/html5/images/Controls/hsimage.svg');
		                this.hsimage__img.setAttribute('style', 'position: absolute;top: 0px;left: 0px;width: 32px;height: 32px;');
		                this.hsimage.appendChild(this.hsimage__img);
		                this.div.appendChild(this.hsimage);
		                this.hstext = document.createElement('div');
		                this.hstext.ggId = 'hstext'
		                this.hstext.ggUpdatePosition = function () {
		                    this.style.webkitTransition = 'none';
		                    this.style.left = (-50 + (100 - this.offsetWidth) / 2) + 'px';
		                }
		                hs = 'position:absolute;';
		                hs += 'left: -50px;';
		                hs += 'top:  20px;';
		                hs += 'width: auto;';
		                hs += 'height: auto;';
		                hs += '-webkit-transform-origin: 50% 50%;';
		                hs += 'visibility: hidden;';
		                hs += 'border: 1px solid #000000;';
		                hs += 'color: #000000;';
		                hs += 'background-color: #ffffff;';
		                hs += 'text-align: center;';
		                hs += 'white-space: nowrap;';
		                hs += 'padding: 0px 1px 0px 1px;'
		                hs += 'overflow: hidden;';
		                this.hstext.setAttribute('style', hs);
		                this.hstext.innerHTML = me.hotspot.title;
		                this.div.appendChild(this.hstext);
		            }
		        };
		        this.addSkinHotspot = function (hotspot) {
		            return new SkinHotspotClass(me, hotspot);
		        }
		        this.addSkin();
		    };

		    //////////////////////////////////////////////////////////////////////
		    // Pano2VR HTML5/CSS3 Panorama Player 3.0beta/1502                  //
		    // Licensed to: Edward Huff                                         //
		    // (c) 2010, Garden Gnome Software, http://gardengnomesoftware.com  //
		    //////////////////////////////////////////////////////////////////////
		    eval((function (x) { var d = ""; var p = 0; while (p < x.length) { if (x.charAt(p) != "`") d += x.charAt(p++); else { var l = x.charCodeAt(p + 3) - 28; if (l > 4) d += d.substr(d.length - x.charCodeAt(p + 1) * 96 - x.charCodeAt(p + 2) + 3104 - l, l); else d += "`"; p += 4 } } return d })("function lI0(px,o1,l1J){this.x=px;` %!y=o1` $\"Ol=l1J` &\"lJl=` Y$` 1H}` X#jO` U&iO){var iI=Math.si` /!;` /!0` .\"cos` +%llj=` x\"` (#1` )\"Ol`!-$i0*llj-iI*ll`!6&` *!j+` 8!1`!@&j` nXI`!5\"x`!42x`!D\"I+`!;+`!T\"I`!;,oJ` `g`\"{%`!=)`!4\"j`# %`!='j;};}`%K&oi(`$h#lJi`!N&` -#jo=`\"(#JI=0` ##0=` \"$1` +%l` ?%O` ,$Oi` 6$O` >&o=`\"^&Ji()`\"k$I`\"`/`\"L/`\"w,`!5Li` #$O=-iI`!U/` -$Jo=i0`$H&1`!!Z`!.%`!T(`!\"%`#&<`!`&`#<(`!S(0` ve`!#%`#4)` ,%`#5)`$`Doo`!U&IJ,IO`&3&IJ.jo*IO.jo+IJ.JI*IO.J1` '!0*IO.Oi`!R%` C%JI` A'l` A'`#5$0` A'0` A'O` B&Jo`\"4%IJ.J1`!9'l`!9'O`!7)l` B&`!=#` F\"`!>\"` F\"`$W&` B&`!>\"` F\"`!>\"` G!`!<$Oi=IJ.Oi`!:&O` R\"`!>\"`\"#!`!<$O`!k!` F\"`!>\"` E#`!>\"` F\"`!<$`#[!` E#`!?!` E#`!>\"`\"z\"o`$?$Il`/`'v`%~#i=new lI0;ii.x=v.x*`$T#+v.y` &\"JI+v.Ol` &#` E!y` @&J1` ?'l` >(O;ii.Ol` A&Oi` A&O1` ?(o;return ii`,Q)pano2vrPlayer(l10`!q\"`-]\"`\"1\"transitionsDisabled=false`-z\"1={j:0,ji:0,min:0,max:360,lj:0}` @!o` 5+-9` @\"9` 9(ll={j:9` B!9` B\"1` A!170,lii:` G\",mode` K%O={width:320,height:48` 6$i={OJ:{x:0,y:0},I1` \"'lastclick` )'j` A(l` #&` i#o={j1:-1,` *hIo={OO:true` ]*`!./li=0` %!divViewer=null` '(port` ()Panorama` $-view` ()Hotspots` /\"`%8!ol` \"'li0`'I!Arra`3(#checkLoaded` +,o0`%`#`'j\"j` \"(divSkin` q'is` _#`&1&percent` 1#`\"R\"ljl` }'` -!J0=1` $\"Ij` ##II`#N!` f!,timeout:5,io` ,#i1:0.4,l11`&!$ij={` 1*1,l1:0,o`&B%1`/<#skinObj`!q'userdata={title:\"\",description:\"\",author` 4!atetim` @!copyr`'.!\"\",sourc` 2!informa` T$comment:\"\"`!C\"j`#t-empty`$V#={`!o$,`!D%url:\"\",targe`!'!id`!-!kin` $\"ii:100,iJ:20,wordwrap`\"f#lI:null`!6\"sound`!5!`#w+globalVolume`#~#il={lj1` `#OI` h$j0` ##oi`'o\"lj`#r#`.!#h`\"##`\"+-`%u#ebugMsg`.B&l0o){` 2!=document.getElementById(\"` :!\");if(` &!` J#.innerHTML+=l0o+\"<br />\";}`!F#getP`&[)`!-%){`.,#`&{)`4K%iO` 8/lO.`,?\"*1/(2*Math.tan(` $!PI/180*(I.getVFov()/2)))` k$set`*N\"Size` s&ii,iJ){if(I.o0){ii=window`\"6\"Width;iJ` ()H`!@!;}`+>%.style.`.'!=ii+\"px\";` .,`!u\"=iJ` 6%`+*$` G4` 1+` R+I.Oj(`$.!I`*j$&&` \"%.ggUpdateSize){` \"2`\"W#;`-X%0`#j(I`#&*(I.llo.offset`\"s!,` &(`\"o\"`#f%changeViewMod`#e'v`#g!v==0){il.oi`,L#}` 0\"1` .$tru` ,%2` -$` 6!?` L!`'x!`!p%OI`!m(var v`(~!lI0(0,0,-100)`3G\"O=I.liO();for(var J=0;J<j0.length;J++` ^\"`(c$j0[J];v.lJl` b(.ljO(-` >#.o`&j\"`&a\"` 8\"j(` 5$l1` *0-l1.j` '.O(o` &-ar visible`#)#if(v.Ol<0.1`!g\"Ii=-oO/v.O`1L\"px=v.x*Ii`\"Q\"1=v.y*Ii;if`(J\"abs(px)<lO`&S\"/2+500&&` 4%o1` 8!`&P\"` 8\"){`!=$`#w#else{px=0;o1=0`$K!`\"J%I){` \"&.div`'_$ebkitTransition=\"none\"`\"-!` x\"` B3left=px+`!g'`($!` 81top=o1+`!u(` H!`!m\"` f6\"-100` Y9` 9%}}`,$Il1`(K*jj`\"|\"setT`3;\"(` 3)IlI();},1`(G%`,N#`'7,I`4R#1l=ll.mode;switch(l1l){case 0:I0=ll.j/2;break;` 2!1:I0=`$h\"tan(`\"`&`#8$`-w&` W\"`&F))*180/` +#` k(2:`+%\"J` v\"sqrt(` i%` s$`#n&*`!/%);`!84lJ` uR3:if` ^&*4/3>`!9$){`\"j&`%-\"`!'2*4/`!t&3)` }G}default:;}`1X#I0*2`1\"(`$b)llO`$l\"`$_(`$|\"=llO/2` (!Ji,l1j,llJ`$s0ll.j=`!!!`$z)Ji`\",*`(7\"`\"9&`\"+%I0`!u7` x!Ji` o*2:`$TQl1j`!G(lJ` xRl1j`!A*`$t9`\"t&`%+\"`\"b1*3`%+!` Y$`%&'`\"VC`%,'`15#IO`)e,J`.N\"`)s%j;if`&,!<ll.min`!e#` '\"`-p!ll.j>ll.max` 2'ax;}I0=I`*u$()`%`#loO`0 #`$Vbif(I0*2>o.max-o`!^\"I0=(` %(/2;}I`'Q$` K!);if` =\"<90){` (!j+I0` b\"){o.j=` \\\"I0`0E$` ?\"` 1-;}}` f\"in>-` c&-I0<`!H#` D#in+` ],` 1-` g\"l1`!6!l1.min<359.99`$:\"da=0`!u\"j!=0`)P#JO,l1o`#Y\"01=`0{&`$#J=`0a';lJO=l01`#V&Ji`4D*l1o=lI`,n'`#r+` o#l=lIJ`$\"\"tan`40&o.j`&u#` h%Il-=l1o`&'!Il>0){da`%2'1/(lIl/l1o`$n+da=da*`\"a*)/360`\"~%j+(Ji+da)>` ?\"){l1`):!` M!` 5#`%`!I.io){II.i1=-` \"!;l1.lj=` b'` I$<`!$#` c&in`!!$` HA`&&#90`$u\"90`&\"!`%O&`%c!o.j=-90`%O\"`.V#Oj`3x2if(!I.jj){`/,\";}` 7!`(d\"lIO(`$/#1i` -#`+h\"`%C!!=divViewer.offsetWidth||`%M%` 0.H`-I\"{` \\$=parseInt(` W1);` c%` 06` m#;` .&style.webkitTransformOriginX`'<'+\"px\"` 1BY`'_(` R\"l1i`#>\"}var oO`&S\"round(I.liO()`$p!`#{!liJ!=oO){` M%` /$=oO`-*\"1i){`!62Perspective=oO;if(divPanoview` P!` %$`!m2=\"translate3d(\"+`\"T*,` ,!`\"0(,\"+oO+\"px)\"`&Q\"` {#rama`!&%rama` w4rotateX(\"+Number`)w!.toFixed(10)+\"deg)  ` C\"Y` ?&-l1` 61\";}lOI();`'I#getPan`'H(`':\" l1.j` 7*N`1<,jl=` A!while(jl<-180){jl+=`*^!` 0$>` 0#-` 1\"` z#jl` w$s`!5+v){resetActivity(`%:!!isNaN(v)`*O#`\"B#v);}`)S#`\",&change` e,`!\"'`&'\"`\"2\"()+v`\"d(Tilt`\"^/o`\"f&s` 5,`!Q=o`!LA` d-`!\"(`!j%` (!`!e+Fov`$P0l`!g)` 8)`!T;&&v>0&&v<`$W!ll`!s)`!t'`!s)` q,`!.'`!u%Fov`!p)`$*\"`\"L*jl,Ii`!D8jl`%>$jl`*P!` 1#Ii`#e#Ii`#R/`!!&`!\\)`!)!,jO` P]` D'O)&&jO>0&&jO`#8'jO`!A2DefaultView`$[(`!d.`2a!i,o.ji,ll.ji` ]%moveTo`\"$.,i1`\"&/j.io`1o&`\"9'ij.l1`\"@!else` (#`*f\"`\"G+ij.o`\"R!` A$o=`(>!`\"I9ij.ll`\"b!` v%l=`'!\"` N&i1)&&i1>` J\"i1=i1` F&i1=1`&N%`\"O\"`#)1i1`#9#` >\"`#(-,i1);}`4/\"i`#-'x,y){li.OJ.x=x;` $\"y=y;li.I1` ,$I1` /!Io` ,$` $\"y=y` m#oj` d+`!$!lO=I.getV`(!!;l1.j+=x*llO/`0R%;o.j+=y` &+lIO`%m!`!j\"` m,li.j`!S$j`!e%l.x=` 4\"-`!v#` 0#y` 1\"y` 0#y;if(il.oi){oj(` T#,` E#)`\"Q%` f\"`\"V%` ^\";I`*a%var `&=)`'@(if(II.io){` \"!=false`%1!`&h!`%%\"` .%`\"B!j=new Date;l1I=lj`-8\"me`\"`&Oi` z&e`! !!il.OI){e=e?e:window.event;e.pr` %!`%j#`*<\"e.which||` \"#==0|` \"&1` p!e.target==I.ol){lio(e.pageX,` \"\"Y);lo.j1=1`!S\"`!q'var Jj`!q*lo.star` *!=Jj;`)Q,}}`$Y$0`$K%0`#\\$mouseMove`!{]`!n!`)#!`\"E?lij`\"C.}`!h.`$3$J` uf`#U\"-1`'N%`\"j(`#L@var jJ=-1;jJ=Math.abs(`*@#`(6$)+` ++`(8%`!I!Jj-`$K(<400&&jJ>=` \"!<20){` n+lastclick` l4` 7&` z-I.oo<7` q.s`\".\"out(`(s'I.toggleFullscreen();},10);I.oo=0`-z#` (!Jj;}`!P*=`*u'`!I'` 0#y`$j80`$r0lj0`$r1var wheelData=e.detail?e` \"#*-1:e.` ;\"elta/40`,4#ljo){` P&-` \"%;}ll.j+=` (%>0?1:-1`,/$`&&/`!z.`\"%\"JI`&y4if(!e`/=\"e=`\"$)}` m/var Ii=e.changedTouches`'H%<=0&&Ii[0]`0/#`*]Klo`1W\"` ^!`)J\"` -#y` )'Y;l`1V#` 9+`/7!` <(I.touchT`,|\"` 4\"`-)\";if(` $(`-3(` k',` c'`-?$` -\"identifier`#d.if(`!-)){e=` $);JJ`0(#while(e&&e!`!;#`,K!on`-E!over){` \")` y#` +%down&&!JJ` ='down();JJ=true;}e=e.parentNode;}`.F9lO`3x'`%_*`%'4`,m2`%A#`$$D}`-J(` _/for(`%!=0;J<Ii.length;J++`3}\"i[J`$+(==` i!`/W\"` 7\"`$j%` %#Y);break;}`*K7l`/S5`/;:` a-`.rW`(1\"-`#D#`.=(`(7\"` 3#`/$E`&=``.$!`&O'` +!`&99`\"$+`0V)`\"&0`0V)`\"3*`0K4`\"),`*/$`0MJ`)<cdbl`\"u,` +$`\"k9`2 3`#%)=`#+#;`\"z*` 0#y`+8hut`+w(ut`+r,up`+p,up`\"!;`$6(null`.3#-1`+|%0J`(lG` L.i` 2Hl.lii=ll.j`3(5OO` CJj=` p\"/`'K!sqrt(` @!.scale`4/6Oo` ]G` N3keyDown` U0l`-K!li=e.keyCode` J7Up` V)lli=0` =.` I!onBlur` </}` @-`\"B!I`\"<'`!X!`-p\"-1` (!il.oi){Io`1^\"li`'e!-I`'k#` 2\"y` 1#y` 1#y` 3!1` F&` )#` ?%`(h$` o#-`2\\#*0.1` j%` .#y` 0!oj(` 9(,` 1();I.jj`'Z#`)'!o.OO&&(` x#!=0||` o#!=0)&&`&~\"=-1`\"/&0.9*` %#`!9%` *&y`3?!` 9\"` ?$+` 4#` ;$<0.` d(` \\&`\"F#oj` Z$,` 4#`!h-lli!=0){switch(lli){case 37:oj(1,0`2}$` 0\"8:oj(0,1` (*9:oj(-` =,40` G\"-` @*43:` #!107` \"#6:`(L$j-1` ?(1` :$8` K$9` #\"45` \"\"91` Q&+` S$d`(=\":;}`\"B'if(!I.isLoaded){`3z!,lI1=0;for(J=0;J<I.check` ?\".length;J++`0t#` 2'[J].complete){lI1++`#K#I1==` T0){percent` 0\"=1;`!F&`!_\"` !divSkin&&` \"%.gg`!n$` \".`)o!`%)!` p*lI1/`!d*`!@#*1);}}`/o\"l1.j>360){l1.j-=360;` 0'<-` 3%+` 6\"if(ij.io` 0!lj=ij.l1-l1.j;o` )#o-o.j`.1!` 9\"l-`.6!var Oo=`-G&` \\!*` b!+o.lj*o.lj+` S!*` Y!);if(Oo*10<ij.i1){`!6!`1o#`\"N\"` =!>` :!*5){Oo=` '!/Oo` :#Oo=0.2;}`!(\"=Oo`!c!` $!`!%\"=Oo` 9!j+=` ?!;o.j+=o.l`!!j+`/E!`!y\"lj=new Date;l1I=lj.getTime(`(k)else if(II`#%!`\"t!II.l11*(o.ji-o.j)/100`\"#` 4$ll.ji`#)!` 7#`#^!`!t\"0.95+-II.i1*0.05;`!Q>`!C+`'=!I.OO`'v\"`\"!(`.Z$<0&&`\")(-l1I>II.timeout*1000){`\"2!` s\"`!\\\"0`%6\"`\"'$0;}`-N\".jj){`!A!`$M\"I.Oj();}s` z\"out(`/|'lIo();},20`0o$liI`0<(` A2I.setFullscreen(`!&!);},10);` \"M`!7!this.assignHandle`20'`#7\"jI;jI=divHotspots;I.ol=jI;liI(`! 4`\":$`!@6ll0`&&!`#\"\"` D!if(jI.addEventListener){` \"/(\"touchstart\",lJI,`\"3#` 26move\",lOj` )>end\",lOl` (>cancel\",l0J` 09gesture`!h$0i` *@chang`!o!O` +@`!o#o` /9mousedown\",lO`!(:` F!`#0\"` &!Move` I$document` ?4up\",lO`\"V:`!E\"blclick\",`&f!toggle`''&` ?>whe`#|!O0`!9?key`\"V\"keyDow` v%` 5:up` L!Up` G$window` :/orientation`$T%i`&s%` >5resize\",ll`!|%` 35blur\",onBlur` ,=scroll`#U#exi`*`'` T$}};`)N$ lJo(player,h`*H\"`*^\"I=this`+'\"` :\"=` A\"` -\"` E#=` M#` /\"div=`#N%createElement(\"div\")` A\"img` .5img` <'.setAttribute(\"src\",\"`!,#_red.pn` 38tyle\",\"position: absolute;top: -14px;left` $$` d$div.appendChild(` y$);O=` e&` e%\";O+=\"-webkit-user-select: none;\"` l&`!M1O`!,'on`(7!`.g(I`#z#.openUrl(`\"[#.url`$M$.target`/W%text`#m;`!p7`\"m#\"+` v$ii/2+\"px` 9#`#:! 20` (%width: ` E(` H#if`!f%iJ==0){O+=\"height: auto;\"`4[#` -(` _'J` c#}` `'wordwrap` h\"white-space: pre-wrap`!9=`!/\"`!J2` O#`!N-` S7`!8-no`!@#` 6!`%V$transform-origin: 50% 50%`!i#visibility: hidden` 2#border: 1px solid #000000` 8$ackground-color: #ffffff`$L$ext-align: center` 2#overflow`!!+padding: 0px`!0!0` \"\"`'F$text`'1:` =!innerHTML`+#$.title`'e(`/O!over`'i(`$&.ii=I` n\"offsetWidth;` ,#`!6!.left=-`'%$\";}` 1)`#Q&=\"inherit\"`(H$`!>(ut`)0*` L3`#(\"` U)`+#-text);}` '!l00` l(for(var J=0;J<j0.length;J++`\"Y!I.skinObj&&` \"%.addSkinH`/\"$j0[J].lI=new ` /4(` A!)`'@#` F)lJo`!d!,` ;$var jO=div` V#s.first`\"/!;if(jO){` 2(insertBefore`!#\".lI.div,jO`!+$` D(`\"w(` C();}}};var addSound`,H#`#*&sound`1W#j=-1;`#5*` :!Array`#;,` 0&[J].id==` -!.id){` .*lI.pause();`!i(remove`!o\"` A,);delete ` ),;Ij=J;}}`!&\"lI`.V5audio\");` A$`2B0` 6\"url` <'volume`\"%#level*globalV` 4!;`\"R$.loop`(^!` R%ggLoop=1`+<\"0`.7!` B&>=1` :.` 5&-1` K!` J#mode==1||` #(2|` \")3` 0*5)&&`!\"(`!N(autoplay=true`!M'` T\"0){`#G%.addEventListener(\"ended\",`+9*`)8!`!\">0){` $'--`)u\"play();}},false);if(Ij`!N&`$o\"Ij]`\"_\"`'O#`&E'push`&x#;}`'V4`!p$`4;%play`'e!`'Y&id,loop`&bid){`&]#`\"'&if(loop&&!isNaN(Number` /!))`\"@,`%4'` <(-1`\"R-` ?+` ),`%o#` >.`#r%`\"l#ause`\"c-`!q!id==\"_main\"`-m,`\"^3`*\"5}`!x\"`\"ku`\"4/`!1%`\"B#stop`!G}` (-currentTime=0`!e~`\"N5` (.`!J,`\"$et`,g\"`(6)v`(:\"vol`&o$v`)o!vol>1){vol=`,=\"vol<0` +\"`,~\"`#P)`-a(=vol`\":B`#D-`.[(` .&`.].`\"m~`#G*evel`\"2!`#\\.`!l#vo`!R-`#q#change`#e2`#7,`$#$` ])vol+`#pI`#+~`#+~`$\"'`\"m$`$!0`\"KN`$/hremove`1Z$`$s&`!n\"iJ;while(j0`\"S#>0){iJ=j0.pop();`23(` f\"`29\"iJ.lI.div);delete ` -!;` 3!=null`/]%setFullscreen`!8&`&6\"` =!o0!=v`4E#o0=v` R\"jj=true`\"|!` 4#` N!1){divViewport.style.position=\"absolute\";var li1=lil=0` (\"I=` T#er;if(lI.offsetParent){do{li1+=` 0%Left;lil` ('Top;}`#1\"lI` -&` \\#;}`!K.left=window.pageXO` M!-li1+\"px\";` A.top` F(Y` I%l` L\"`'K\"`\"L8fixed\"`#9!`$D!Skin&&` \"%.ggEnter`$1&){` \"7`3>&` x8relative`!~0`\"`!\"0`\"56` 6\"`!T7xi`%~'`!_+` /)`!h!ll0();`&]#toggle`&S0`&P#`&x)(!`&H$` X$e` s)` R(I` L+false` O%startAutorotat`.1(1,ljJ,lji){II.OO`'f\"II.io` %\"if(i1&&i1!=0` ?!i1=i`+)\"ljJ` -!timeout=ljJ` 1#` h\"loj=lji`)8&top`!10` q\"o=`!o!;`!A\"` &\"`#*)` B5OO=!` ]!`!q#` .!` T$l0j` E&l1`-_#O` 6#lo=document.getElementById` G!`%W(` ?&create` E#(\"div\"` =).setAttribute(\"id\",\"v` 5#\");O=\"top:  0px;\";O+=\"left:` %'`'C$:`'B$` 1#z-index: 0;`'0+`!%)`'H!\",O)`\"=%.append`-o\"` N'`!o%er`!zBer`\"&4er`!vBwidth:  10` *&height:` ')overflow: hidden`\"k,`.9$`\"g0O+=\"-webkit-user-select: none`#1&`!|-`#/&`#Q(`#)/er`1G)`\"z>`1u%`#+/h` 5#`\">~`\"|@100`\"|C`!}3`# A` T$`&|#`-|#=` /'`)x%J1`*D(`)x\"divPanoview`#];O`\"96`\"#8` 4(transform-`\"6!: preserve-3d`#5%I`#5%`\"w\"`!W$`\"a8` -3`%P!` 4'`)1)`#9+` <$` g%rama`\"Q>`!pG` 9.origin: 0`)#%`#*T`#!2rama`\"rA`\"Y/rama);var oJ,J;for(J=0;J<6;J++){oJ`\"d5img\");oJ`!+,rc\",ljl[J]`%q9`)X,`*#!`)f-128`)d.` )'`);(`&5N`$(@` ,4-z`-j.backface-visibility`+@+`(3M: \";if(J<4){O+=\"rotateY(\"+J*-90+\"deg)\";}else` 6'X(\"+(J==4?-90:90)` @%O+=\" scale(\"+lIj+\") `!)!late3d(-64.0px,-64.0px,` )#);\"`$y/`&7,`&f!`&;(oJ);I.li0.push` (#checkLoaded` -&}`,W%j`,T(if`)j){`*0&remove`){;null`-1)` ,!`!M!=new Array`!1'J`-l,l1O=1;if(window.devicePixelRatio){l1O=` &3;}return{ii:screen.`'S!*l1O,iJ` ,$`'S\"*l1O}`/>$getMaxS` ;!Re`(T!ion`!M,v=`!o$();`!%\" v.ii>v.iJ?v.ii:v.iJ` m$readConfigString` e&ljI`#f!`\"!#DOMParser){p` #!`#'!` -%;O0=` 2\".` \"!From` l\"(ljI,\"text/xml\")`&p$`#f#ctiveXObject(\"Microsoft.XMLDOM\");O0.async=\"false\";O0.loadXML`!_!;}`!z+Xml(O0)`\"0.Url`\"5&url`$g#ll`$a'XMLHttpRequest){lll`!c!` **`!}#` 5$`!i8HTTP\");}lll.open(\"GET\",url,`\"+!);lll.send(`.O\"O0=lll.responseXML;if(O0){`\"04`!G!alert(\"Error loading p`'m# XML\"`(k!`\"M!II=true`\"p,Xm`\"u'`!&'moveHotspots()`)J%` \"%iJ=0` s\"0O=O0.first`)@!` /#=l0O` %-J,v,J`1!\"I`!P#i=1000000;while(l0`&]!l0.nodeName==\"`4i\"{lJ=l` ~)` L#J` M\"J` G(start\"){v=lJ.g`,D'Node(\"pan\");l1.j=Number(v?v` [!Value:0` 7\"i=l1.j;` N3tilt\");o` J7o.ji=o` I6fov\");ll` K470` 8\"i=ll.j;}`\"7-min`\"#?min=1*`!M.`!y<` C1-9` @6`\"'%` E15);if(` 8\"<1e-8){` C$e-8;}`\",/ax`!{@ax`!iR` D0`!yA` E0120`\"7%ax>=180`\":\"ax=179.9;}}lJ=lJ.nextSibling;}v=l0` v2mode\"` n!v` c\"ode`%^%`!/&)`#2#`(F)autorotate\"`(8\"II){II.OO`*b\"` 4speed`!,%II.i1=1*`!#'` A5delay` N(timeout` 8C`1r\"tohorizon` ](l1`!3-`\"?.input`\"F\"!oI){oI=l0`\"e1lt` D$var iI=0;`\"U4`3t!siz`#}&iI`!;,if(iI>0&&iI>`4:7()&&iI<lIi){lIi=iI;`!J5control`'t\"`!D1imulatemass`# &o.OO=`!Q'==1`#V5lock`$~'il.lj1` L-` 4%OI` KJwheel` p*0` :Dinvert` O/o`!L0`'T!` M9`#E%` b&oi` Z-`#t/userdata`#n6title\");I.` D$.` .!=`*m).toString():\"\"`&54descripti`'x!` g'` .'` DPauthor` e*` .\"`!1Qatetim`\"F+` .$` >Pcopyright` e*` .%` @Psourc`!U+` .\"` :Pinfo` ^*informa`#[Tcommen`\"@-` 0!` S9`&r.hotspots\"){lJ=l0.firstChild;while(lJ`/{\"`3K)` P#`-=#` '#={l1:0,o:0,url:\"\",target:\"\",id:\"\",skin` $\"ii:100,iJ:20,wordwrap:false,lI:null}`3/4`4R\"`!.#.l1`4)O` U$o` 0Hur`*[&` Y$url`*_(`#e'`,;!`!.1`\"p!` Y-` 1\"` <M`+;#` ]+`+?\"` =Ji`/E&` `$id` 5L`$|\"` X-` 1\"` Q8`'T0width`!C.`.P)` D@he`*y#` Z+J`! M`'\"#` Z-` 1$`0&-j0.push(` B#);}lJ=lJ.nextSibling`4N0sound`)%L` P!`)L#` '!={`)'\"`)G#loop:0,level:1,mode:1,`)k%l0l:0,l0I:0`)\"5`$p'` }!`%/O`(Z(` ^\"`(1O`!!` X+` /!=Number(` e'` K7oo`$a&` ^#oop` 7Imod`)6&` ^\"mode` 7I`,k\"`!:)1` 4I`,o#` V(o` rLsiz`!y,l0`#\"Jtilt` R1I` Q2addSoundElement(` J!`(,3l0=l0` &*if(oI){for(J=0;J<6;J++){v=oI`!H2e\"+J+`&U)ljl[J]=new `&N#`!U*}` R7`\">)lJ0=1*` R';` >9ca`/,'lIj` L,}this.lJ1();` %\"00` #$jj=true;lII=`3-!;ll0();}` 9\"openUrl=function(url`4##`+0!url.length>0` (%substr` 1'-4)==\".xml\"||` &8swf\"){`!1%Next`!&(;}else{window` =!` 0*}};var lo`!f'){I.isLoaded`\";#I.check` -#new Array;if(I.divSkin&&` \"%.ggRe` G\"){` \"0();}percent` m#0`#4(Next`#+2lol(`$S!I`20!Obj`#?!` %%.`/c#ProxyOut){` \"5(I` *$.id)`0!\"`#H>url=` ;'0,` ;)+`$M\";}var O=`!p\".replace(\"$cur\",l1.j+\"/\"+o.` \"\"ll.j);O=O` ?'ap` C\"` '-t\",o` $/f\",` a\"I.readConfigUrl(url)`$!sl=O.split(\"/`'w\"s`&_(`#y!setPan(`+)#sl[0]))`*G!` B&1` B&Tilt` B'1` ;.2` C&Fov` A'2` G!resetActivity`([&l0j(l10`)+#assignHandler();}"))

		</script>
		<style type="text/css" title="Default">
			 
			.TableStyle table,.TableStyle tr,.TableStyle td {
				font-size: 10pt;
				border-color : #777777;
				background : #dddddd; 
				color: #000000; 
				border-style : solid;
				border-width : 1px;
			}
		 
			.warning {
				font-weight: bold;
			}
		</style>	

        
		<div id="container" style="width:708px;height:200px;"></div>
		<br />
		 
		<script type="text/javascript">
		    if (BrowserDetect.browser != "Safari" || BrowserDetect.browser != "Chrome")
		        document.getElementById("container").style.display = "none";
		    pano = new pano2vrPlayer("container");
		    skin = new pano2vrSkin(pano);
		    pano.readConfigUrl('<%= GetXMLFile() %>');
		    //			pano.readConfigString(panorama);
		    updateOrientation();
		    setTimeout(function () { updateOrientation(); }, 10);
		    setTimeout(function () { updateOrientation(); }, 1000);
		</script>
		<noscript>
			<p><b>Please enable Javascript!</b></p>
		</noscript>
		