﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteMenu.ascx.cs" Inherits="SitefinityWebApp.Controls.SiteMenu" %>

<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>
 
<script language="javascript">
  
    function URLDecode(encodedString) {
        return encodedString.replace(/\&amp;/g, '&');

    }

  

    function JustifyMenu() {
      
        var menu = $find("<%= RadMenu1.ClientID %>");


        var itemPadding = parseInt("<%= MenuPadding %>", 10);
        var itemBorder =0;
        var currentWidth = menu.get_childListElement().offsetWidth;
        var intTotal = 0;
        for (var i = 0; i < menu.get_items().get_count(); i++) {

            var itemDomElement = menu.get_items().getItem(i).get_textElement();
            var percentage = itemDomElement.offsetWidth / currentWidth;
            var newWidth = Math.round(percentage * menu.get_element().offsetWidth);
           
            if (i == menu.get_items().get_count() - 1) 
            {

               
              itemDomElement.style.width = currentWidth - intTotal - newWidth + 30 + "px";



            }
            else {

                itemDomElement.style.width = newWidth + itemPadding + "px";
             

            }
            intTotal += menu.get_items().getItem(i).get_element().offsetWidth;
            var span = menu.get_items().getItem(i).get_textElement();
            if (menu.get_items().getItem(i).get_selected()) {
                var NewImg = document.createElement("img");
                NewImg.setAttribute('id', 'curlMe');
                NewImg.style.position = "absolute";
                NewImg.style.top = "0px";

                var ilen = menu.get_items().getItem(i).get_element().offsetWidth - 13;

                NewImg.style.left = (ilen - 3) + "px";
                NewImg.style.border = "none";
                var baseurl = location.href.substring(0, location.href.indexOf('/', 14));
                NewImg.src = baseurl + "/App_Themes/VibcoOrange/Global/Images/menuCurl.png";
                
                span.appendChild(NewImg);

                var sideheader = document.getElementById("SideBarNavTitle");
               
                if (sideheader != null) {

                    sideheader.innerHTML = menu.get_items().getItem(i).get_text();
                }
            }
        }

     

    }

    function ShowToolTip(menu, args) {
        var pdescrip = document.getElementById("MenuSubHover");
        var item = args.get_item();
        var attributes = item.get_attributes();

        pdescrip.innerHTML = attributes.getAttribute("PageDescrip");

    }

    function ClearToolTip(menu, args) {
        var pdescrip = document.getElementById("MenuSubHover");
        pdescrip.innerHTML = "";
    }
</script>

<asp:Label ID="Label1"  Visible="false" runat="server" Text="" />
 <telerik:RadMenu ID="RadMenu1" DataSourceID = "SiteMap" EnableImagePreloading="true"  OnClientMouseOut="ClearToolTip" OnClientMouseOver="ShowToolTip"   runat="server"  OnClientLoad="JustifyMenu" OnItemDataBound="RadMenu1_ItemDataBound" Width="918px" EnableEmbeddedSkins="false" Skin="Vibco" MaxDataBindDepth="1" >
 </telerik:RadMenu>
<asp:SiteMapDataSource runat="server" ID="SiteMap" StartFromCurrentNode="false" ShowStartingNode="false" StartingNodeOffset="0"  />

 
