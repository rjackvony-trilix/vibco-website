﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Web.UI;
using System.IO;
using System.Reflection;
using System.Web;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Services;
using System.Configuration;
using Products.Migration.Properties;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace Products.Migration
{
    public class FilesMigrationManager
    {
        public static FilesMigrationManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new FilesMigrationManager();

                return instance;
            }
        }

        private static FilesMigrationManager instance;

        public void UploadProductImages(IDictionary<OldProductInfo, Product> products, int start, int offset)
        {
            ProductManager pmanager = new ProductManager();
            int i = 0;
            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                if (i >= start && i < start + offset)
                {
                    ProductUploadedFile uploadedImage = GetUploadedFile(Settings.Default.ProductImagesFolder, product.Key.ImageName);
                    if (uploadedImage != null)
                        product.Value.RelatedProductFiles.AddImageToUpload("Image", uploadedImage);

                    ProductUploadedFile uploadedSpecImage = GetUploadedFile(Settings.Default.ProductImagesFolder, product.Key.SchemaImageName);
                    if (uploadedSpecImage != null)
                        product.Value.RelatedProductFiles.AddImageToUpload("Spec Image", uploadedSpecImage);


                    pmanager.UploadFiles(product.Value, true);
                    pmanager.SaveProductChanges(product.Value, true);

                    if (uploadedImage != null)
                    {
                        uploadedImage.InputStream.Close();
                        //uploadedImage.InputStream.Dispose();
                    }
                    if (uploadedSpecImage != null)
                    {
                        uploadedSpecImage.InputStream.Close();
                        //uploadedImage.InputStream.Dispose();
                    }

                    //SystemManager.RestartApplication(true);
                }
                i++;
            }
        }

        public void PickProductDocuments1(IDictionary<OldProductInfo, Product> products)
        {
            List<ProductDocumentInfo> applicationBullettinsInfo = GetUploadedDocs("Product Application Bulletins", products);
            SaveProductFiles(products, applicationBullettinsInfo, "Application Bulletin");
        }

        public void PickProductDocuments2(IDictionary<OldProductInfo, Product> products)
        {
            List<ProductDocumentInfo> catalogsInfo = GetUploadedDocs("Catalogs", products);
            SaveProductFiles(products, catalogsInfo, "Catalog");

            List<ProductDocumentInfo> flyersInfo = GetUploadedDocs("Flyers", products);
            SaveProductFiles(products, flyersInfo, "Flyer");
        }
        
        public void UploadProductDocuments(IDictionary<OldProductInfo, Product> products)
        {
            List<ProductDocumentInfo> applicationBullettinsInfo = GetDocumentsInfoStartsWith("ApplicationBulletin");
            UploadFiles("Application Bulletins", applicationBullettinsInfo, @"D:\work\Dropbox\VibCo\Product Application Bulletins");
            SaveProductFiles(products, applicationBullettinsInfo, "Application Bulletin");

            List<ProductDocumentInfo> completeServiceManualInfo = GetDocumentsInfoStartsWith("CompleteServiceManual");
            UploadFiles("Complete Service Manuals", completeServiceManualInfo, @"D:\work\Dropbox\VibCo\Service Manuals");
            SaveProductFiles(products, completeServiceManualInfo, "Complete Service Manual");

            List<ProductDocumentInfo> quickReferenceGuideInfo = GetDocumentsInfoStartsWith("QuickReferenceGuide");
            UploadFiles("Quick Reference Guides", quickReferenceGuideInfo, @"D:\work\Dropbox\VibCo\QuickRefManuals");
            SaveProductFiles(products, quickReferenceGuideInfo, "Quick Reference Guide");

            List<ProductDocumentInfo> flyersGuideInfo = GetDocumentsInfoStartsWith("Flyer");
            UploadFiles(products, flyersGuideInfo, @"D:\work\vibco\files\Flyers");
            
            List<ProductDocumentInfo> productCatalogsInfo = GetDocumentsInfoStartsWith("ProductCatalog");
            UploadFiles(products, productCatalogsInfo, @"D:\work\vibco\files\Catalogs");
 
        }

        public void RemoveRedundantLibraries(IDictionary<OldProductInfo, Product> products)
        {
            List<Guid> libids = new List<Guid>();
            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                App.WorkWith().DocumentLibrary(product.Value.RelatedProductFiles.LibraryId).Delete().SaveChanges();
            }

            
            
        }

        protected List<ProductDocumentInfo> GetUploadedDocs(string libraryName, IDictionary<OldProductInfo, Product> products)
        {
            List<ProductDocumentInfo> result = new List<ProductDocumentInfo>();

            Guid libId = Guid.NewGuid();

            //LibrariesManager libManager = LibrariesManager.GetManager();

            //DocumentLibrary docLib = libManager.GetDocumentLibraries().Where(d => d.Title == libraryName).FirstOrDefault();

            DocumentLibrary dlib = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault();

            foreach (Document doc in dlib.Documents)
            {
                tfile tfile = DataContext.tfiles.Where(tf => tf.filepath.Contains(doc.Title)).FirstOrDefault();
                
                if(tfile != null)
                    result.Add(new ProductDocumentInfo() { 
                        FileName = doc.Title,
                        FileId = DataContext.tfiles.Where(tf => tf.filepath.Contains(doc.Title)).First().fileid,
                        NewFileId = doc.Id });
            }
            //DocumentLibrary lib = App.WorkWith().DocumentLibraries().Where(d => d.Title == libraryName).First().;
            


            return result;
        }

        protected void UploadFiles(string LibraryName, IList<ProductDocumentInfo> docs, string folderPath)
        {
            Guid libId = Guid.NewGuid();
            App.WorkWith().DocumentLibrary().CreateNew(libId).Do(d =>
            {
                d.Title = LibraryName;
            }).SaveChanges();

            IList<ProductDocumentInfo> docstodelete = new List<ProductDocumentInfo>();

            foreach(ProductDocumentInfo doc in docs)
            {
                ProductUploadedFile file = GetUploadedFile(folderPath, doc.FileName);
                try
                {
                    App.WorkWith()
                                .DocumentLibrary(libId)
                                .CreateDocument()
                                .Do(d =>
                                {
                                    d.Title = doc.FileName;
                                    doc.NewFileId = d.Id;
                                })
                                .CheckOut()
                                .UploadContent(file.InputStream, file.GetExtension())
                                .CheckInAndPublish()
                                .SaveChanges();
                }
                catch (Exception ex)
                {
                    docstodelete.Add(doc);
                }

            }

            foreach(ProductDocumentInfo doc in docstodelete)
            {
                docs.Remove(doc);
            }

        }

        protected void UploadFiles(IDictionary<OldProductInfo, Product> products, IList<ProductDocumentInfo> docs, string folderPath)
        {
            ProductManager manager = new ProductManager();
            IList<ProductDocumentInfo> docstodelete = new List<ProductDocumentInfo>();

            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                ProductDocumentInfo docinfo = GetOldProductDocumentInfo(product.Key, docs);
                if (docinfo != null)
                {
                    ProductUploadedFile file = GetUploadedFile(folderPath, docinfo.FileName);
                    try
                    {
                        manager.UploadFileToProductLibrary(product.Value.RelatedProductFiles.LibraryId, file);
                    }
                    catch (Exception ex)
                    {
                        docstodelete.Add(docinfo);
                    }
                }

            }

            foreach (ProductDocumentInfo doc in docstodelete)
            {
                docs.Remove(doc);
            }

        }

        protected void SaveProductFiles(IDictionary<OldProductInfo, Product> products, List<ProductDocumentInfo> files, string type)
        {
            ProductManager manager = new ProductManager();

            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                if (GetOldProductDocumentInfo(product.Key, files) != null)
                {
                    Guid id = GetOldProductDocumentInfo(product.Key, files).NewFileId;
                    product.Value.RelatedProductFiles.DeleteFile(type);
                    product.Value.RelatedProductFiles.Add(type, id, ProductFileType.File);
                    manager.SaveProductChanges(product.Value, true);
                }
            }
        }

        private ProductDocumentInfo GetOldProductDocumentInfo(OldProductInfo oldproduct, IList<ProductDocumentInfo> files)
        {
            ProductDocumentInfo result = null;

            var query = from tpf in DataContext.tproduct_files
                        where tpf.productid == oldproduct.ProductId
                        select tpf;

            foreach (tproduct_file tpf in query)
            {
                if (files.Where(c => c.FileId == tpf.fileid).Count() > 0)
                {
                    result = files.Where(c => c.FileId == tpf.fileid).First();
                }
            }

            return result;
        }

        private List<ProductDocumentInfo> GetDocumentsInfoStartsWith(string type)
        {
            List<ProductDocumentInfo> result = new List<ProductDocumentInfo>();
            var query = from f in DataContext.tfiles
                        where f.filepath.StartsWith(type)
                        select f;

            foreach (tfile tf in query)
            {
                result.Add(new ProductDocumentInfo() { FileId = tf.fileid, FileName = tf.filepath.Split('/')[1] });
            }

            return result;
        }

        private List<ProductDocumentInfo> GetDocumentsInfoNotStartsWith()
        {
            List<ProductDocumentInfo> result = new List<ProductDocumentInfo>();
            var query = from f in DataContext.tfiles
                        where !f.filepath.StartsWith("ApplicationBulletin")
                            && !f.filepath.StartsWith("CompleteServiceManual")
                            && !f.filepath.StartsWith("QuickReferenceGuide")
                        select f;

            foreach (tfile tf in query)
            {
                result.Add(new ProductDocumentInfo() { FileId = tf.fileid, FileName = tf.filename });
            }

            return result;
        }

        private ProductUploadedFile GetUploadedFile(string folderpath, string name)
        {
            ProductUploadedFile file = new ProductUploadedFile();
            string filepath = Path.Combine(folderpath, name);
            
            if (!File.Exists(filepath))
                return null;
                   
            file.InputStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            file.FileName = name;
            file.Extension = "." + name.Split('.')[1];
            return file;
            
           
        }

        public OldVibcoProductsDataContext DataContext
        {
            get
            {
                if (datacontext == null)
                    datacontext = new OldVibcoProductsDataContext();
                return datacontext;
            }
        }

        private OldVibcoProductsDataContext datacontext;

    }
}
