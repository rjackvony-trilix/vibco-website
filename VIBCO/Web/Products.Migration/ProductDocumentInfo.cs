﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Migration
{
    public class ProductDocumentInfo
    {
        public string FileName { get; set; }
        public long FileId { get; set; }

        public Guid NewFileId { get; set; }
    }

    
}
