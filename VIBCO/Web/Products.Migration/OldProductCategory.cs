﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Migration
{
    public class OldProductCategory
    {
        public long OldCategoryId { get; set; }
        public long OldParentId { get; set; }
        public Guid CategoryId { get; set; }

        public string OldProductName { get; set; }
        public string OldProductDesсription { get; set; }
    }
}
