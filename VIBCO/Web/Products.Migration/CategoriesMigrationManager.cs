﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Products.Migration
{
    public class CategoriesMigrationManager
    {
        public static CategoriesMigrationManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new CategoriesMigrationManager();

                return instance;
            }
        }

        private static CategoriesMigrationManager instance;

        public IList<OldProductCategory> GetCategories()
        {
            IList<OldProductCategory> result = new List<OldProductCategory>();
            
            IList<long> oldcatids = GetAllOldCategories();

            result.Add(new OldProductCategory()
            {
               OldCategoryId = oldcatids[0]
            });
            
            FillCategories(1, oldcatids, result);
            
            return result;
        }

        private void FillCategories(int i, IList<long> oldcatids, IList<OldProductCategory> result)
        {
            IList<long> parentids = GetParentCategories(oldcatids[i]);
            var oldcategory = DataContext.tproducttrees.Where(t => t.producttreeid == oldcatids[i]).Single();
            result.Add(new OldProductCategory() { 
                OldCategoryId = oldcatids[i], 
                OldParentId = parentids[parentids.Count - 1],
                OldProductName = oldcategory.producttreename,
                OldProductDesсription = oldcategory.producttreedesc
            });

            if(i+1 < oldcatids.Count)
                FillCategories(i + 1, oldcatids, result);
        }

        private IList<long> GetAllOldCategories()
        {
            var query = from p in DataContext.tproducttrees
                        select p.producttreeid;
            return query.ToList<long>();
        }

        private IList<long> GetParentCategories(long id)
        {
            var query = from p1 in DataContext.tproducttrees
                        from p2 in DataContext.tproducttrees
                        where p2.producttreeid == id && p2.lft > p1.lft && p2.lft < p1.rgt
                        select p1.producttreeid;

            return query.ToList<long>();

        }

        public void SetProductCategories(IList<OldProductCategory> categories)
        {
            categories[0].CategoryId = ProductSetup.ProductsCategoryId;
            //var rootTaxonProducts = taxonomyManager
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var taxonomy = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().Where(t => t.Name == "Categories").SingleOrDefault();

            bool isFirst = true;
            foreach (OldProductCategory category in categories)
            {
                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    Guid parentId = categories.Where(c => c.OldCategoryId == category.OldParentId).Single().CategoryId;
                    category.CategoryId = Guid.NewGuid();

                    var taxonProducts = taxonomyManager.CreateTaxon<HierarchicalTaxon>(category.CategoryId);
                    taxonProducts.Title = category.OldProductName;
                    taxonProducts.Name = category.OldProductName;
                    taxonProducts.Description = category.OldProductDesсription;
                    taxonProducts.Parent = taxonomyManager.GetTaxon<HierarchicalTaxon>(parentId);
                    taxonomy.Taxa.Add(taxonProducts);

                    taxonomyManager.SaveChanges();
                }
            }
        }

        public OldVibcoProductsDataContext DataContext
        {
            get
            {
                if (datacontext == null)
                    datacontext = new OldVibcoProductsDataContext();
                return datacontext;
            }
        }

        private OldVibcoProductsDataContext datacontext;
    }
}
