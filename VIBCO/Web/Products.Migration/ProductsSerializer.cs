﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Products.Migration.Properties;
using System.Runtime.Serialization;

namespace Products.Migration
{
    public class ProductsSerializer
    {
        
        public static void Save(IDictionary<OldProductInfo, Product> products)
        {
            
            using (FileStream writer = new FileStream(Settings.Default.ProductsSerializationFilePath,
                FileMode.Create, FileAccess.Write))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(IDictionary<OldProductInfo, Product>));
                ser.WriteObject(writer, products);
            }
        }

        public static IDictionary<OldProductInfo, Product> Load()
        {
            IDictionary<OldProductInfo, Product> products = new Dictionary<OldProductInfo, Product>();

            using (FileStream reader = new FileStream(Settings.Default.ProductsSerializationFilePath,
              FileMode.Open, FileAccess.Read))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(IDictionary<OldProductInfo, Product>));
                products = (IDictionary<OldProductInfo, Product>)ser.ReadObject(reader);
            }


            return products;
        }


    }
}
