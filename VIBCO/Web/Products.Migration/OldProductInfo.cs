﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Migration
{
    public class OldProductInfo
    {
        public long ProductId { get; set; }
        public long CategoryId { get; set; }
        public string ImageName { get; set; }
        public string SchemaImageName { get; set; }
    }
}
