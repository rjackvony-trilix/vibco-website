﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity;

namespace Products.Migration
{
    public class MigrationManager
    {
        public void DoMigration1()
        {
            SetProductParameters();

            IList<OldProductCategory> categories = CategoriesMigrationManager.Instance.GetCategories();

            CategoriesMigrationManager.Instance.SetProductCategories(categories);

            IDictionary<OldProductInfo, Product> products = GetProductNames();

            FillDescriptions(products);

            FillDimensionsAndAccessoires(products);

            FillCategories(products, categories);

            SetProducts(products);

            ProductsSerializer.Save(products);
        }

        public void DoMigration2(int i)
        {
            IDictionary<OldProductInfo, Product> products = ProductsSerializer.Load();

            FilesMigrationManager.Instance.UploadProductImages(products, (int)(products.Count/6) * (i-1), (int)(products.Count/6));

            ProductsSerializer.Save(products);
        }

        public void DoMigration2_3(int i)
        {
            IDictionary<OldProductInfo, Product> products = ProductsSerializer.Load();

            FilesMigrationManager.Instance.UploadProductImages(products, (int)(products.Count / 3) + (int)(products.Count / 12) * (i - 1), (int)(products.Count / 12) * i);

            ProductsSerializer.Save(products);
        }

        public void DoMigration3()
        {
            IDictionary<OldProductInfo, Product> products = ProductsSerializer.Load();

            FilesMigrationManager.Instance.UploadProductDocuments(products);

            ProductsSerializer.Save(products);
        }

        public void DoMigrationUpdate1()
        {
            App.WorkWith()
               .ContentItems() //working with Generic Content items. Could be . NewsItems(); .BlogPosts() etc.
               .ForEach(cI =>
               {
                   try
                   {
                       App.WorkWith().ContentItem(cI.Id).CheckOut().CheckInAndPublish().SaveChanges();
                   }
                   catch (Exception ex)
                   { }
               });
        }

        public void DoMigrationUpdate2()
        {
            IDictionary<OldProductInfo, Product> products = ProductsSerializer.Load();

            string[] badParameters = new string[] {"Force lbs", "Force Newtons", "dB", "Max Lbs Material in Bin"  };
            ProductManager pmanager = new ProductManager();

            foreach (KeyValuePair<OldProductInfo, Product> tempproduct in products)
            {
                Product product = pmanager.GetProduct(tempproduct.Value.Id);
                IList<ListAttribute> laToRemove = new List<ListAttribute>();
                foreach (ListAttribute la in product.Dimensions.Items)
                    if (badParameters.Contains(la.ParamName))
                    {
                        laToRemove.Add(la);

                        product.TechnicalData.Items.Add(la);
                    }

                foreach (ListAttribute la in laToRemove)
                    product.Dimensions.Items.Remove(la);
                
                if (laToRemove.Count > 0)
                    pmanager.SaveProductChanges(product, true);
                
            }
            
        }

        public void DoAccessoiresMigration()
        {
            ProductManager pmanager = new ProductManager();

            IList<Product> products = pmanager.GetProducts();

            IDictionary<OldProductInfo, Product> accessoires = GetProductAccessories();
            foreach (KeyValuePair<OldProductInfo, Product> product in accessoires)
            {
                product.Value.Categories.Add(new ProductCategory() { Id = ProductSetup.ProductAccessoriesCategoryId });
                
                pmanager.CreateProduct(product.Value);
            }

            IList<tproduct_accessory> paccessories = new List<tproduct_accessory>();

            foreach(tproduct_accessory tpa in DataContext.tproduct_accessories)
                paccessories.Add(tpa);

            IDictionary<OldProductInfo, Product> tproducts = ProductsSerializer.Load();
            
            foreach (KeyValuePair<OldProductInfo, Product> tempproduct in tproducts)
            {
                if (paccessories.Where(pa => pa.productid == tempproduct.Key.ProductId).Count() > 0)
                {
                    
                    Product product = products.Where(p => p.Id == tempproduct.Value.Id).Single();
                    foreach (tproduct_accessory paccessory in paccessories.Where(pa => pa.productid == tempproduct.Key.ProductId).OrderBy(pa => pa.accessoryorder))
                    {
                        KeyValuePair<OldProductInfo, Product> pa = accessoires.Where(a => a.Key.ProductId == paccessory.accessoryid).Single();
                        product.Accessories.Items.Add(new Accessory()
                        {
                            Id = pa.Value.Id,
                            Name = pa.Value.Title
                        });
                    }

                    pmanager.SaveProductChanges(product, true);
                }
            }
        }

        public void DoMigrationUpdate3()
        {
            IDictionary<OldProductInfo, Product> products = GetNewProducts();

            FilesMigrationManager filesMigrationManager = new FilesMigrationManager();

            filesMigrationManager.PickProductDocuments1(products);
        }

        public void DoMigrationUpdate4()
        {
            IDictionary<OldProductInfo, Product> products = GetNewProducts();

            FilesMigrationManager filesMigrationManager = new FilesMigrationManager();

            filesMigrationManager.PickProductDocuments2(products);
        }

        public void DoMigrationUpdate5()
        {
            IDictionary<OldProductInfo, Product> products = GetNewProducts();

            FilesMigrationManager filesMigrationManager = new FilesMigrationManager();

            filesMigrationManager.RemoveRedundantLibraries(products);
        }


        private IDictionary<OldProductInfo, Product> GetNewProducts()
        {
            ProductManager pmanager = new ProductManager();

            IDictionary<OldProductInfo, Product> tproducts = ProductsSerializer.Load();

            IList<Product> newproducts = pmanager.GetProducts();

            IDictionary<OldProductInfo, Product> result = new Dictionary<OldProductInfo, Product>();

            foreach (KeyValuePair<OldProductInfo, Product> tp in tproducts)
            {
                Product prod = newproducts.Where(p => p.Id == tp.Value.Id).Single();
                result.Add(tp.Key, prod);
            }

            return result;
        }

        private void FillCategories(IDictionary<OldProductInfo, Product> products, IList<OldProductCategory> categories)
        {
            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                if (product.Key.CategoryId != 0)
                {
                    var pcategory = categories.Where(c => c.OldCategoryId == product.Key.CategoryId).Single();
                    product.Value.Categories.Add(new ProductCategory() { Id = pcategory.CategoryId });
                }
            }
        }

        public void SetProducts(IDictionary<OldProductInfo, Product> products)
        {
            ProductManager pManager = new ProductManager();
            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                pManager.CreateProduct(product.Value);
            }
        }

        private void FillDimensionsAndAccessoires(IDictionary<OldProductInfo, Product> products)
        {

            ProductManager pManager = new ProductManager();
            IList<string> dimensionParams  = pManager.GetDimensionParameters();
            IList<string> techdataParams = pManager.GetTechnicalDataParameters();

            var query = from tppf in DataContext.tproduct_productfields
                        from tp in DataContext.tproductfields
                        where tppf.productfieldid == tp.productfieldid
                        select new { tppf.productid, tp.productfieldname, tppf.productfieldid, tppf.productfieldvalue };

            var fields = query.ToList();

            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                foreach (var field in fields.Where(f => f.productid == product.Key.ProductId).Where(f => f.productfieldid <= 15))
                {
                    if(dimensionParams.Contains(field.productfieldname))
                    {
                        ListAttribute la = new ListAttribute()
                        {
                            ParamName = field.productfieldname,
                            Value = field.productfieldvalue
                        };
                        product.Value.Dimensions.Items.Add(la);
                    }
                }
            }

            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                foreach (var field in fields.Where(f => f.productid == product.Key.ProductId).Where(f => f.productfieldid >= 24 && f.productfieldid <= 40))
                {
                    if (techdataParams.Contains(field.productfieldname))
                    {
                        ListAttribute la = new ListAttribute()
                        {
                            ParamName = field.productfieldname,
                            Value = field.productfieldvalue
                        };
                        product.Value.Dimensions.Items.Add(la);
                    }
                    else if(techdataParams.Where(tp => field.productfieldname.Contains(tp)).Count() > 0)
                    {
                        if (field.productfieldname.EndsWith("VPM"))
                        {
                            ListAttribute la = new ListAttribute()
                            {
                                ParamName = field.productfieldname.Sub(0, 5),
                                Value = string.Empty
                            };
                            product.Value.TechnicalData.Items.Add(la);

                            ListAttribute la1 = new ListAttribute()
                            {
                                ParamName = "VPM",
                                Value = field.productfieldvalue
                            };
                            product.Value.TechnicalData.Items.Add(la1);
                        }
                        else if (field.productfieldname.EndsWith("CFM"))
                        {
                            ListAttribute la1 = new ListAttribute()
                            {
                                ParamName = "CFM",
                                Value = field.productfieldvalue
                            };
                            product.Value.TechnicalData.Items.Add(la1);
                        }
                    }
                }
            }
        }

        private void FillDescriptions(IDictionary<OldProductInfo, Product> products)
        {
            foreach (KeyValuePair<OldProductInfo, Product> product in products)
            {
                if (product.Key.CategoryId != 0)
                {
                    tproducttree producttreeElem = DataContext.tproducttrees.Where(pt => pt.producttreeid == product.Key.CategoryId).First();
                    product.Value.Description = producttreeElem.producttreecopy;
                }
            }
 	    }

        private IDictionary<OldProductInfo, Product> GetProductAccessories()
        {
            IDictionary<OldProductInfo, Product> result = new Dictionary<OldProductInfo, Product>();
            foreach (taccessory ta in DataContext.taccessories)
            {
                Product product = new Product()
                {
                    Key = ta.accessorykey,
                    Name = ta.accessoryname,
                    Title = ta.accessorykey + " - " + ta.accessoryname
                };

                OldProductInfo info = new OldProductInfo()
                {
                    ProductId = ta.accessoryid,
                };

                result.Add(info, product);
            }
            return result;
        }

        private IDictionary<OldProductInfo, Product> GetProductNames()
        {
            IDictionary<OldProductInfo, Product> products = new Dictionary<OldProductInfo, Product>();
            foreach (tproduct tp in DataContext.tproducts)
            {
                Product product = new Product()
                    {
                         Title = tp.productkeydisplay + " - " + tp.productname,
                         Key = tp.productkeydisplay,
                         Name = tp.productname
                    };

                OldProductInfo info = new OldProductInfo()
                {
                    ProductId = tp.productid,
                    CategoryId = tp.productcategoryid, 
                    ImageName = tp.productimage, 
                    SchemaImageName = tp.productschematicimage
                };

                products.Add(info, product);    
            }
            return products;
        }

        
        #region Set Methods
        private void SetProductParameters()
        {

            App.WorkWith().ContentItem(ProductSetup.DimensionParametersContentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = 
                                "<p>A</p> <p>B</p> <p>C*</p> <p>L</p> <p>W</p> <p>H</p> <p>D</p> <p>E</p> <p>F</p> <p>G</p> <p>K</p> <p>R</p> <p>S</p> <p>INLET</p> <p>OUTLET</p> <p>Weight</p>";
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();

            App.WorkWith().ContentItem(ProductSetup.TechnicalDataParametersContentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = "<p>40 psi</p> <p>60 psi</p> <p>80 psi</p> <p>&nbsp;&nbsp;VPM</p> <p>&nbsp;&nbsp;CFM</p> <p>Force lbs</p> <p>Force Newtons</p> <p>dB</p> <p>Max Lbs Material in Bin</p>";
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();
        }
        #endregion

        public OldVibcoProductsDataContext DataContext
        {
            get
            {
                if(datacontext == null)
                    datacontext = new OldVibcoProductsDataContext();
                return datacontext;
            }
        }

        private OldVibcoProductsDataContext datacontext;

        
    }
}
