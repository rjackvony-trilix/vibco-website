﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web;
namespace Widgets
{
    public class CustomSearchBox : Telerik.Sitefinity.Services.Search.Web.UI.Public.SearchBox
    {
        private static Telerik.Sitefinity.Services.Search.Web.UI.Public.SearchBox sBox;
        private string layoutTemplateName = "Widgets.CustomSearchBox.ascx";
        public CustomSearchBox()
        {
            //this.SearchButton.OnClientClick = "clickme(" + this + ");";
            //sBox = this;
             
        }
        protected override string LayoutTemplateName
        {
            get
           { 
                
                return null;
            }
        }
        protected override void  OnPreRender(EventArgs e)
        {
 	         base.OnPreRender(e);
             if (!Page.Request.Url.AbsoluteUri.Contains("*") && Page.Request.Url.AbsoluteUri.Contains("search-results?"))
             {
                 // http://localhost:60876/search-results?indexCatalogue=all%2Dvibco&searchQuery=Search...&wordsMode=0
                 string urlDecoded = HttpUtility.UrlDecode(Page.Request.Url.AbsoluteUri);
                 int urlIndex = urlDecoded.IndexOf("searchQuery=");
                 string url = urlDecoded.Left(urlIndex + 12);
                 string parameters = urlDecoded.Substring(urlIndex + 12, urlDecoded.Length - urlIndex - 12 - 12);

                 //remove periods from search
                 parameters = parameters.Replace(".", "");

                 string newParameters = "";
                 for (int len = 0; len < parameters.Length; len++)
                 {
                     int Num = 0;
                     string currentChar = parameters[len].ToString();
                     newParameters += currentChar;

                     //is the current char not a number? and...
                     //is the next char a number?
                     if (!int.TryParse(currentChar, out Num) &&
                       (len < parameters.Length - 1 && int.TryParse(parameters[len + 1].ToString(), out Num)))
                     {
                         newParameters += "*";
                     }
                 }
                 newParameters += "";
                 Page.Response.Redirect(url + newParameters+"*");
             }
             else
             {

             }
        }
 
        
        /*[System.Web.Services.WebMethod()] 
        [System.Web.Script.Services.ScriptMethod()] 
        public static Telerik.Sitefinity.Services.Search.Web.UI.Public.SearchBox GetBox() 
        {
            return sBox;
        }*/
        public override string LayoutTemplatePath
        {
            get
            {
                var path = "~/Widgets/Widgets.CustomSearchBox.ascx";
                return path;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

    }
}
