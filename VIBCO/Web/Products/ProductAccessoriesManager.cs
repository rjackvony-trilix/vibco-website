﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;

namespace Products
{
    public class ProductAccessoriesManager
    {
        public ProductAccessoriesManager()
        {
        }

        public ProductAccessoriesManager(StateBag viewState, string attributeName)
        {
            this.viewState = viewState;
            this.attributeName = attributeName;
        }

        private string attributeName;
        private StateBag viewState;

        public List<Accessory> Dimensions
        {
            get
            {
                if (viewState[attributeName] == null)
                    viewState[attributeName] = new List<Accessory>();
                return (List<Accessory>)this.viewState[attributeName];
            }
        }

        public List<Accessory> Select()
        {
            return Dimensions;
        }

        public void Insert(string Id)
        {
            Guid id = new Guid(Id);
            ProductManager pm = new ProductManager();
            Product product = pm.GetProduct(id);

            //IDictionary<Guid, string> products = (IDictionary<Guid, string>)HttpContext.Current.Cache["ProductsIdName"];

            Dimensions.Add(new Accessory { Id = id, Name = product.Name });
        }

        public void Delete(object Id)
        {
            Guid id = (Guid)Id;

            Accessory attributeToDelete = Dimensions.Where(a => a.Id == id).FirstOrDefault();

            Dimensions.Remove(attributeToDelete);
        }
    }
}
