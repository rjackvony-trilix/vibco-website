﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;

namespace Products
{
    public class BaseProduct
    {

        public BaseProduct()
        {
            LoadBasicFields();
        }

        public BaseProduct(ProductManager pm, ContentItem contentItem)
        {
            LoadBasicFields();

            id = contentItem.Id;
            title = contentItem.Title;

            name = contentItem.GetValue<string>("PRODUCT NAME");
            key = contentItem.GetValue<string>("PRODUCT KEY");

            IList<Guid> categoryIds = (IList<Guid>)contentItem.GetValue("Category");

            foreach (ProductCategory category in pm.GetCategories())
            {
                if (categoryIds.Contains(category.Id))
                    Categories.Add(category);
            }
        }

        public Guid Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }
        

        public IList<ProductCategory> Categories
        {
            get
            {
                return productCategories;
            }
            set
            {
                productCategories = value;
            }
        }

        public string EditProductUrl
        {
            get
            {
                return string.Concat("~/sitefinity/content/products/product-editor?productid=", Id);
            }
        }

        public string ProductUrl
        {
          get
          {
            return string.Concat("product/product?productid=", Id);
          }
        }

        public string MainCategory
        {
            get
            {
                if(Categories.Count > 0)
                    return Categories[0].Name;

                  return string.Empty;
            }
        }

        protected Guid id;
        protected string title;
        protected IList<ProductCategory> productCategories;
        protected string name;
        protected string key;


        protected void LoadBasicFields()
        {
            id = Guid.NewGuid();
            productCategories = new List<ProductCategory>();
        }

    }
}
