﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Web.UI;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages.Configuration;
using System.Xml.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Telerik.Sitefinity.Utilities.MS.ServiceModel.Web;

namespace Products
{
    public class ProductManager
    {
        //private StreamWriter log;
        //private Stopwatch sw;

        public ProductManager()
        {
            //log  = File.CreateText("C:\\codespeed\\codespeed" + DateTime.Now.Second + "-" + DateTime.Now.Millisecond + ".txt");
            //sw = new Stopwatch();
            ////log.AutoFlush = true;
        }
        private static ProductSetup productSetup;
        public ProductSetup ProductSetup
        {
            get
            {
                if(productSetup == null)
                    productSetup = new ProductSetup();

                return productSetup;
            }
        }

        #region Public Methods

        #region Product Parameters

        public IList<string> GetDimensionParameters()
        {
            var manager = ContentManager.GetManager();
            var item = manager.GetContent(ProductSetup.DimensionParametersContentId);
          
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            XDocument xdoc = XDocument.Parse("<root>" + itemContent + "</root>");

            var parameters = xdoc.Root.Elements("p").Select(element => element.Value).ToList<string>();

            return parameters;
        }
        public void deleteParameter(string value, string specificParameter)
        {
            var manager = ContentManager.GetManager();
            ContentItem item;
            Guid contentId = new Guid();
            string outputString = "";

            if(specificParameter == "Dimensions")
            {
               contentId = ProductSetup.DimensionParametersContentId;
            }
            else if (specificParameter == "Technical Data")
            {
               contentId = ProductSetup.TechnicalDataParametersContentId;
            }

            item = manager.GetContent(contentId);
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            string[] Contents = Regex.Split(itemContent, "</p>");
            
            for (var i = 0; i < Contents.Length-1; i++)
            {
                int index = Contents[i].IndexOf("<p>");
                string originalContent = Contents[i].Substring(index+3).Trim();
                if (originalContent != value)
                {
                    outputString += "<p>" + originalContent + "</p> ";
                }
            }
            outputString = outputString.Trim();
            App.WorkWith().ContentItem(contentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = outputString;
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();
        }
        public void setParameter(string value, string specificParameter)
        {
            var manager = ContentManager.GetManager();
            ContentItem item;
            Guid contentId = new Guid();
            if (specificParameter == "Dimensions")
            {
                contentId = ProductSetup.DimensionParametersContentId;
            }
            else if (specificParameter == "Technical Data")
            {
                contentId = ProductSetup.TechnicalDataParametersContentId;
            }
            item = manager.GetContent(contentId);
            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));
            itemContent += " <p>" + value + "</p>";

            App.WorkWith().ContentItem(contentId)
                        .CheckOut().Do(c =>
                        {
                            c.Content = itemContent;
                        })
                        .CheckIn()
                        .Publish()
                        .SaveChanges();
        }
        public IList<string> GetTechnicalDataParameters()
        {
            IList<string> result = new List<string>();

            var manager = ContentManager.GetManager();
            var item = manager.GetContent(ProductSetup.TechnicalDataParametersContentId);

            string itemContent = item.Content.Value.Replace("&nbsp;", HttpUtility.HtmlDecode("&nbsp;"));

            XDocument xdoc = XDocument.Parse("<root>" + itemContent + "</root>");
            
            var parameters = xdoc.Root.Elements("p").Select(element => element.Value).ToList<string>();

            return parameters;
        }

        public int GetProductLibraryFilesCount(string libraryName, string title)
        {
            IList<ProductFile> result = new List<ProductFile>();

             DocumentLibrary dlib = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault();
                
             foreach (Document doc in dlib.Documents.Where(d => d.Status == ContentLifecycleStatus.Live && d.Visible == true))
             {
                 result.Add(new ProductFile(){ FileId = doc.Id, FileType = ProductFileType.File, Key = doc.Title});
             }
             if (title == String.Empty || title == "" || title == null)
                 return result.Count;
             else
                return result.Where(r => r.Key.ToLower().Contains(title.ToLower())).Count();
        }

        public IList<ProductFile> GetProductLibraryFiles(string libraryName, int maximumRowsCount, int startRowIndex, string title)
        {
            IList<ProductFile> result = new List<ProductFile>();

            DocumentLibrary dlib = App.WorkWith()
                                 .DocumentLibraries()
                                 .Where(dL => dL.Title == libraryName)
                                 .Get().FirstOrDefault();


            //GJM - 3/23/2012 sort by LastModified
            List<Document> tempList = new List<Document>();
            tempList.AddRange(dlib.Documents.Where(d => d.Status == ContentLifecycleStatus.Live && d.Visible == true));
            tempList.Sort(CompareDocumentsLastModified);

            foreach (Document doc in tempList)
            {
                result.Add(new ProductFile(){ FileId = doc.Id, FileType = ProductFileType.File, Key = doc.Title});
            }
            //if (title == String.Empty || title == "" || title == null)
                return result;
            //else
            //   return result.Where(r => r.Key.ToLower().Contains(title.ToLower())).ToList<ProductFile>();
        }


        //GJM - 3/23/2012
        private int CompareDocumentsLastModified(Document a, Document b)
        {
            DateTime aTime = a.LastModified, bTime = b.LastModified;
            if (aTime < bTime)
                return -1;
            else if (aTime > bTime)
                return 1;
            else
                return 0;
        }

        #endregion

        #region Managing Products

        public void DeleteProducts(IList<Guid> productIdsToDelete)
        {
            using (var sf = App.WorkWith())
            {
                foreach (Guid id in productIdsToDelete)
                {
                    //try
                    //{
                    //    sf.DocumentLibrary(product.RelatedProductFiles.LibraryId).Delete();
                    //}
                    //catch (Exception ex) { }

                    //try
                    //{
                    //    sf.Image(product.RelatedProductFiles.GetId("Image")).Delete();
                    //}
                    //catch (Exception ex) { }

                    //try
                    //{
                    //    sf.Image(product.RelatedProductFiles.GetId("Spec Image")).Delete();
                    //}
                    //catch (Exception ex) { }
                    try
                    {
                        sf.ContentItem(id).Delete().SaveChanges();
                    }
                    catch (Exception ex) { }
                }
            }
            
            HttpContext.Current.Cache.Remove("BaseProductList");
            
        }

        public void CreateProduct(Product product, bool duplicate = false)
        {
            using (var sf = App.WorkWith())
            {
                try
                {
                    sf.ContentItem()
                        .CreateNew(product.Id).CheckOut()
                        .Do(c =>
                        { //setting the Content item (c) properties
                            product.FillContentItem(c, duplicate);
                        }).CheckIn()
                        .Publish()
                        .SaveChanges();

                    //Guid libraryId = product.RelatedProductFiles.LibraryId;

                    //sf.DocumentLibrary().CreateNew(libraryId).Do(d =>
                    //{
                    //    d.Title = string.Concat("ProductLibrary ", product.Title);
                    //}).SaveChanges();

                }
                catch (Exception ex)
                {
                    if (ex.Message.StartsWith("An item with the URL", true, System.Globalization.CultureInfo.CurrentCulture) && 
                        ex.Message.EndsWith("already exists.", true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                    }
                }
            }

            HttpContext.Current.Cache.Remove("BaseProductList");
        }

        public Guid DuplicateProduct(Guid oldProductId)
        {
            Product product = GetProduct(oldProductId);
            product.Id = Guid.NewGuid();

            //clear out the related product files, links were being kept around from the product 
            //we are duplicating from and creating the problem described in issue # 72
            product.RelatedProductFiles = new ProductFiles();
            ContentItem ci = ContentManager.GetManager().GetContent(oldProductId);
            Product p = new Product(this, ci);

            //fill duplicated product with images from original product
            foreach (ProductFile pf in p.RelatedProductFiles.GetProductImages())
            {
                Guid imageId = Guid.Empty;
                Image img = App.WorkWith().Image(pf.FileId).Get();
                
                System.Net.WebRequest req = System.Net.WebRequest.Create("http://" + HttpContext.Current.Request.Url.Authority + img.MediaUrl);
                System.Net.WebResponse response = req.GetResponse();
                System.IO.Stream responseStream = response.GetResponseStream();
                MemoryStream ms = new MemoryStream((int)response.ContentLength);

                const int BufferLength = 1000;
                byte[] b = new byte[BufferLength]; //Buffer
                int cnt = 0;

                do
                {
                    //Read up to 1000 bytes from the response stream
                    cnt = responseStream.Read(b, 0, BufferLength);

                    //Write the number of bytes actually read
                    ms.Write(b, 0, cnt);
                }
                while (cnt > 0);

                App.WorkWith()
                    .Album(ProductSetup.ThumbnailsAlbumId)
                    .CreateImage()
                    .Do(i =>
                    {

                        i.Title = string.Concat(product.Title, " ", pf.Key);
                        imageId = i.Id;
                    })
                    .CheckOut()
                    .UploadContent(ms, img.Extension)
                    .CheckIn().Publish()
                    .SaveChanges();

                product.RelatedProductFiles.Add(pf.Key, imageId, ProductFileType.Image);
            }
            //create product and put into database
            CreateProduct(product, true);
            return product.Id;
        }

        public void SaveProductChanges(Product product, bool checkout)
        {
            using (var sf = App.WorkWith())
            {
                try
                {
                    if (!checkout)
                    {
                        sf.ContentItem(product.Id)
                            .Do(c =>
                            { //setting the Content item (c) properties
                                product.FillContentItem(c);
                            })

                            .SaveChanges();
                    }
                    else
                    {
                        sf.ContentItem(product.Id)
                            .CheckOut()
                            .Do(c =>
                            { //setting the Content item (c) properties
                                product.FillContentItem(c);
                            }).CheckIn().Publish()


                            .SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    //super bootleg but sitefinity sucks, so lets try it.
                    try
                    {
                        if (checkout)
                        {
                            sf.ContentItem(product.Id)
                                .Do(c =>
                                { //setting the Content item (c) properties
                                    product.FillContentItem(c);
                                })

                                .SaveChanges();
                        }
                        else
                        {
                            sf.ContentItem(product.Id)
                                .CheckOut()
                                .Do(c =>
                                { //setting the Content item (c) properties
                                    product.FillContentItem(c);
                                }).CheckIn().Publish()


                                .SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            HttpContext.Current.Cache.Remove("BaseProductList");
        }

        #endregion

        #region Retrieve Products
        
        public IList<Product> GetProducts()
        {
            IList<Product> products = new List<Product>();

            using (var sf = App.WorkWith())
            {
                try
                {
                    
                    sf.ContentItems()
                        .Where(c =>  c.ExpirationDate > DateTime.Today)
                        .OrderByDescending(c => c.DateCreated)
                        .ForEach(c =>
                        {
                            if (c.Organizer.TaxonExists("Tags", ProductSetup.ProductsTaxonId))
                            {
                                Product product = new Product(this, c);                                
                                products.Add(product);
                            }
                        });
                }
                catch(Exception ex)
                {

                }

                return products;
                
            }
        }

        public IList<BaseProduct> GetAccessories()
        {
            IList<BaseProduct> products = new List<BaseProduct>();

            using (var sf = App.WorkWith())
            {
                try
                {
                    sf.ContentItems().Publihed().Where(c => c.GetValue<IList<Guid>>("Category").Contains(ProductSetup.ProductAccessoriesCategoryId)).ForEach(c =>
                        {
                            Product product = new Product(this, c);
                            products.Add(product);
                        });
                }
                catch (Exception ex)
                {

                }
                List<BaseProduct> tempList = new List<BaseProduct>(products);
                tempList.Sort(new NaturalComparer());
                return tempList;
            }
        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex, string title)
        {
            IList<BaseProduct> products = new List<BaseProduct>();

            try
            {
                if (title == string.Empty)
                {
                    
                    App.WorkWith().ContentItems().Skip(startRowIndex).Take(maximumRows).ForEach(c =>
                    {
                        products.Add(new Product(this, c));
                    });
                    //products = GetAllBaseProducts().Skip(startRowIndex).Take(maximumRows).ToList();
                }
                else
                {
                    
                    products = GetAllBaseProducts().Where(c => c.Title.ToLower().Contains(title.ToLower())).Skip(startRowIndex).Take(maximumRows).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return products;
        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex)
        {
            return GetBaseProducts(maximumRows, startRowIndex, null);
        }

        public IList<BaseProduct> GetUncategorizedProducts()
        {
            IList<BaseProduct> products = new List<BaseProduct>();
            IList<BaseProduct> filteredProducts = new List<BaseProduct>();

            App.WorkWith().ContentItems().Publihed().Where(c => c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId)).ForEach(c =>
            {
                IList<Guid> cats = (IList<Guid>)c.GetValue("Category");
                if (cats == null || cats.Count == 0)
                {
                    BaseProduct product = new BaseProduct(this, c);
                    filteredProducts.Add(product);
                }
            });


            List<BaseProduct> tempList = new List<BaseProduct>(filteredProducts);
            tempList.Sort(new NaturalComparer());
            return tempList;

        }

        public IList<BaseProduct> GetBaseProducts(int maximumRows, int startRowIndex, object checkedNodes, string title)
        {
            
            IList<Guid> checkedNodesList = (IList<Guid>) checkedNodes;
            IList<BaseProduct> products = new List<BaseProduct>();
            IList<BaseProduct> filteredProducts = new List<BaseProduct>();
            for (int i = 0; i < checkedNodesList.Count; i++)
            {
                App.WorkWith().ContentItems().Publihed().Where
                              (c => c.GetValue<IList<Guid>>("Category").Contains(checkedNodesList[i])).Where(c => c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId)).ForEach(c =>
                {
                    BaseProduct product = new BaseProduct(this, c);
                    filteredProducts.Add(product);
                });
            }

            List<BaseProduct> tempList = new List<BaseProduct>(filteredProducts);
            tempList.Sort(new NaturalComparer());
            return tempList;         
        }

        public IDictionary<Guid, string> GetProductsIdName()
        {
            IDictionary<Guid, string> products = new Dictionary<Guid, string>();

            using (var sf = App.WorkWith())
            {
                try
                {
                    
                    sf.ContentItems()
                       // .Where(c => c.ExpirationDate > DateTime.Today)
                        .OrderBy(c => c.Name)
                        .ForEach(c =>
                        {
                            if (c.Organizer.TaxonExists("Tags", ProductSetup.ProductsTaxonId))
                            {
                                products.Add(new KeyValuePair<Guid,string>(c.Id, c.Title));                                
                            }
                        });
                }
                catch (Exception ex)
                {

                }

                return products;
            }
        }

        public Product GetProduct(string title)
        {
            Product product = null;

            var manager = ContentManager.GetManager();
            
            var item = manager.GetContent().Where(t => t.Title == title).FirstOrDefault();
            if (item != null)
            {
                product = new Product(this, item);
            }
            return product;
        }

        public Product GetProductByKey(string key)
        {
            Product product = null;
            /*App.WorkWith().ContentItems().Publihed().Where(c => c.Title.Equals(key)).ForEach(c =>
            
                product = new Product(this, c);
            }); */
            ContentItem ci = App.WorkWith().ContentItems().Publihed().Where(c => c.Title.Equals(key)).Get().FirstOrDefault<ContentItem>();
            if (ci != null)
                product = new Product(this, ci);

            return product;
                //(c => c.GetValue<IList<string>>("Title"));

          /*BaseProduct result = GetAllBaseProducts().Where(c =>
              {
                  return c.Key == key;
              }).FirstOrDefault();

          return (result == null) ? null : (Product)result;*/

          //var manager = ContentManager.GetManager();
          //var item = manager.GetContent().Where(t => (string)t.GetValue("[PRODUCT KEY]") == key).FirstOrDefault();
          /*            var items = App.WorkWith()
                          .ContentItems()
                          .Where(t => t.Title.Contains(key))
                          .Get();*/

          //foreach(var item in items)
          //{
          //    if (item.GetValue<string>("PRODUCT KEY") == key)
          //}

/*
          try {
              
              var f = GetAllBaseProducts();
              "var f = GetAllBaseProducts(); " + ////sw.ElapsedMilliseconds);

              
              var a = f.Where(c => c.Title == key);  //should return 2 objects
              "var a = f.Where(c => c.Title == key); " + ////sw.ElapsedMilliseconds);

              
              foreach (var b in a)
              {
                  //var originalGuid = b.Id;

                  //var manager = ContentManager.GetManager();
                  //var keyGuid = manager.GetContent().Where(t => t.Title == key).FirstOrDefault();
                  //var items = manager.GetContent().Where(t => t.Title == key); 
                    
                  //if (items.Count() > 1)
                  //{
                  //    foreach (var d in items)
                  //    {
                  //        var g = d.Id;
                  //        if (g == originalGuid)
                  //        {
                  //            return GetProduct(g);

                  //        }
                  //    }
                  //}
                  
               return GetProduct(b.Id);
              }
              "foreach " + ////sw.ElapsedMilliseconds);
            
          }
          catch(Exception ex)
          {
              string s = ex.Message;
          }          

          return null;*/
        }

        public Product GetProduct(Guid id)
        {
            Product product = null;

            var manager = ContentManager.GetManager();
            var item = manager.GetContent(id);
            if (item != null)
            {
                product = new Product(this, item);
            }
            return product;
        }

        #region Products Count

        public int GetAccessoriesCount()
        {
            int cnt = 0;
            App.WorkWith().ContentItems().Publihed().Where(c => c.GetValue<IList<Guid>>("Category").Contains(ProductSetup.ProductAccessoriesCategoryId)).Count(out cnt);
            return cnt;
        }

        public int GetProductsCount(string title)
        {
            
            int cnt = 0;
            if (title == string.Empty)
            {
                //cnt = GetAllBaseProducts().Count();
                App.WorkWith().ContentItems().Publihed().Where(c=>c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId)).Count(out cnt);
                return cnt;
            }

            
            cnt = GetAllBaseProducts().Where(c => c.Title.ToLower().Contains(title.ToLower())).Count();
            return cnt;
        }

        public int GetProductsCount(int maximumRows, int startRowIndex, object checkedNodes, string title)
        {
            IList<Guid> checkedNodesList = (IList<Guid>)checkedNodes;
            IList<BaseProduct> filteredProducts = new List<BaseProduct>();
            
            for (int i = 0; i < checkedNodesList.Count; i++)
            {
                App.WorkWith().ContentItems().Publihed().Where
                              (c => c.GetValue<IList<Guid>>("Category").Contains(checkedNodesList[i])).Where(c => c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId)).ForEach(c =>
                              {
                                  BaseProduct product = new BaseProduct(this, c);
                                  filteredProducts.Add(product);
                              });
            }
            return filteredProducts.Count();
        }

        #endregion

        #endregion

        #region Product Categories

        public IList<ProductCategory> GetCategories()
        {
            if (HttpContext.Current.Cache["ProductCategories"] == null)
            {
                
                TaxonomyManager manager = TaxonomyManager.GetManager();
                var parentTaxon = manager.GetTaxon<HierarchicalTaxon>(ProductSetup.ProductsCategoryId);

                
                IList<ProductCategory> categories = new List<ProductCategory>();
                GetSubCategories(parentTaxon, categories);
                
                var parentAcessoryTaxon = manager.GetTaxon<HierarchicalTaxon>(ProductSetup.ProductAccessoriesCategoryId);
                
                IList<ProductCategory> accessorycategories = new List<ProductCategory>();
                GetSubCategories(parentAcessoryTaxon, accessorycategories);
                
                foreach (ProductCategory apc in accessorycategories)
                    categories.Add(apc);

                HttpContext.Current.Cache.Insert("ProductCategories", categories, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 10));
            }

            return ((IList<ProductCategory>)HttpContext.Current.Cache["ProductCategories"]);
        }

        public Guid GetCategoryId(string name)
        {
            Guid id = Guid.Empty;
            try
            {
                id = GetCategories().Where(c => c.Name == name).Single().Id;
            }
            catch (Exception) {}
            return id;
        }

        public ProductCategory GetCategory(Guid id)
        {
            try
            {
                return GetCategories().Where(c => c.Id == id).Single();
            }
            catch (Exception) { }
            return null;
        }

        public string GetCategoryName(Guid id)
        {
          try
          {
              return GetCategories().Where(c => c.Id == id).Single().Name;
          }
          catch (Exception) { }
          return null;
        }

        public IList<ProductCategory> GetFLSubCategories(Guid categoryId)
        {
            TaxonomyManager manager = TaxonomyManager.GetManager();
            var parentTaxon = manager.GetTaxon<HierarchicalTaxon>(categoryId);

            IList<ProductCategory> categories = new List<ProductCategory>();

            ProductCategory productCategory = new ProductCategory()
            {
              Id = parentTaxon.Id,
              Name = parentTaxon.Name,
            };

            if (parentTaxon.Parent != null)
              productCategory.ParentId = parentTaxon.Parent.Id;

            categories.Add(productCategory);

            foreach (HierarchicalTaxon childtaxon in parentTaxon.Subtaxa)
            {
              categories.Add(
                new ProductCategory() {
                  Id = childtaxon.Id,
                  Name = childtaxon.Name,
              });
            }

            return categories;
        }

        private bool IsInCategories(IList<Guid> itemCategories, IList<Guid> checkedNodes)
        {
            foreach (Guid id in checkedNodes)
            {
                if (itemCategories.Contains(id))
                    return true;
            }

            return false;
        }

        #endregion
        
        #region Files

        public void UploadFiles(Product product, bool isNewProduct)
        {
            foreach (string imageKey in product.RelatedProductFiles.GetImageKeys())
            {

                Guid imageId = product.RelatedProductFiles.GetId(imageKey);
                ProductUploadedFile imageFile = product.RelatedProductFiles.GetImage(imageKey);
 
                if (imageId == Guid.Empty)
                {
                    App.WorkWith()
                        .Album(ProductSetup.ThumbnailsAlbumId)
                        .CreateImage()
                        .Do(i =>
                        {

                            i.Title = string.Concat(product.Title, " ", imageKey);
                            imageId = i.Id;
                        })
                        .CheckOut()
                        .UploadContent(imageFile.InputStream, imageFile.GetExtension())
                        .CheckIn().Publish()
                        .SaveChanges();
                    
                    product.RelatedProductFiles.Add(imageKey, imageId, ProductFileType.Image);
                }
                else
                {

                    App.WorkWith()
                       .Image(imageId)
                       .CheckOut()
                           .UploadContent(imageFile.InputStream, imageFile.GetExtension())
                           .Do(i =>
                           {
                               i.Title = string.Concat(product.Title, " ", imageKey);
                               i.LastModified = DateTime.UtcNow;
                           })
                        .CheckIn().Publish()
                        .SaveChanges();



                }

            }


            foreach (string fileKey in product.RelatedProductFiles.GetFileKeys())
            {

                Guid fileId = product.RelatedProductFiles.GetId(fileKey);
                ProductUploadedFile file = product.RelatedProductFiles.GetFile(fileKey);

                //Document document;
                if (fileId == Guid.Empty)
                {
                    //App.WorkWith()
                    //    .DocumentLibrary(product.RelatedProductFiles.LibraryId)
                    //    .CreateDocument()
                    //    .Do(d =>
                    //    {
                    //        d.Title = string.Concat(fileKey);
                    //        fileId = d.Id;
                    //    })
                    //    .CheckOut()
                    //    .UploadContent(file.InputStream, file.GetExtension())
                    //    .CheckInAndPublish()
                    //    .SaveChanges();

                    /*App.WorkWith()
                        .DocumentLibrary(GetFileLibId(fileKey))
                        .CreateDocument()
                        .CheckOut()
                        .Do(d =>
                        {
                            d.Title = product.Title;
                            fileId = d.Id;
                        })
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckInAndPublish(true)
                        .SaveChanges();*/
                    App.WorkWith()
                        .DocumentLibrary(GetFileLibId(fileKey))
                        .CreateDocument()
                        .Do(d =>
                        {
                            d.Title = file.FileName.Remove(0, file.FileName.LastIndexOf('\\') + 1);
                            fileId = d.Id;
                        })
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckIn()
                        .Do(d =>
                        {
                            d.ApprovalWorkflowState = "Published";
                        })
                        .Publish()
                        .SaveChanges();
                    product.RelatedProductFiles.Add(fileKey, fileId, ProductFileType.File);
                }
                else
                {
                    /*App.WorkWith()
                        .Document(fileId)
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .Do(d =>
                        {
                            d.LastModified = DateTime.UtcNow;
                        })
                        .CheckInAndPublish(true)
                        .SaveChanges();*/
                    /*App.WorkWith()
                        .Document(fileId)
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckIn()
                        .Do(d =>
                        {
                            d.LastModified = DateTime.UtcNow;
                            d.ApprovalWorkflowState = "Published";
                        })
                        .Publish()
                        .SaveChanges();*/
                    App.WorkWith()
                       .Document(fileId)
                       .CheckOut()
                       .UploadContent(file.InputStream, file.GetExtension())
                       .Do(d =>
                       {
                           d.LastModified = DateTime.UtcNow;
                           d.ApprovalWorkflowState = "Published";
                       })
                       .CheckIn()
                       .Publish()
                       .SaveChanges(); 
                }

            }
            if (isNewProduct)
                SaveProductChanges(product, false);
            else
                SaveProductChanges(product, true);
        }

        public void UploadFileToProductLibrary(Guid libraryId, ProductUploadedFile file)
        {
            App.WorkWith()
                        .DocumentLibrary(libraryId)
                        .CreateDocument()
                        .Do(d =>
                        {
                            d.Title = file.FileName.Remove(0, file.FileName.LastIndexOf('\\') + 1);
                        })
                        .CheckOut()
                        .UploadContent(file.InputStream, file.GetExtension())
                        .CheckInAndPublish()
                        .SaveChanges();
        }

        public string GetImageURL(Product product, string imgName)
        {
            Guid id = product.RelatedProductFiles.GetId(imgName);
            if (id == Guid.Empty)
                return null;
            else
            {
                Image result = App.WorkWith().Image(id).Get();

                if (result.Width > 300)
                  return result.MediaUrl.Contains("?") ? 
                   result.MediaUrl + "&size=400" :
                   result.MediaUrl + "?size=400";
                else
                  return result.MediaUrl;
            }
        }

        public string GetFileURL(Product product, string fileType)
        {
            Guid id = product.RelatedProductFiles.GetId(fileType);

            if (id == Guid.Empty)
                return null;
            else
            {
                try {
                  return App.WorkWith().Document(id).Get().MediaUrl;
                }
                catch {
                  return null;
                }
            }
        }

        public string GetFileName(Product product, string fileType)
        {
            Guid id = product.RelatedProductFiles.GetId(fileType);

            if (id == Guid.Empty)
                return string.Empty;
            else
            {
                try
                {
                    return App.WorkWith().Document(id).Get().Title;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public void DeleteFile(Product product, string fileName) 
        {
            Guid id = product.RelatedProductFiles.GetId(fileName);

            if (id == Guid.Empty)
              return;
            else
            {
                product.RelatedProductFiles.DeleteFile(fileName);
                SaveProductChanges(product, true);

                App.WorkWith().Document(id).Delete().SaveChanges();
            }
        }
        #endregion

        public bool CheckProductTitle(string title, Guid id)
        {
            try
            {
                int indicator = 0;
                App.WorkWith().ContentItems().Publihed()
                                    // .Where(c => c.ExpirationDate > DateTime.Today)
                                     .Where( (c => c.Title == title && !c.Id.Equals(id)) )
                                     .Count(out indicator);
             
                if (indicator == 0)
                    return true;

            }
            catch (Exception ex)
            {

            }
            return false;
        }
         
        #endregion

        #region Private Methods

        private Guid GetFileLibId(string fileKey)
        {
            string libraryName = string.Empty;

            switch (fileKey)
            {
                case "Application Bulletin":
                    libraryName = "Product Application Bulletins";
                    break;
                case "Complete Service Manual":
                    libraryName = "Service Manuals";
                    break;
                case "Flyer":
                    libraryName = "Flyers";
                    break;
                case "Catalog":
                    libraryName = "Catalogs";
                    break;
                case "Quick Reference Guides":
                    libraryName = "Quick Ref Manuals";
                    break;
            }

            Guid result = App.WorkWith()
                                     .DocumentLibraries()
                                     .Where(dL => dL.Title == libraryName)
                                     .Get().FirstOrDefault().Id;
            return result;
        }

        private IList<BaseProduct> GetAllBaseProducts()
        {
            //if (HttpContext.Current.Cache["BaseProductList"] == null)
            {
                IList<BaseProduct> products = new List<BaseProduct>();
                try
                {
                    
                    App.WorkWith().ContentItems().Publihed()
                        //.Where(c => c.ExpirationDate > DateTime.Today)
                        .Where(c => c.GetValue<IList<Guid>>("Tags").Contains(ProductSetup.ProductsTaxonId))
                        .OrderBy(c => c.Title)
                        .ForEach(c =>
                        {
                            BaseProduct product = new BaseProduct(this, c);
                            products.Add(product);
                        });
                }
                catch (Exception ex)
                {

                }

                List<BaseProduct> tempList = new List<BaseProduct>(products);
                tempList.Sort(new NaturalComparer());

                HttpContext.Current.Cache.Insert("BaseProductList", tempList, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 1, 0));
            }
            return (IList<BaseProduct>)HttpContext.Current.Cache["BaseProductList"];
        }

        private void GetSubCategories(HierarchicalTaxon taxon, IList<ProductCategory> categories)
        {
            ProductCategory productCategory = new ProductCategory()
            {
                Id = taxon.Id,
                Name = taxon.Name,
            };

            if (taxon.Parent != null)
                productCategory.ParentId = taxon.Parent.Id;

            categories.Add(productCategory);

            foreach (HierarchicalTaxon childtaxon in taxon.Subtaxa)
            {
                GetSubCategories(childtaxon, categories);
            }
        }

        #endregion
    }
}
