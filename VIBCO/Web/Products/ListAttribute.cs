﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products
{
    [Serializable]
    public class ListAttribute
    {
        public Guid AttributeId { get; set; }
        public string ParamName { get; set; }
        public string Value { get; set; }
    }
}
