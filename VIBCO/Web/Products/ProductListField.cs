﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Products
{
    public class ProductListField<T>
    {
        
        public ProductListField()
        {
            Items = new List<T>();
        }

        public List<T> Items { get; set; }

        public string Serialize()
        {
              String XmlizedString = null;
              MemoryStream memoryStream = new MemoryStream();
              XmlSerializer xs = new XmlSerializer(this.GetType());
              XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
              xs.Serialize(xmlTextWriter, this);
              memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
              XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
              return XmlizedString;
        }

        public void Deserialize(string str)
        {
            if(string.IsNullOrEmpty(str)) {
              this.Items = (new ProductListField<T>()).Items;
              return;
            }

            XmlSerializer xs = new XmlSerializer(this.GetType());
            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(str));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            ProductListField<T> pf = (ProductListField<T>) xs.Deserialize(memoryStream);
            this.Items = pf.Items;
        }

        private String UTF8ByteArrayToString(Byte[] characters)
        {

            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        private Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        } 
    }
}
