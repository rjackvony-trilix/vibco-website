﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products
{
    [Serializable]
    public class Accessory
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
    }
}
