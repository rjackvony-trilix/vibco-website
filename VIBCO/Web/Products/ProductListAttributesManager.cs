﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Products
{
    public class ProductListAttributesManager
    {
        public ProductListAttributesManager()
        {
        }

        public ProductListAttributesManager(StateBag viewState, string attributeName)
        {
            this.viewState = viewState;
            this.attributeName = attributeName;
        }

        private string attributeName;
        private StateBag viewState;

        public List<ListAttribute> Dimensions
        {
            get
            {
                if (viewState[attributeName] == null)
                    viewState[attributeName] = new List<ListAttribute>();
                return (List<ListAttribute>)this.viewState[attributeName];
            }
        }

        public List<ListAttribute> Select()
        {
            return Dimensions;
        }

        public void Update(string ParamName, string Value)
        {
            //dimension = Dimensions.Where(d => d.ParamName == ParamName).Single();
            //Dimensions.Add(new Dimension() { ParamName = ParamName, Value = Value });
        }

        public void Insert(string ParamName, string Value)
        {
            Dimensions.Add(new ListAttribute() { AttributeId = Guid.NewGuid(), ParamName = ParamName, Value = Value });
        }

        public void Delete(object AttributeId, string ParamName, string Value)
        {
            Guid id = (Guid)AttributeId;

            ListAttribute attributeToDelete = null;

            if(id != Guid.Empty)
              attributeToDelete = Dimensions.Where(a => a.AttributeId == id).SingleOrDefault();
            else
              attributeToDelete = Dimensions.Where(a => a.ParamName == ParamName && a.Value == Value).SingleOrDefault();

            Dimensions.Remove(attributeToDelete);
        }
        
    }
}
