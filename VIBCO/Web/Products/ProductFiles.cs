﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Web.UI;

namespace Products
{
    public class ProductFiles
    {
        public ProductFiles()
        {
            CreateDictionaries();
            Add("ProductLibrary", Guid.NewGuid(), ProductFileType.Library);
        }

        public Guid LibraryId
        {
            get
            {
                return fieldlist.Items
                    .Where(f => f.Key == "ProductLibrary" && f.FileType == ProductFileType.Library)
                    .SingleOrDefault()
                    .FileId;
            }
            set
            {
                fieldlist.Items
                    .Where(f => f.Key == "ProductLibrary" && f.FileType == ProductFileType.Library)
                    .SingleOrDefault()
                    .FileId = value;
            }
        }

        public Guid GetId(string key)
        {
            ProductFile pfile = fieldlist.Items.Where(i => i.Key == key).FirstOrDefault();
            if (pfile == null)
                return Guid.Empty;
            return pfile.FileId;
        }

        public ProductUploadedFile GetFile(string key)
        {
            if (!files.Keys.Contains(key))
                return null;
            return files[key];
        }

        public void DeleteFile(string key)
        {
            List<ProductFile> filesToRemove = fieldlist.Items.Where(item => item.Key == key).ToList();
            
            foreach (ProductFile item in filesToRemove)
            {
                fieldlist.Items.Remove(item);   //
                files.Remove(item.Key);
            }
        }

        public ProductUploadedFile GetImage(string key)
        {
            if (!images.Keys.Contains(key))
                return null;
            return images[key];
        }

        public ICollection<string> GetFileKeys()
        {
            return files.Keys;
        }

        public ICollection<string> GetImageKeys()
        {
            return images.Keys;
        }

        private ProductListField<ProductFile> fieldlist;
        private IDictionary<string, ProductUploadedFile> files;
        private IDictionary<string, ProductUploadedFile> images;

        public List<ProductFile> GetProductImages()
        {
            List<ProductFile> imgs = new List<ProductFile>();
            foreach (ProductFile p in fieldlist.Items)
            {
                if (p.Key.Equals("Image") || p.Key.Equals("Spec Image"))
                    imgs.Add(p);
            }
            return imgs;
        }

        public ProductFile GetProductImage(string key)
        {
            foreach (ProductFile p in fieldlist.Items)
            {
                if (p.Key.Equals(key))
                    return p;
            }
            return null;
        }

        public ProductFile GetProductDocument(string key)
        {
            foreach (ProductFile p in fieldlist.Items)
            {
                if (p.Key.Equals(key))
                    return p;
            }
            return null;

        }

        public string Serialize()
        {
            return fieldlist.Serialize();
        }

        public void Deserialize(string str)
        {
            fieldlist.Deserialize(str);
        }

        public void AddFileToUpload(string filetype, ProductUploadedFile file)
        {
            files.Add(filetype, file);
        }

        public void AddImageToUpload(string filetype, ProductUploadedFile file)
        {
            images.Add(filetype, file);
        }

        public void Add(string filekey, Guid fileid, ProductFileType fileType)
        {
            fieldlist.Items.Add(new ProductFile() { Key = filekey, FileId = fileid , FileType = fileType });
        }

        private void CreateDictionaries()
        {
            fieldlist = new ProductListField<ProductFile>();

            files = new Dictionary<string, ProductUploadedFile>();
            images = new Dictionary<string, ProductUploadedFile>();
        }

    }

}
