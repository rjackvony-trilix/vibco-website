﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products
{
    [Serializable]
    public class ProductFile
    {
        public ProductFile()
        {
            FileId = Guid.Empty;
        }

        public string Key { get; set; }
        public Guid FileId { get; set; }
        public ProductFileType FileType { get; set; }
    }

    public enum ProductFileType
    {
        Image = 0,
        File = 1,
        Library = 2
    }
}
