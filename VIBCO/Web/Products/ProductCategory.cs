﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products
{
    [Serializable]
    public class ProductCategory
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }

        public string Name { get; set; }

    }
}
