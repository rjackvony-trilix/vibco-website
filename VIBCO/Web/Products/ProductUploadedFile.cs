﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Products
{
    public class ProductUploadedFile
    {
        public Stream InputStream { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string GetExtension()
        {
            return Extension;
        }
    }
}
