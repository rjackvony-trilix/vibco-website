﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity;

namespace Products
{
    public class ProductSetup
    {

        public static Guid ThumbnailsAlbumId
        {
            get
            {
                return new Guid("855D8128-E7CE-49A6-9482-166A76425888");
            }
        }

        public static Guid ProductsTaxonId
        {
            get
            {
                return new Guid("D59719BA-E72A-487D-96EF-0AA4239845BC");
            }
        }

        public static Guid DimensionParametersContentId
        {
            get
            {
                return new Guid("D2B6EA99-AB45-4A3B-879B-42955567CF74");
            }
        }

        public static Guid TechnicalDataParametersContentId
        {
            get
            {
                return new Guid("BCD1818C-E857-4DDF-A08B-9D61017A518C");
            }
        }

        public static Guid ProductsCategoryId
        {
            get
            {
                return new Guid("9E8BA821-EA76-4582-B340-05DDE7FAFEBA");
            }
        }

        public static Guid ProductAccessoriesCategoryId
        {
            get
            {
                return new Guid("63AE9D63-8717-4E26-95D6-83461D771C4E");
            }
        }

        public void Setup()
        {
            using (var sf = App.WorkWith())
            {
                SystemManager.RestartApplication(false);

                 sf.Pages().Where(p => p.Page != null && p.Page.Title == "Product Editor")
                     .Where(p => p.Page.Status == ContentLifecycleStatus.Live)
                     .ForEach(p =>
                     {
                         p.Page.EnableViewState = true;
                     }).SaveChanges();

                 SystemManager.RestartApplication(false);

                 sf.ContentItem()
                         .CreateNew(DimensionParametersContentId)
                         .Do(c =>
                         {
                             c.Title = "Product Dimension Parameters";
                             c.Content = "<p>A</p> <p>C*</p> <p>L</p> <p>W</p> <p>H</p> <p>F</p> <p>G</p> <p>R</p> <p>INLET</p> <p>OUTLET</p> <p>Weight</p>";
                         })
                         .Publish()
                         .SaveChanges();

                 sf.ContentItem()
                         .CreateNew(TechnicalDataParametersContentId)
                         .Do(c =>
                         {
                             c.Title = "Product Technical Data Parameters";
                             c.Content = "<p>20psi</p> <p>40psi</p> <p>60psi</p> <p>&nbsp;&nbsp;VPM</p> <p>&nbsp;&nbsp;CFM</p> <p>Force lbs</p> <p>dB</p> <p>Max Lbs Material in Bin</p>";
                         })
                         .Publish()
                         .SaveChanges();

                 SystemManager.RestartApplication(false);


                 try
                 {
                     sf.DynamicData()
                         .Type(typeof(ContentItem))
                         .Field(new Guid("B703579C-4584-4C33-B78A-3E61025FFD36")).Delete().Done()
                         .Field(new Guid("76C0BA41-B162-4F48-8BE0-D9D160371A21")).Delete().Done()
                         .Field(new Guid("6897BB28-F0D1-4B64-BA13-82571FF9615D")).Delete().Done()
                         .Field(new Guid("B0F604BF-3073-4C91-AD1C-ACB1FCDE5DDE")).Delete().Done()
                         .Field(new Guid("831A11D0-5D24-4919-BCBE-83BAF9FE89F2")).Delete().Done()
                         .SaveChanges(true);

                 }
                 catch (Exception ex)
                 {
                 }

                 SystemManager.RestartApplication(false);

                 try
                 {
                     sf.Album(ThumbnailsAlbumId).Get();
                 }
                 catch (Exception ex)
                 {
                     sf.Album().CreateNew(ThumbnailsAlbumId).Do(a =>
                     {
                         a.Title = "Product Thumbnails";
                     }).SaveChanges();

                 }


                 sf.DynamicData()
                    .Type(typeof(ContentItem))
                    .Field()
                        .TryCreateNew("DIMENSIONS", typeof(string), new Guid("B703579C-4584-4C33-B78A-3E61025FFD36"))
                        .Do(f =>
                        {
                            f.FieldName = "DIMENSIONS";
                            f.ColumnName = "DIMENSIONS";
                            f.Title = "DIMENSIONS";
                            f.DBSqlType = "NVARCHAR(MAX)";
                            f.DBType = "LONGVARCHAR";
                        })
                        .Done()
                    .Field()
                        .TryCreateNew("TECHNICAL DATA", typeof(string), new Guid("76C0BA41-B162-4F48-8BE0-D9D160371A21"))
                        .Do(f =>
                        {
                            f.FieldName = "TECHNICAL DATA";
                            f.ColumnName = "TECHNICAL DATA";
                            f.Title = "TECHNICAL DATA";
                            f.DBSqlType = "NVARCHAR(MAX)";
                            f.DBType = "LONGVARCHAR";
                        })
                        .Done()
                    .Field()
                        .TryCreateNew("ACCESSORIES", typeof(string), new Guid("6897BB28-F0D1-4B64-BA13-82571FF9615D"))
                        .Do(f =>
                        {
                            f.FieldName = "ACCESSORIES";
                            f.ColumnName = "ACCESSORIES";
                            f.Title = "ACCESSORIES";
                            f.DBSqlType = "NVARCHAR(MAX)";
                            f.DBType = "LONGVARCHAR";
                        })
                        .Done()
                     .Field()
                        .TryCreateNew("FILES", typeof(string), new Guid("B0F604BF-3073-4C91-AD1C-ACB1FCDE5DDE"))
                        .Do(f =>
                        {
                            f.FieldName = "FILES";
                            f.ColumnName = "FILES";
                            f.Title = "FILES";
                            f.DBSqlType = "NVARCHAR(MAX)";
                            f.DBType = "LONGVARCHAR";
                        })
                        .Done()
                    .SaveChanges(true);

                 SystemManager.RestartApplication(false);
                 
                TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
                var parentTag = taxonomyManager.CreateTaxonomy<FlatTaxonomy>(new Guid("D080903C-0DA8-40E0-80B0-E4EE104827F5"));
                parentTag.Name = "Vibco Content Types";
                parentTag.TaxonName = "Generic Content Types";
                parentTag.Description = "Taxonomy which will classify generic content items for Vibco.";
              
                var taxon = taxonomyManager.CreateTaxon<FlatTaxon>(ProductsTaxonId);
                taxon.Title = "Product Entity";
                taxon.Name = "Product Entity";
                taxon.Description = "This tag categorizes the Products";
                parentTag.Taxa.Add(taxon);
                taxonomyManager.SaveChanges();


                SystemManager.RestartApplication(false);


            }

        }

        public void Update1()
        {
            App.WorkWith().Pages().Where(p => p.Page != null && p.Page.Title == "Products")
                    .Where(p => p.Page.Status == ContentLifecycleStatus.Live)
                    .ForEach(p =>
                    {
                        p.Page.EnableViewState = true;
                    }).SaveChanges();

            
            SystemManager.RestartApplication(false);
        }

        public void Update2()
        {
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var taxonomy = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().Where(t => t.Name == "Categories").SingleOrDefault();

            var rootTaxonProducts = taxonomyManager.CreateTaxon<HierarchicalTaxon>(ProductsCategoryId);
            rootTaxonProducts.Title = "Products";
            rootTaxonProducts.Name = "Products";
            rootTaxonProducts.Description = "This category holds product types";
            taxonomy.Taxa.Add(rootTaxonProducts);

            taxonomyManager.SaveChanges();

        }

        public void Update3()
        {
            App.WorkWith().DynamicData()
                   .Type(typeof(ContentItem))
                   .Field()
                       .TryCreateNew("PRODUCT NAME", typeof(string), new Guid("A3151C0A-4429-4B63-9AEA-23AD3BF84B0B"))
                       .Do(f =>
                       {
                           f.FieldName = "PRODUCT NAME";
                           f.ColumnName = "PRODUCT NAME";
                           f.Title = "PRODUCT NAME";
                           f.DBSqlType = "NVARCHAR(MAX)";
                           f.DBType = "LONGVARCHAR";
                       })
                       .Done()
                    .Field()
                       .TryCreateNew("PRODUCT KEY", typeof(string), new Guid("3FF3325F-3ACA-4FC4-97D2-5275A7A2CAF9"))
                       .Do(f =>
                       {
                           f.FieldName = "PRODUCT KEY";
                           f.ColumnName = "PRODUCT KEY";
                           f.Title = "PRODUCT KEY";
                           f.DBSqlType = "NVARCHAR(MAX)";
                           f.DBType = "LONGVARCHAR";
                       })
                       .Done()
                .SaveChanges(true);

            SystemManager.RestartApplication(false);
                       
        }

        public void Update4()
        {
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var taxonomy = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().Where(t => t.Name == "Categories").SingleOrDefault();

            var rootTaxonAccessoires = taxonomyManager.CreateTaxon<HierarchicalTaxon>(ProductAccessoriesCategoryId);
            rootTaxonAccessoires.Title = "Accessories";
            rootTaxonAccessoires.Name = "Accessories";
            rootTaxonAccessoires.Description = "This category holds accessories";
            taxonomy.Taxa.Add(rootTaxonAccessoires);

            taxonomyManager.SaveChanges();

        }

        public void Update5()
        {
          using (var sf = App.WorkWith())
          {
            SystemManager.RestartApplication(false);

            try
            {
              sf.DynamicData()
                  .Type(typeof(ContentItem))
                  .Field(new Guid("4C1816AE-4FC8-11E0-9283-7449DFD72085")).Delete().Done()
                  .SaveChanges(true);

            }
            catch (Exception)
            {
            }

            SystemManager.RestartApplication(false); 
            
            sf.DynamicData()
               .Type(typeof(ContentItem))
               .Field()
                   .TryCreateNew("REVIEWS", typeof(string), new Guid("4C1816AE-4FC8-11E0-9283-7449DFD72085"))
                   .Do(f =>
                   {
                     f.FieldName = "REVIEWS";
                     f.ColumnName = "REVIEWS";
                     f.Title = "REVIEWS";
                     f.DBSqlType = "NVARCHAR(MAX)";
                     f.DBType = "LONGVARCHAR";
                   })
                   .Done()
               .SaveChanges(true);

            SystemManager.RestartApplication(false);
          }

        }
        
    }
}
