﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.OpenAccess;

namespace Products
{
    public class Product :BaseProduct
    {
        public Product():base()
        {
            LoadBasicFields();
        }

        public Product(Guid id)
        {
            base.LoadBasicFields();
            LoadBasicFields();

            this.id = id;
        }

        //public Product(string title, string description)
        //{
        //    LoadBasicFields();
            
        //    this.title = title;
        //    this.description = description;
        //}

        public Product(Guid id, string title, string description)
        {
            LoadBasicFields();

            this.id = id;
            this.title = title;
            this.description = description;
        }

        public Product(ProductManager pm, ContentItem contentItem) :base(pm, contentItem)
        {
            LoadBasicFields();

            description = contentItem.Content;
            
            Dimensions.Deserialize(contentItem.GetValue<string>("DIMENSIONS"));

            TechnicalData.Deserialize(contentItem.GetValue<string>("TECHNICAL DATA"));

            Reviews.Deserialize(contentItem.GetValue<string>("REVIEWS"));

            Accessories.Deserialize(contentItem.GetValue<string>("ACCESSORIES"));

            relatedProductFiles.Deserialize(contentItem.GetValue<string>("FILES"));

            
        }

        public void FillContentItem(ContentItem contentItem, bool duplicate = false)
        {
            //FIX FOR Duplicating products
            if (duplicate)
            {
                contentItem.Title = Title + " COPY";
                Title += " COPY";
            }
            else
                contentItem.Title = Title;

            contentItem.Content = Description;
            
            contentItem.PublicationDate = DateTime.Now;
            contentItem.ExpirationDate = DateTime.Now.AddDays(4 * 365);

            if(!contentItem.Organizer.TaxonExists("Tags",ProductSetup.ProductsTaxonId))
                contentItem.Organizer.AddTaxa("Tags", ProductSetup.ProductsTaxonId);

            contentItem.SetValue("DIMENSIONS", Dimensions.Serialize());

            contentItem.SetValue("TECHNICAL DATA", TechnicalData.Serialize());

            contentItem.SetValue("REVIEWS", Reviews.Serialize());

            contentItem.SetValue("ACCESSORIES", Accessories.Serialize());

            contentItem.SetValue("FILES", RelatedProductFiles.Serialize());

            contentItem.SetValue("PRODUCT NAME", Name);

            contentItem.SetValue("PRODUCT KEY", Key); 

            TrackedList<Guid> categoryIds = new TrackedList<Guid>();
            foreach (ProductCategory category in Categories)
            {
                categoryIds.Add(category.Id);
            }

            contentItem.SetValue("Category", categoryIds);

            /*contentItem.Organizer.RemoveTaxa("Category", categoryIds);

            foreach (ProductCategory productCategory in Categories)
                contentItem.Organizer.AddTaxa("Category", productCategory.Id);
            */
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public ProductListField<ListAttribute> Dimensions
        {
            get
            {
                return dimesions;
            }
            set
            {
                dimesions = value;
            }
        }

        public ProductListField<ListAttribute> TechnicalData
        {
            get
            {
                return technicalData;
            }
            set
            {
                technicalData = value;
            }

        }

        public ProductListField<ListAttribute> Reviews
        {
          get
          {
            return reviews;
          }
          set
          {
            reviews = value;
          }
        }

        public ProductListField<Accessory> Accessories
        {
            get
            {
                return accessories;
            }
            set
            {
                accessories = value;
            }
        }

        public ProductFiles RelatedProductFiles
        {
            get
            {
                return relatedProductFiles;
            }
            set
            {
                relatedProductFiles = value;
            }
        }

        protected string description;
        protected ProductListField<ListAttribute> dimesions;
        protected ProductListField<ListAttribute> technicalData;
        protected ProductListField<ListAttribute> reviews;
        protected ProductListField<Accessory> accessories;
        protected ProductFiles relatedProductFiles;

        protected void LoadBasicFields()
        {
            //base.LoadBasicFields();

            dimesions = new ProductListField<ListAttribute>();
            technicalData = new ProductListField<ListAttribute>();
            reviews = new ProductListField<ListAttribute>();
            accessories = new ProductListField<Accessory>();
            relatedProductFiles = new ProductFiles();
        }
        
    }
}
